import 'package:vgrow/models/models.dart';
import 'package:vgrow/reducers/auth/auth_reducer.dart';
import 'package:vgrow/reducers/loan/loan_reducer.dart';
import 'package:vgrow/reducers/loan_summary/loan_summary_reducer.dart';
import 'package:vgrow/reducers/notification/notification_reducer.dart';
import 'package:redux/redux.dart';

Reducer<AppState> reducer = combineReducers(<Reducer<AppState>>[
  authReducer,
  notificationReducer,
  loanReducer,
  loanSummaryReducer
]);
