import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<AppState> loanSummaryReducer = combineReducers(<Reducer<AppState>>[
  TypedReducer<AppState, SaveLoanSummaryDetails>(saveLoanSummaryDetails),
  TypedReducer<AppState, GetLoanSummaryDetails>(getLoanSummaryDetails),
]);

AppState saveLoanSummaryDetails(AppState state, SaveLoanSummaryDetails action) {
  final AppStateBuilder b = state.toBuilder();
  print(action.loanSummary);
  b..loanSummary = action.loanSummary?.toBuilder();
  return b.build();
}

AppState getLoanSummaryDetails(AppState state, GetLoanSummaryDetails action) {
  print('--get loan summary reducer');
  final AppStateBuilder b = state.toBuilder();
  return b.build();
}
