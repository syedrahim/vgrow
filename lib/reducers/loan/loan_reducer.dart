import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/actions/loan/loan_action.dart';
import 'package:vgrow/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<AppState> loanReducer = combineReducers(<Reducer<AppState>>[
  TypedReducer<AppState, SaveLoanDetails>(saveLoanDetails),
  TypedReducer<AppState, GetLoanDetails>(getLoanDetails),
]);

AppState saveLoanDetails(AppState state, SaveLoanDetails action) {
  final AppStateBuilder b = state.toBuilder();
  print(action.loanDetails);
  b..loanDetails = action.loanDetails?.toBuilder();
  return b.build();
}


AppState getLoanDetails(AppState state, GetLoanDetails action) {
  final AppStateBuilder b = state.toBuilder();
  return b.build();
}

