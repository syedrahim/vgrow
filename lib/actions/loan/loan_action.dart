import 'package:built_collection/built_collection.dart';
import 'package:vgrow/models/loan/loans.dart';

class GetLoanDetails {}

class SaveLoanDetails {
  SaveLoanDetails({this.loanDetails});

  final BuiltList<Loans>? loanDetails;
}
