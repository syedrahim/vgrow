import 'package:vgrow/models/loan_summary/loan_summary_model.dart';

class GetLoanSummaryDetails {
  GetLoanSummaryDetails({required this.id});
  final int? id;
}

class SaveLoanSummaryDetails {
  SaveLoanSummaryDetails({this.loanSummary});

  final LoanSummary? loanSummary;
}
