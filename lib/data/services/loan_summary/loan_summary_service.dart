import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:vgrow/data/api/api_client.dart';
import 'package:vgrow/data/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/models/models.dart';
import 'package:vgrow/models/serializers.dart';

class LoanSummaryService extends ApiService {
  LoanSummaryService({required ApiClient client}) : super(client: client);

//************************************ log-in *********************************//
  Future<LoanSummary?> getLoanSummaryDetails(headersToApi,id) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.GET,
        headers: headersToApi,
        path: '/loan_management/loans/$id/summary',
        body: {});
    debugPrint('----loan summary service');
    debugPrint((res.data!.loanSummary == null).toString());
    if (res.isSuccess) {
      // print('res ------------loans summary');
      return res.data!.loanSummary;
    } else {
      print('res error loan summary');
      throw res.error;
    }
  }
}
