import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:vgrow/data/api/api_client.dart';
import 'package:vgrow/data/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/models/models.dart';
import 'package:vgrow/models/serializers.dart';

class AuthService extends ApiService {
  AuthService({required ApiClient client}) : super(client: client);

//************************************ log-in *********************************//
  Future<Map<String, dynamic>> loginWithPassword(
      {Map<String, dynamic>? objToApi}) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.POST,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        path: '/user_management/employee/login',
        body: objToApi);
    print(res);
    if (res.isSuccess) {
      print('res -------------------');
      print(res);
      return {'employee': res.data!.employee, 'token': res.data!.token};
    } else {
      throw res.error;
    }
  }
}
