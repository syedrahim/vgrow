import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:vgrow/data/api/api_client.dart';
import 'package:vgrow/data/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/models/models.dart';
import 'package:vgrow/models/serializers.dart';

class LoanService extends ApiService {
  LoanService({required ApiClient client}) : super(client: client);

//************************************ log-in *********************************//
  Future<BuiltList<Loans>?> getLoanDetails(headersToApi) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.GET,
        headers: headersToApi,
        path: '/loan_management/loans',
        body: {});
    if (res.isSuccess) {
      return res.data!.loanDetails;
    } else {
      throw res.error;
    }
  }
}
