// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AppUser> _$appUserSerializer = new _$AppUserSerializer();

class _$AppUserSerializer implements StructuredSerializer<AppUser> {
  @override
  final Iterable<Type> types = const [AppUser, _$AppUser];
  @override
  final String wireName = 'AppUser';

  @override
  Iterable<Object?> serialize(Serializers serializers, AppUser object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.userId;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.firstName;
    if (value != null) {
      result
        ..add('firstname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastName;
    if (value != null) {
      result
        ..add('lastname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.email;
    if (value != null) {
      result
        ..add('email')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.phone_number;
    if (value != null) {
      result
        ..add('phone_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.profile_pic_id;
    if (value != null) {
      result
        ..add('profile_pic_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.profile_pic_url;
    if (value != null) {
      result
        ..add('profile_pic_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.gender;
    if (value != null) {
      result
        ..add('gender')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.role_id;
    if (value != null) {
      result
        ..add('role_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.role_name;
    if (value != null) {
      result
        ..add('role_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.is_active;
    if (value != null) {
      result
        ..add('is_active')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.address;
    if (value != null) {
      result
        ..add('address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AppUser deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppUserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'firstname':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'lastname':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'phone_number':
          result.phone_number = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'profile_pic_id':
          result.profile_pic_id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'profile_pic_url':
          result.profile_pic_url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'role_id':
          result.role_id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'role_name':
          result.role_name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'is_active':
          result.is_active = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$AppUser extends AppUser {
  @override
  final int? userId;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? email;
  @override
  final String? phone_number;
  @override
  final String? profile_pic_id;
  @override
  final String? profile_pic_url;
  @override
  final String? gender;
  @override
  final int? role_id;
  @override
  final String? role_name;
  @override
  final bool? is_active;
  @override
  final String? code;
  @override
  final String? address;

  factory _$AppUser([void Function(AppUserBuilder)? updates]) =>
      (new AppUserBuilder()..update(updates))._build();

  _$AppUser._(
      {this.userId,
      this.firstName,
      this.lastName,
      this.email,
      this.phone_number,
      this.profile_pic_id,
      this.profile_pic_url,
      this.gender,
      this.role_id,
      this.role_name,
      this.is_active,
      this.code,
      this.address})
      : super._();

  @override
  AppUser rebuild(void Function(AppUserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AppUserBuilder toBuilder() => new AppUserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppUser &&
        userId == other.userId &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        email == other.email &&
        phone_number == other.phone_number &&
        profile_pic_id == other.profile_pic_id &&
        profile_pic_url == other.profile_pic_url &&
        gender == other.gender &&
        role_id == other.role_id &&
        role_name == other.role_name &&
        is_active == other.is_active &&
        code == other.code &&
        address == other.address;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, userId.hashCode);
    _$hash = $jc(_$hash, firstName.hashCode);
    _$hash = $jc(_$hash, lastName.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, phone_number.hashCode);
    _$hash = $jc(_$hash, profile_pic_id.hashCode);
    _$hash = $jc(_$hash, profile_pic_url.hashCode);
    _$hash = $jc(_$hash, gender.hashCode);
    _$hash = $jc(_$hash, role_id.hashCode);
    _$hash = $jc(_$hash, role_name.hashCode);
    _$hash = $jc(_$hash, is_active.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AppUser')
          ..add('userId', userId)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('email', email)
          ..add('phone_number', phone_number)
          ..add('profile_pic_id', profile_pic_id)
          ..add('profile_pic_url', profile_pic_url)
          ..add('gender', gender)
          ..add('role_id', role_id)
          ..add('role_name', role_name)
          ..add('is_active', is_active)
          ..add('code', code)
          ..add('address', address))
        .toString();
  }
}

class AppUserBuilder implements Builder<AppUser, AppUserBuilder> {
  _$AppUser? _$v;

  int? _userId;
  int? get userId => _$this._userId;
  set userId(int? userId) => _$this._userId = userId;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _phone_number;
  String? get phone_number => _$this._phone_number;
  set phone_number(String? phone_number) => _$this._phone_number = phone_number;

  String? _profile_pic_id;
  String? get profile_pic_id => _$this._profile_pic_id;
  set profile_pic_id(String? profile_pic_id) =>
      _$this._profile_pic_id = profile_pic_id;

  String? _profile_pic_url;
  String? get profile_pic_url => _$this._profile_pic_url;
  set profile_pic_url(String? profile_pic_url) =>
      _$this._profile_pic_url = profile_pic_url;

  String? _gender;
  String? get gender => _$this._gender;
  set gender(String? gender) => _$this._gender = gender;

  int? _role_id;
  int? get role_id => _$this._role_id;
  set role_id(int? role_id) => _$this._role_id = role_id;

  String? _role_name;
  String? get role_name => _$this._role_name;
  set role_name(String? role_name) => _$this._role_name = role_name;

  bool? _is_active;
  bool? get is_active => _$this._is_active;
  set is_active(bool? is_active) => _$this._is_active = is_active;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _address;
  String? get address => _$this._address;
  set address(String? address) => _$this._address = address;

  AppUserBuilder();

  AppUserBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _userId = $v.userId;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _email = $v.email;
      _phone_number = $v.phone_number;
      _profile_pic_id = $v.profile_pic_id;
      _profile_pic_url = $v.profile_pic_url;
      _gender = $v.gender;
      _role_id = $v.role_id;
      _role_name = $v.role_name;
      _is_active = $v.is_active;
      _code = $v.code;
      _address = $v.address;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppUser other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AppUser;
  }

  @override
  void update(void Function(AppUserBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AppUser build() => _build();

  _$AppUser _build() {
    final _$result = _$v ??
        new _$AppUser._(
            userId: userId,
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone_number: phone_number,
            profile_pic_id: profile_pic_id,
            profile_pic_url: profile_pic_url,
            gender: gender,
            role_id: role_id,
            role_name: role_name,
            is_active: is_active,
            code: code,
            address: address);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
