import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'app_user.g.dart';

abstract class AppUser implements Built<AppUser, AppUserBuilder> {
  factory AppUser([AppUserBuilder updates(AppUserBuilder builder)]) = _$AppUser;

  AppUser._();

  @BuiltValueField(wireName: 'id')
  int? get userId;

  @BuiltValueField(wireName: 'firstname')
  String? get firstName;

  @BuiltValueField(wireName: 'lastname')
  String? get lastName;

  String? get email;

  String? get phone_number;

  String? get profile_pic_id;

  String? get profile_pic_url;

  String? get gender;

  int? get role_id;

  String? get role_name;

  bool? get is_active;

  String? get code;

  // @BuiltList()
  // List? get preferred_channel;

  String? get address;

  // List? get branches;

  // List? get centers;

  static Serializer<AppUser> get serializer => _$appUserSerializer;
}
