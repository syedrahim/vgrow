// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(AccessToken.serializer)
      ..add(AdditionalDetailsJson.serializer)
      ..add(Address.serializer)
      ..add(AddressAttachments.serializer)
      ..add(ApiError.serializer)
      ..add(ApiSuccess.serializer)
      ..add(AppUser.serializer)
      ..add(BankDetail.serializer)
      ..add(Branch.serializer)
      ..add(CreatedBy.serializer)
      ..add(CrifScore.serializer)
      ..add(Customer.serializer)
      ..add(EligibilityDetail.serializer)
      ..add(ExistingLoan.serializer)
      ..add(FamilyDetail.serializer)
      ..add(FamilyMember.serializer)
      ..add(FileAttachment.serializer)
      ..add(LoanAddress.serializer)
      ..add(LoanAddressAttachment.serializer)
      ..add(LoanCustomer.serializer)
      ..add(LoanDocument.serializer)
      ..add(LoanSummary.serializer)
      ..add(Loans.serializer)
      ..add(Notifications.serializer)
      ..add(OwnedEntity.serializer)
      ..add(PageLinks.serializer)
      ..add(Pagination.serializer)
      ..add(RecentLoanStatus.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AddressAttachments)]),
          () => new ListBuilder<AddressAttachments>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType(LoanAddressAttachment)]),
          () => new ListBuilder<LoanAddressAttachment>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(LoanDocument)]),
          () => new ListBuilder<LoanDocument>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(OwnedEntity)]),
          () => new ListBuilder<OwnedEntity>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(FamilyMember)]),
          () => new ListBuilder<FamilyMember>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ExistingLoan)]),
          () => new ListBuilder<ExistingLoan>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Loans)]),
          () => new ListBuilder<Loans>())
      ..addBuilderFactory(
          const FullType(
              BuiltList, const [const FullType.nullable(Notifications)]),
          () => new ListBuilder<Notifications?>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
