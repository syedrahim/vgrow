import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import '../loan_summary/created_by.dart';

part 'loans.g.dart';

abstract class Loans implements Built<Loans, LoansBuilder> {
  Loans._();
  factory Loans([updates(LoansBuilder b)]) = _$Loans;

  static Serializer<Loans> get serializer => _$loansSerializer;

  int? get id;
  String? get code;

  @BuiltValueField(wireName: 'center_id')
  int? get centerId;

  @BuiltValueField(wireName: 'center_name')
  String? get centerName;

  @BuiltValueField(wireName: 'created_by')
  CreatedBy? get createdBy;

  @BuiltValueField(wireName: 'created_by_id')
  int? get createdById;
  @BuiltValueField(wireName: 'loan_type_id')
  int? get loanTypeId;
  @BuiltValueField(wireName: 'loan_type_name')
  String? get loanTypeName;
  @BuiltValueField(wireName: 'loan_type_code')
  String? get loanTypeCode;
  @BuiltValueField(wireName: 'estimated_cost')
  double? get estimatedCost;
  @BuiltValueField(wireName: 'requested_loan_amt')
  double? get requestedLoanAmt;
  Null? get currency;
  String? get status;
  @BuiltValueField(wireName: 'loan_purpose_id')
  int? get loanPurposeId;
  @BuiltValueField(wireName: 'loan_purpose_name')
  String? get loanPurposeName;
  @BuiltValueField(wireName: 'organisation_id')
  int? get organisationId;
  @BuiltValueField(wireName: 'organisation_name')
  String? get organisationName;
  Null? get group;
  @BuiltValueField(wireName: 'cro_picture_id')
  int? get croPictureId;
  @BuiltValueField(wireName: 'current_step')
  String? get currentStep;
  @BuiltValueField(wireName: 'tenure_in_months')
  int? get tenureInMonths;
  @BuiltValueField(wireName: 'interest_rate')
  double? get interestRate;
  @BuiltValueField(wireName: 'expected_start_date')
  String? get expectedStartDate;
  @BuiltValueField(wireName: 'bm_picture_id')
  int? get bmPictureId;
  @BuiltValueField(wireName: 'ia_picture_id')
  int? get iaPictureId;
  @BuiltValueField(wireName: 'rejection_reason')
  Null? get rejectionReason;
  @BuiltValueField(wireName: 'submitted_at')
  Null? get submittedAt;

  @BuiltValueField(wireName: 'recent_loan_status')
  RecentLoanStatus? get recentLoanStatus;

  @BuiltValueField(wireName: 'branch')
  Branch? get branch;

  @BuiltValueField(wireName: 'batch_id')
  String? get batchId;

  @BuiltValueField(wireName: 'customer')
  Customer? get customer;
}

abstract class RecentLoanStatus
    implements Built<RecentLoanStatus, RecentLoanStatusBuilder> {
  RecentLoanStatus._();
  factory RecentLoanStatus([updates(RecentLoanStatusBuilder b)]) =
      _$RecentLoanStatus;

  static Serializer<RecentLoanStatus> get serializer =>
      _$recentLoanStatusSerializer;

  int? get id;

  @BuiltValueField(wireName: 'loan_id')
  int? get loanId;
  @BuiltValueField(wireName: 'loan_status')
  String? get loanStatus;
  @BuiltValueField(wireName: 'created_at')
  String? get createdAt;
  @BuiltValueField(wireName: 'updated_at')
  String? get updatedAt;
  @BuiltValueField(wireName: 'assigned_at')
  String? get assignedAt;
}

abstract class Branch implements Built<Branch, BranchBuilder> {
  Branch._();
  factory Branch([updates(BranchBuilder b)]) = _$Branch;

  static Serializer<Branch> get serializer => _$branchSerializer;

  int? get id;
  String? get name;
  String? get code;

  @BuiltValueField(wireName: 'organisation_name')
  String? get organisationName;
}

abstract class Customer implements Built<Customer, CustomerBuilder> {
  Customer._();
  factory Customer([updates(CustomerBuilder b)]) = _$Customer;

  static Serializer<Customer> get serializer => _$customerSerializer;

  int? get id;
  String? get code;

  @BuiltValueField(wireName: 'firstname')
  String? get firstname;
  @BuiltValueField(wireName: 'lastname')
  Null? get lastname;
  @BuiltValueField(wireName: 'profile_pic_id')
  int? get profilePicId;
  @BuiltValueField(wireName: 'phone_number')
  String? get phoneNumber;
  @BuiltValueField(wireName: 'address')
  Address? get address;
}

abstract class Address implements Built<Address, AddressBuilder> {
  Address._();
  factory Address([updates(AddressBuilder b)]) = _$Address;

  static Serializer<Address> get serializer => _$addressSerializer;

  int? get id;
  @BuiltValueField(wireName: 'addressable_type')
  String? get addressableType;
  @BuiltValueField(wireName: 'addressable_id')
  int? get addressableId;
  @BuiltValueField(wireName: 'address_line_1')
  String? get addressLine1;
  @BuiltValueField(wireName: 'address_line_2')
  String? get addressLine2;
  @BuiltValueField(wireName: 'city_id')
  int? get cityId;
  @BuiltValueField(wireName: 'city_name')
  String? get cityName;
  @BuiltValueField(wireName: 'state_id')
  int? get stateId;
  @BuiltValueField(wireName: 'state_name')
  String? get stateName;
  @BuiltValueField(wireName: 'pincode')
  String? get pincode;
  Null? get landmark;
  double? get latitude;
  double? get longitude;
  @BuiltValueField(wireName: 'address_attachments')
  BuiltList<AddressAttachments>? get addressAttachments;
}

abstract class AddressAttachments
    implements Built<AddressAttachments, AddressAttachmentsBuilder> {
  AddressAttachments._();
  factory AddressAttachments([updates(AddressAttachmentsBuilder b)]) =
      _$AddressAttachments;

  static Serializer<AddressAttachments> get serializer =>
      _$addressAttachmentsSerializer;

  int? get id;
  @BuiltValueField(wireName: 'attachment_id')
  int? get attachmentId;
  @BuiltValueField(wireName: 'attachment_url')
  String? get attachmentUrl;
  @BuiltValueField(wireName: 'employee_role_code')
  String? get employeeRoleCode;
  @BuiltValueField(wireName: 'employee_id')
  int? get employeeId;
}
