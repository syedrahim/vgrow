// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loans.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Loans> _$loansSerializer = new _$LoansSerializer();
Serializer<RecentLoanStatus> _$recentLoanStatusSerializer =
    new _$RecentLoanStatusSerializer();
Serializer<Branch> _$branchSerializer = new _$BranchSerializer();
Serializer<Customer> _$customerSerializer = new _$CustomerSerializer();
Serializer<Address> _$addressSerializer = new _$AddressSerializer();
Serializer<AddressAttachments> _$addressAttachmentsSerializer =
    new _$AddressAttachmentsSerializer();

class _$LoansSerializer implements StructuredSerializer<Loans> {
  @override
  final Iterable<Type> types = const [Loans, _$Loans];
  @override
  final String wireName = 'Loans';

  @override
  Iterable<Object?> serialize(Serializers serializers, Loans object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.centerId;
    if (value != null) {
      result
        ..add('center_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.centerName;
    if (value != null) {
      result
        ..add('center_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdBy;
    if (value != null) {
      result
        ..add('created_by')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CreatedBy)));
    }
    value = object.createdById;
    if (value != null) {
      result
        ..add('created_by_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanTypeId;
    if (value != null) {
      result
        ..add('loan_type_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanTypeName;
    if (value != null) {
      result
        ..add('loan_type_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.loanTypeCode;
    if (value != null) {
      result
        ..add('loan_type_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.estimatedCost;
    if (value != null) {
      result
        ..add('estimated_cost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.requestedLoanAmt;
    if (value != null) {
      result
        ..add('requested_loan_amt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.currency;
    if (value != null) {
      result
        ..add('currency')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.loanPurposeId;
    if (value != null) {
      result
        ..add('loan_purpose_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanPurposeName;
    if (value != null) {
      result
        ..add('loan_purpose_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.organisationId;
    if (value != null) {
      result
        ..add('organisation_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.organisationName;
    if (value != null) {
      result
        ..add('organisation_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.group;
    if (value != null) {
      result
        ..add('group')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.croPictureId;
    if (value != null) {
      result
        ..add('cro_picture_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.currentStep;
    if (value != null) {
      result
        ..add('current_step')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.tenureInMonths;
    if (value != null) {
      result
        ..add('tenure_in_months')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.interestRate;
    if (value != null) {
      result
        ..add('interest_rate')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.expectedStartDate;
    if (value != null) {
      result
        ..add('expected_start_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bmPictureId;
    if (value != null) {
      result
        ..add('bm_picture_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.iaPictureId;
    if (value != null) {
      result
        ..add('ia_picture_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.rejectionReason;
    if (value != null) {
      result
        ..add('rejection_reason')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.submittedAt;
    if (value != null) {
      result
        ..add('submitted_at')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.recentLoanStatus;
    if (value != null) {
      result
        ..add('recent_loan_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(RecentLoanStatus)));
    }
    value = object.branch;
    if (value != null) {
      result
        ..add('branch')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Branch)));
    }
    value = object.batchId;
    if (value != null) {
      result
        ..add('batch_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.customer;
    if (value != null) {
      result
        ..add('customer')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Customer)));
    }
    return result;
  }

  @override
  Loans deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoansBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'center_id':
          result.centerId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'center_name':
          result.centerName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_by':
          result.createdBy.replace(serializers.deserialize(value,
              specifiedType: const FullType(CreatedBy))! as CreatedBy);
          break;
        case 'created_by_id':
          result.createdById = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_type_id':
          result.loanTypeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_type_name':
          result.loanTypeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'loan_type_code':
          result.loanTypeCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'estimated_cost':
          result.estimatedCost = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'requested_loan_amt':
          result.requestedLoanAmt = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'currency':
          result.currency = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'loan_purpose_id':
          result.loanPurposeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_purpose_name':
          result.loanPurposeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'organisation_id':
          result.organisationId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'organisation_name':
          result.organisationName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'group':
          result.group = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'cro_picture_id':
          result.croPictureId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'current_step':
          result.currentStep = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'tenure_in_months':
          result.tenureInMonths = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'interest_rate':
          result.interestRate = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'expected_start_date':
          result.expectedStartDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bm_picture_id':
          result.bmPictureId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'ia_picture_id':
          result.iaPictureId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'rejection_reason':
          result.rejectionReason = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'submitted_at':
          result.submittedAt = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'recent_loan_status':
          result.recentLoanStatus.replace(serializers.deserialize(value,
                  specifiedType: const FullType(RecentLoanStatus))!
              as RecentLoanStatus);
          break;
        case 'branch':
          result.branch.replace(serializers.deserialize(value,
              specifiedType: const FullType(Branch))! as Branch);
          break;
        case 'batch_id':
          result.batchId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'customer':
          result.customer.replace(serializers.deserialize(value,
              specifiedType: const FullType(Customer))! as Customer);
          break;
      }
    }

    return result.build();
  }
}

class _$RecentLoanStatusSerializer
    implements StructuredSerializer<RecentLoanStatus> {
  @override
  final Iterable<Type> types = const [RecentLoanStatus, _$RecentLoanStatus];
  @override
  final String wireName = 'RecentLoanStatus';

  @override
  Iterable<Object?> serialize(Serializers serializers, RecentLoanStatus object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanId;
    if (value != null) {
      result
        ..add('loan_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanStatus;
    if (value != null) {
      result
        ..add('loan_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdAt;
    if (value != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.updatedAt;
    if (value != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.assignedAt;
    if (value != null) {
      result
        ..add('assigned_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RecentLoanStatus deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RecentLoanStatusBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_id':
          result.loanId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_status':
          result.loanStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'assigned_at':
          result.assignedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$BranchSerializer implements StructuredSerializer<Branch> {
  @override
  final Iterable<Type> types = const [Branch, _$Branch];
  @override
  final String wireName = 'Branch';

  @override
  Iterable<Object?> serialize(Serializers serializers, Branch object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.organisationName;
    if (value != null) {
      result
        ..add('organisation_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Branch deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BranchBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'organisation_name':
          result.organisationName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$CustomerSerializer implements StructuredSerializer<Customer> {
  @override
  final Iterable<Type> types = const [Customer, _$Customer];
  @override
  final String wireName = 'Customer';

  @override
  Iterable<Object?> serialize(Serializers serializers, Customer object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.firstname;
    if (value != null) {
      result
        ..add('firstname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastname;
    if (value != null) {
      result
        ..add('lastname')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.profilePicId;
    if (value != null) {
      result
        ..add('profile_pic_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.phoneNumber;
    if (value != null) {
      result
        ..add('phone_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.address;
    if (value != null) {
      result
        ..add('address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Address)));
    }
    return result;
  }

  @override
  Customer deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CustomerBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'firstname':
          result.firstname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'lastname':
          result.lastname = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'profile_pic_id':
          result.profilePicId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'phone_number':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address':
          result.address.replace(serializers.deserialize(value,
              specifiedType: const FullType(Address))! as Address);
          break;
      }
    }

    return result.build();
  }
}

class _$AddressSerializer implements StructuredSerializer<Address> {
  @override
  final Iterable<Type> types = const [Address, _$Address];
  @override
  final String wireName = 'Address';

  @override
  Iterable<Object?> serialize(Serializers serializers, Address object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.addressableType;
    if (value != null) {
      result
        ..add('addressable_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.addressableId;
    if (value != null) {
      result
        ..add('addressable_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.addressLine1;
    if (value != null) {
      result
        ..add('address_line_1')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.addressLine2;
    if (value != null) {
      result
        ..add('address_line_2')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.cityId;
    if (value != null) {
      result
        ..add('city_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.cityName;
    if (value != null) {
      result
        ..add('city_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.stateId;
    if (value != null) {
      result
        ..add('state_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.stateName;
    if (value != null) {
      result
        ..add('state_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.pincode;
    if (value != null) {
      result
        ..add('pincode')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.landmark;
    if (value != null) {
      result
        ..add('landmark')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.latitude;
    if (value != null) {
      result
        ..add('latitude')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.longitude;
    if (value != null) {
      result
        ..add('longitude')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.addressAttachments;
    if (value != null) {
      result
        ..add('address_attachments')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(AddressAttachments)])));
    }
    return result;
  }

  @override
  Address deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddressBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'addressable_type':
          result.addressableType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'addressable_id':
          result.addressableId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'address_line_1':
          result.addressLine1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address_line_2':
          result.addressLine2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'city_id':
          result.cityId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'city_name':
          result.cityName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'state_id':
          result.stateId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'state_name':
          result.stateName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pincode':
          result.pincode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'landmark':
          result.landmark = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'address_attachments':
          result.addressAttachments.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(AddressAttachments)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$AddressAttachmentsSerializer
    implements StructuredSerializer<AddressAttachments> {
  @override
  final Iterable<Type> types = const [AddressAttachments, _$AddressAttachments];
  @override
  final String wireName = 'AddressAttachments';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, AddressAttachments object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.attachmentId;
    if (value != null) {
      result
        ..add('attachment_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.attachmentUrl;
    if (value != null) {
      result
        ..add('attachment_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.employeeRoleCode;
    if (value != null) {
      result
        ..add('employee_role_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.employeeId;
    if (value != null) {
      result
        ..add('employee_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  AddressAttachments deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AddressAttachmentsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'attachment_id':
          result.attachmentId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'attachment_url':
          result.attachmentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'employee_role_code':
          result.employeeRoleCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'employee_id':
          result.employeeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$Loans extends Loans {
  @override
  final int? id;
  @override
  final String? code;
  @override
  final int? centerId;
  @override
  final String? centerName;
  @override
  final CreatedBy? createdBy;
  @override
  final int? createdById;
  @override
  final int? loanTypeId;
  @override
  final String? loanTypeName;
  @override
  final String? loanTypeCode;
  @override
  final double? estimatedCost;
  @override
  final double? requestedLoanAmt;
  @override
  final Null? currency;
  @override
  final String? status;
  @override
  final int? loanPurposeId;
  @override
  final String? loanPurposeName;
  @override
  final int? organisationId;
  @override
  final String? organisationName;
  @override
  final Null? group;
  @override
  final int? croPictureId;
  @override
  final String? currentStep;
  @override
  final int? tenureInMonths;
  @override
  final double? interestRate;
  @override
  final String? expectedStartDate;
  @override
  final int? bmPictureId;
  @override
  final int? iaPictureId;
  @override
  final Null? rejectionReason;
  @override
  final Null? submittedAt;
  @override
  final RecentLoanStatus? recentLoanStatus;
  @override
  final Branch? branch;
  @override
  final String? batchId;
  @override
  final Customer? customer;

  factory _$Loans([void Function(LoansBuilder)? updates]) =>
      (new LoansBuilder()..update(updates))._build();

  _$Loans._(
      {this.id,
      this.code,
      this.centerId,
      this.centerName,
      this.createdBy,
      this.createdById,
      this.loanTypeId,
      this.loanTypeName,
      this.loanTypeCode,
      this.estimatedCost,
      this.requestedLoanAmt,
      this.currency,
      this.status,
      this.loanPurposeId,
      this.loanPurposeName,
      this.organisationId,
      this.organisationName,
      this.group,
      this.croPictureId,
      this.currentStep,
      this.tenureInMonths,
      this.interestRate,
      this.expectedStartDate,
      this.bmPictureId,
      this.iaPictureId,
      this.rejectionReason,
      this.submittedAt,
      this.recentLoanStatus,
      this.branch,
      this.batchId,
      this.customer})
      : super._();

  @override
  Loans rebuild(void Function(LoansBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoansBuilder toBuilder() => new LoansBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Loans &&
        id == other.id &&
        code == other.code &&
        centerId == other.centerId &&
        centerName == other.centerName &&
        createdBy == other.createdBy &&
        createdById == other.createdById &&
        loanTypeId == other.loanTypeId &&
        loanTypeName == other.loanTypeName &&
        loanTypeCode == other.loanTypeCode &&
        estimatedCost == other.estimatedCost &&
        requestedLoanAmt == other.requestedLoanAmt &&
        currency == other.currency &&
        status == other.status &&
        loanPurposeId == other.loanPurposeId &&
        loanPurposeName == other.loanPurposeName &&
        organisationId == other.organisationId &&
        organisationName == other.organisationName &&
        group == other.group &&
        croPictureId == other.croPictureId &&
        currentStep == other.currentStep &&
        tenureInMonths == other.tenureInMonths &&
        interestRate == other.interestRate &&
        expectedStartDate == other.expectedStartDate &&
        bmPictureId == other.bmPictureId &&
        iaPictureId == other.iaPictureId &&
        rejectionReason == other.rejectionReason &&
        submittedAt == other.submittedAt &&
        recentLoanStatus == other.recentLoanStatus &&
        branch == other.branch &&
        batchId == other.batchId &&
        customer == other.customer;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, centerId.hashCode);
    _$hash = $jc(_$hash, centerName.hashCode);
    _$hash = $jc(_$hash, createdBy.hashCode);
    _$hash = $jc(_$hash, createdById.hashCode);
    _$hash = $jc(_$hash, loanTypeId.hashCode);
    _$hash = $jc(_$hash, loanTypeName.hashCode);
    _$hash = $jc(_$hash, loanTypeCode.hashCode);
    _$hash = $jc(_$hash, estimatedCost.hashCode);
    _$hash = $jc(_$hash, requestedLoanAmt.hashCode);
    _$hash = $jc(_$hash, currency.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, loanPurposeId.hashCode);
    _$hash = $jc(_$hash, loanPurposeName.hashCode);
    _$hash = $jc(_$hash, organisationId.hashCode);
    _$hash = $jc(_$hash, organisationName.hashCode);
    _$hash = $jc(_$hash, group.hashCode);
    _$hash = $jc(_$hash, croPictureId.hashCode);
    _$hash = $jc(_$hash, currentStep.hashCode);
    _$hash = $jc(_$hash, tenureInMonths.hashCode);
    _$hash = $jc(_$hash, interestRate.hashCode);
    _$hash = $jc(_$hash, expectedStartDate.hashCode);
    _$hash = $jc(_$hash, bmPictureId.hashCode);
    _$hash = $jc(_$hash, iaPictureId.hashCode);
    _$hash = $jc(_$hash, rejectionReason.hashCode);
    _$hash = $jc(_$hash, submittedAt.hashCode);
    _$hash = $jc(_$hash, recentLoanStatus.hashCode);
    _$hash = $jc(_$hash, branch.hashCode);
    _$hash = $jc(_$hash, batchId.hashCode);
    _$hash = $jc(_$hash, customer.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Loans')
          ..add('id', id)
          ..add('code', code)
          ..add('centerId', centerId)
          ..add('centerName', centerName)
          ..add('createdBy', createdBy)
          ..add('createdById', createdById)
          ..add('loanTypeId', loanTypeId)
          ..add('loanTypeName', loanTypeName)
          ..add('loanTypeCode', loanTypeCode)
          ..add('estimatedCost', estimatedCost)
          ..add('requestedLoanAmt', requestedLoanAmt)
          ..add('currency', currency)
          ..add('status', status)
          ..add('loanPurposeId', loanPurposeId)
          ..add('loanPurposeName', loanPurposeName)
          ..add('organisationId', organisationId)
          ..add('organisationName', organisationName)
          ..add('group', group)
          ..add('croPictureId', croPictureId)
          ..add('currentStep', currentStep)
          ..add('tenureInMonths', tenureInMonths)
          ..add('interestRate', interestRate)
          ..add('expectedStartDate', expectedStartDate)
          ..add('bmPictureId', bmPictureId)
          ..add('iaPictureId', iaPictureId)
          ..add('rejectionReason', rejectionReason)
          ..add('submittedAt', submittedAt)
          ..add('recentLoanStatus', recentLoanStatus)
          ..add('branch', branch)
          ..add('batchId', batchId)
          ..add('customer', customer))
        .toString();
  }
}

class LoansBuilder implements Builder<Loans, LoansBuilder> {
  _$Loans? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  int? _centerId;
  int? get centerId => _$this._centerId;
  set centerId(int? centerId) => _$this._centerId = centerId;

  String? _centerName;
  String? get centerName => _$this._centerName;
  set centerName(String? centerName) => _$this._centerName = centerName;

  CreatedByBuilder? _createdBy;
  CreatedByBuilder get createdBy =>
      _$this._createdBy ??= new CreatedByBuilder();
  set createdBy(CreatedByBuilder? createdBy) => _$this._createdBy = createdBy;

  int? _createdById;
  int? get createdById => _$this._createdById;
  set createdById(int? createdById) => _$this._createdById = createdById;

  int? _loanTypeId;
  int? get loanTypeId => _$this._loanTypeId;
  set loanTypeId(int? loanTypeId) => _$this._loanTypeId = loanTypeId;

  String? _loanTypeName;
  String? get loanTypeName => _$this._loanTypeName;
  set loanTypeName(String? loanTypeName) => _$this._loanTypeName = loanTypeName;

  String? _loanTypeCode;
  String? get loanTypeCode => _$this._loanTypeCode;
  set loanTypeCode(String? loanTypeCode) => _$this._loanTypeCode = loanTypeCode;

  double? _estimatedCost;
  double? get estimatedCost => _$this._estimatedCost;
  set estimatedCost(double? estimatedCost) =>
      _$this._estimatedCost = estimatedCost;

  double? _requestedLoanAmt;
  double? get requestedLoanAmt => _$this._requestedLoanAmt;
  set requestedLoanAmt(double? requestedLoanAmt) =>
      _$this._requestedLoanAmt = requestedLoanAmt;

  Null? _currency;
  Null? get currency => _$this._currency;
  set currency(Null? currency) => _$this._currency = currency;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  int? _loanPurposeId;
  int? get loanPurposeId => _$this._loanPurposeId;
  set loanPurposeId(int? loanPurposeId) =>
      _$this._loanPurposeId = loanPurposeId;

  String? _loanPurposeName;
  String? get loanPurposeName => _$this._loanPurposeName;
  set loanPurposeName(String? loanPurposeName) =>
      _$this._loanPurposeName = loanPurposeName;

  int? _organisationId;
  int? get organisationId => _$this._organisationId;
  set organisationId(int? organisationId) =>
      _$this._organisationId = organisationId;

  String? _organisationName;
  String? get organisationName => _$this._organisationName;
  set organisationName(String? organisationName) =>
      _$this._organisationName = organisationName;

  Null? _group;
  Null? get group => _$this._group;
  set group(Null? group) => _$this._group = group;

  int? _croPictureId;
  int? get croPictureId => _$this._croPictureId;
  set croPictureId(int? croPictureId) => _$this._croPictureId = croPictureId;

  String? _currentStep;
  String? get currentStep => _$this._currentStep;
  set currentStep(String? currentStep) => _$this._currentStep = currentStep;

  int? _tenureInMonths;
  int? get tenureInMonths => _$this._tenureInMonths;
  set tenureInMonths(int? tenureInMonths) =>
      _$this._tenureInMonths = tenureInMonths;

  double? _interestRate;
  double? get interestRate => _$this._interestRate;
  set interestRate(double? interestRate) => _$this._interestRate = interestRate;

  String? _expectedStartDate;
  String? get expectedStartDate => _$this._expectedStartDate;
  set expectedStartDate(String? expectedStartDate) =>
      _$this._expectedStartDate = expectedStartDate;

  int? _bmPictureId;
  int? get bmPictureId => _$this._bmPictureId;
  set bmPictureId(int? bmPictureId) => _$this._bmPictureId = bmPictureId;

  int? _iaPictureId;
  int? get iaPictureId => _$this._iaPictureId;
  set iaPictureId(int? iaPictureId) => _$this._iaPictureId = iaPictureId;

  Null? _rejectionReason;
  Null? get rejectionReason => _$this._rejectionReason;
  set rejectionReason(Null? rejectionReason) =>
      _$this._rejectionReason = rejectionReason;

  Null? _submittedAt;
  Null? get submittedAt => _$this._submittedAt;
  set submittedAt(Null? submittedAt) => _$this._submittedAt = submittedAt;

  RecentLoanStatusBuilder? _recentLoanStatus;
  RecentLoanStatusBuilder get recentLoanStatus =>
      _$this._recentLoanStatus ??= new RecentLoanStatusBuilder();
  set recentLoanStatus(RecentLoanStatusBuilder? recentLoanStatus) =>
      _$this._recentLoanStatus = recentLoanStatus;

  BranchBuilder? _branch;
  BranchBuilder get branch => _$this._branch ??= new BranchBuilder();
  set branch(BranchBuilder? branch) => _$this._branch = branch;

  String? _batchId;
  String? get batchId => _$this._batchId;
  set batchId(String? batchId) => _$this._batchId = batchId;

  CustomerBuilder? _customer;
  CustomerBuilder get customer => _$this._customer ??= new CustomerBuilder();
  set customer(CustomerBuilder? customer) => _$this._customer = customer;

  LoansBuilder();

  LoansBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _code = $v.code;
      _centerId = $v.centerId;
      _centerName = $v.centerName;
      _createdBy = $v.createdBy?.toBuilder();
      _createdById = $v.createdById;
      _loanTypeId = $v.loanTypeId;
      _loanTypeName = $v.loanTypeName;
      _loanTypeCode = $v.loanTypeCode;
      _estimatedCost = $v.estimatedCost;
      _requestedLoanAmt = $v.requestedLoanAmt;
      _currency = $v.currency;
      _status = $v.status;
      _loanPurposeId = $v.loanPurposeId;
      _loanPurposeName = $v.loanPurposeName;
      _organisationId = $v.organisationId;
      _organisationName = $v.organisationName;
      _group = $v.group;
      _croPictureId = $v.croPictureId;
      _currentStep = $v.currentStep;
      _tenureInMonths = $v.tenureInMonths;
      _interestRate = $v.interestRate;
      _expectedStartDate = $v.expectedStartDate;
      _bmPictureId = $v.bmPictureId;
      _iaPictureId = $v.iaPictureId;
      _rejectionReason = $v.rejectionReason;
      _submittedAt = $v.submittedAt;
      _recentLoanStatus = $v.recentLoanStatus?.toBuilder();
      _branch = $v.branch?.toBuilder();
      _batchId = $v.batchId;
      _customer = $v.customer?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Loans other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Loans;
  }

  @override
  void update(void Function(LoansBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Loans build() => _build();

  _$Loans _build() {
    _$Loans _$result;
    try {
      _$result = _$v ??
          new _$Loans._(
              id: id,
              code: code,
              centerId: centerId,
              centerName: centerName,
              createdBy: _createdBy?.build(),
              createdById: createdById,
              loanTypeId: loanTypeId,
              loanTypeName: loanTypeName,
              loanTypeCode: loanTypeCode,
              estimatedCost: estimatedCost,
              requestedLoanAmt: requestedLoanAmt,
              currency: currency,
              status: status,
              loanPurposeId: loanPurposeId,
              loanPurposeName: loanPurposeName,
              organisationId: organisationId,
              organisationName: organisationName,
              group: group,
              croPictureId: croPictureId,
              currentStep: currentStep,
              tenureInMonths: tenureInMonths,
              interestRate: interestRate,
              expectedStartDate: expectedStartDate,
              bmPictureId: bmPictureId,
              iaPictureId: iaPictureId,
              rejectionReason: rejectionReason,
              submittedAt: submittedAt,
              recentLoanStatus: _recentLoanStatus?.build(),
              branch: _branch?.build(),
              batchId: batchId,
              customer: _customer?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'createdBy';
        _createdBy?.build();

        _$failedField = 'recentLoanStatus';
        _recentLoanStatus?.build();
        _$failedField = 'branch';
        _branch?.build();

        _$failedField = 'customer';
        _customer?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Loans', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$RecentLoanStatus extends RecentLoanStatus {
  @override
  final int? id;
  @override
  final int? loanId;
  @override
  final String? loanStatus;
  @override
  final String? createdAt;
  @override
  final String? updatedAt;
  @override
  final String? assignedAt;

  factory _$RecentLoanStatus(
          [void Function(RecentLoanStatusBuilder)? updates]) =>
      (new RecentLoanStatusBuilder()..update(updates))._build();

  _$RecentLoanStatus._(
      {this.id,
      this.loanId,
      this.loanStatus,
      this.createdAt,
      this.updatedAt,
      this.assignedAt})
      : super._();

  @override
  RecentLoanStatus rebuild(void Function(RecentLoanStatusBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RecentLoanStatusBuilder toBuilder() =>
      new RecentLoanStatusBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RecentLoanStatus &&
        id == other.id &&
        loanId == other.loanId &&
        loanStatus == other.loanStatus &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        assignedAt == other.assignedAt;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, loanId.hashCode);
    _$hash = $jc(_$hash, loanStatus.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, updatedAt.hashCode);
    _$hash = $jc(_$hash, assignedAt.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'RecentLoanStatus')
          ..add('id', id)
          ..add('loanId', loanId)
          ..add('loanStatus', loanStatus)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('assignedAt', assignedAt))
        .toString();
  }
}

class RecentLoanStatusBuilder
    implements Builder<RecentLoanStatus, RecentLoanStatusBuilder> {
  _$RecentLoanStatus? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _loanId;
  int? get loanId => _$this._loanId;
  set loanId(int? loanId) => _$this._loanId = loanId;

  String? _loanStatus;
  String? get loanStatus => _$this._loanStatus;
  set loanStatus(String? loanStatus) => _$this._loanStatus = loanStatus;

  String? _createdAt;
  String? get createdAt => _$this._createdAt;
  set createdAt(String? createdAt) => _$this._createdAt = createdAt;

  String? _updatedAt;
  String? get updatedAt => _$this._updatedAt;
  set updatedAt(String? updatedAt) => _$this._updatedAt = updatedAt;

  String? _assignedAt;
  String? get assignedAt => _$this._assignedAt;
  set assignedAt(String? assignedAt) => _$this._assignedAt = assignedAt;

  RecentLoanStatusBuilder();

  RecentLoanStatusBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _loanId = $v.loanId;
      _loanStatus = $v.loanStatus;
      _createdAt = $v.createdAt;
      _updatedAt = $v.updatedAt;
      _assignedAt = $v.assignedAt;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RecentLoanStatus other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RecentLoanStatus;
  }

  @override
  void update(void Function(RecentLoanStatusBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  RecentLoanStatus build() => _build();

  _$RecentLoanStatus _build() {
    final _$result = _$v ??
        new _$RecentLoanStatus._(
            id: id,
            loanId: loanId,
            loanStatus: loanStatus,
            createdAt: createdAt,
            updatedAt: updatedAt,
            assignedAt: assignedAt);
    replace(_$result);
    return _$result;
  }
}

class _$Branch extends Branch {
  @override
  final int? id;
  @override
  final String? name;
  @override
  final String? code;
  @override
  final String? organisationName;

  factory _$Branch([void Function(BranchBuilder)? updates]) =>
      (new BranchBuilder()..update(updates))._build();

  _$Branch._({this.id, this.name, this.code, this.organisationName})
      : super._();

  @override
  Branch rebuild(void Function(BranchBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BranchBuilder toBuilder() => new BranchBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Branch &&
        id == other.id &&
        name == other.name &&
        code == other.code &&
        organisationName == other.organisationName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, organisationName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Branch')
          ..add('id', id)
          ..add('name', name)
          ..add('code', code)
          ..add('organisationName', organisationName))
        .toString();
  }
}

class BranchBuilder implements Builder<Branch, BranchBuilder> {
  _$Branch? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _organisationName;
  String? get organisationName => _$this._organisationName;
  set organisationName(String? organisationName) =>
      _$this._organisationName = organisationName;

  BranchBuilder();

  BranchBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _code = $v.code;
      _organisationName = $v.organisationName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Branch other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Branch;
  }

  @override
  void update(void Function(BranchBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Branch build() => _build();

  _$Branch _build() {
    final _$result = _$v ??
        new _$Branch._(
            id: id, name: name, code: code, organisationName: organisationName);
    replace(_$result);
    return _$result;
  }
}

class _$Customer extends Customer {
  @override
  final int? id;
  @override
  final String? code;
  @override
  final String? firstname;
  @override
  final Null? lastname;
  @override
  final int? profilePicId;
  @override
  final String? phoneNumber;
  @override
  final Address? address;

  factory _$Customer([void Function(CustomerBuilder)? updates]) =>
      (new CustomerBuilder()..update(updates))._build();

  _$Customer._(
      {this.id,
      this.code,
      this.firstname,
      this.lastname,
      this.profilePicId,
      this.phoneNumber,
      this.address})
      : super._();

  @override
  Customer rebuild(void Function(CustomerBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CustomerBuilder toBuilder() => new CustomerBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Customer &&
        id == other.id &&
        code == other.code &&
        firstname == other.firstname &&
        lastname == other.lastname &&
        profilePicId == other.profilePicId &&
        phoneNumber == other.phoneNumber &&
        address == other.address;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, firstname.hashCode);
    _$hash = $jc(_$hash, lastname.hashCode);
    _$hash = $jc(_$hash, profilePicId.hashCode);
    _$hash = $jc(_$hash, phoneNumber.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Customer')
          ..add('id', id)
          ..add('code', code)
          ..add('firstname', firstname)
          ..add('lastname', lastname)
          ..add('profilePicId', profilePicId)
          ..add('phoneNumber', phoneNumber)
          ..add('address', address))
        .toString();
  }
}

class CustomerBuilder implements Builder<Customer, CustomerBuilder> {
  _$Customer? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _firstname;
  String? get firstname => _$this._firstname;
  set firstname(String? firstname) => _$this._firstname = firstname;

  Null? _lastname;
  Null? get lastname => _$this._lastname;
  set lastname(Null? lastname) => _$this._lastname = lastname;

  int? _profilePicId;
  int? get profilePicId => _$this._profilePicId;
  set profilePicId(int? profilePicId) => _$this._profilePicId = profilePicId;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  AddressBuilder? _address;
  AddressBuilder get address => _$this._address ??= new AddressBuilder();
  set address(AddressBuilder? address) => _$this._address = address;

  CustomerBuilder();

  CustomerBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _code = $v.code;
      _firstname = $v.firstname;
      _lastname = $v.lastname;
      _profilePicId = $v.profilePicId;
      _phoneNumber = $v.phoneNumber;
      _address = $v.address?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Customer other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Customer;
  }

  @override
  void update(void Function(CustomerBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Customer build() => _build();

  _$Customer _build() {
    _$Customer _$result;
    try {
      _$result = _$v ??
          new _$Customer._(
              id: id,
              code: code,
              firstname: firstname,
              lastname: lastname,
              profilePicId: profilePicId,
              phoneNumber: phoneNumber,
              address: _address?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'address';
        _address?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Customer', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Address extends Address {
  @override
  final int? id;
  @override
  final String? addressableType;
  @override
  final int? addressableId;
  @override
  final String? addressLine1;
  @override
  final String? addressLine2;
  @override
  final int? cityId;
  @override
  final String? cityName;
  @override
  final int? stateId;
  @override
  final String? stateName;
  @override
  final String? pincode;
  @override
  final Null? landmark;
  @override
  final double? latitude;
  @override
  final double? longitude;
  @override
  final BuiltList<AddressAttachments>? addressAttachments;

  factory _$Address([void Function(AddressBuilder)? updates]) =>
      (new AddressBuilder()..update(updates))._build();

  _$Address._(
      {this.id,
      this.addressableType,
      this.addressableId,
      this.addressLine1,
      this.addressLine2,
      this.cityId,
      this.cityName,
      this.stateId,
      this.stateName,
      this.pincode,
      this.landmark,
      this.latitude,
      this.longitude,
      this.addressAttachments})
      : super._();

  @override
  Address rebuild(void Function(AddressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddressBuilder toBuilder() => new AddressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Address &&
        id == other.id &&
        addressableType == other.addressableType &&
        addressableId == other.addressableId &&
        addressLine1 == other.addressLine1 &&
        addressLine2 == other.addressLine2 &&
        cityId == other.cityId &&
        cityName == other.cityName &&
        stateId == other.stateId &&
        stateName == other.stateName &&
        pincode == other.pincode &&
        landmark == other.landmark &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        addressAttachments == other.addressAttachments;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, addressableType.hashCode);
    _$hash = $jc(_$hash, addressableId.hashCode);
    _$hash = $jc(_$hash, addressLine1.hashCode);
    _$hash = $jc(_$hash, addressLine2.hashCode);
    _$hash = $jc(_$hash, cityId.hashCode);
    _$hash = $jc(_$hash, cityName.hashCode);
    _$hash = $jc(_$hash, stateId.hashCode);
    _$hash = $jc(_$hash, stateName.hashCode);
    _$hash = $jc(_$hash, pincode.hashCode);
    _$hash = $jc(_$hash, landmark.hashCode);
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jc(_$hash, addressAttachments.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Address')
          ..add('id', id)
          ..add('addressableType', addressableType)
          ..add('addressableId', addressableId)
          ..add('addressLine1', addressLine1)
          ..add('addressLine2', addressLine2)
          ..add('cityId', cityId)
          ..add('cityName', cityName)
          ..add('stateId', stateId)
          ..add('stateName', stateName)
          ..add('pincode', pincode)
          ..add('landmark', landmark)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('addressAttachments', addressAttachments))
        .toString();
  }
}

class AddressBuilder implements Builder<Address, AddressBuilder> {
  _$Address? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _addressableType;
  String? get addressableType => _$this._addressableType;
  set addressableType(String? addressableType) =>
      _$this._addressableType = addressableType;

  int? _addressableId;
  int? get addressableId => _$this._addressableId;
  set addressableId(int? addressableId) =>
      _$this._addressableId = addressableId;

  String? _addressLine1;
  String? get addressLine1 => _$this._addressLine1;
  set addressLine1(String? addressLine1) => _$this._addressLine1 = addressLine1;

  String? _addressLine2;
  String? get addressLine2 => _$this._addressLine2;
  set addressLine2(String? addressLine2) => _$this._addressLine2 = addressLine2;

  int? _cityId;
  int? get cityId => _$this._cityId;
  set cityId(int? cityId) => _$this._cityId = cityId;

  String? _cityName;
  String? get cityName => _$this._cityName;
  set cityName(String? cityName) => _$this._cityName = cityName;

  int? _stateId;
  int? get stateId => _$this._stateId;
  set stateId(int? stateId) => _$this._stateId = stateId;

  String? _stateName;
  String? get stateName => _$this._stateName;
  set stateName(String? stateName) => _$this._stateName = stateName;

  String? _pincode;
  String? get pincode => _$this._pincode;
  set pincode(String? pincode) => _$this._pincode = pincode;

  Null? _landmark;
  Null? get landmark => _$this._landmark;
  set landmark(Null? landmark) => _$this._landmark = landmark;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  ListBuilder<AddressAttachments>? _addressAttachments;
  ListBuilder<AddressAttachments> get addressAttachments =>
      _$this._addressAttachments ??= new ListBuilder<AddressAttachments>();
  set addressAttachments(ListBuilder<AddressAttachments>? addressAttachments) =>
      _$this._addressAttachments = addressAttachments;

  AddressBuilder();

  AddressBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _addressableType = $v.addressableType;
      _addressableId = $v.addressableId;
      _addressLine1 = $v.addressLine1;
      _addressLine2 = $v.addressLine2;
      _cityId = $v.cityId;
      _cityName = $v.cityName;
      _stateId = $v.stateId;
      _stateName = $v.stateName;
      _pincode = $v.pincode;
      _landmark = $v.landmark;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _addressAttachments = $v.addressAttachments?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Address other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Address;
  }

  @override
  void update(void Function(AddressBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Address build() => _build();

  _$Address _build() {
    _$Address _$result;
    try {
      _$result = _$v ??
          new _$Address._(
              id: id,
              addressableType: addressableType,
              addressableId: addressableId,
              addressLine1: addressLine1,
              addressLine2: addressLine2,
              cityId: cityId,
              cityName: cityName,
              stateId: stateId,
              stateName: stateName,
              pincode: pincode,
              landmark: landmark,
              latitude: latitude,
              longitude: longitude,
              addressAttachments: _addressAttachments?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'addressAttachments';
        _addressAttachments?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Address', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$AddressAttachments extends AddressAttachments {
  @override
  final int? id;
  @override
  final int? attachmentId;
  @override
  final String? attachmentUrl;
  @override
  final String? employeeRoleCode;
  @override
  final int? employeeId;

  factory _$AddressAttachments(
          [void Function(AddressAttachmentsBuilder)? updates]) =>
      (new AddressAttachmentsBuilder()..update(updates))._build();

  _$AddressAttachments._(
      {this.id,
      this.attachmentId,
      this.attachmentUrl,
      this.employeeRoleCode,
      this.employeeId})
      : super._();

  @override
  AddressAttachments rebuild(
          void Function(AddressAttachmentsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddressAttachmentsBuilder toBuilder() =>
      new AddressAttachmentsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddressAttachments &&
        id == other.id &&
        attachmentId == other.attachmentId &&
        attachmentUrl == other.attachmentUrl &&
        employeeRoleCode == other.employeeRoleCode &&
        employeeId == other.employeeId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, attachmentId.hashCode);
    _$hash = $jc(_$hash, attachmentUrl.hashCode);
    _$hash = $jc(_$hash, employeeRoleCode.hashCode);
    _$hash = $jc(_$hash, employeeId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AddressAttachments')
          ..add('id', id)
          ..add('attachmentId', attachmentId)
          ..add('attachmentUrl', attachmentUrl)
          ..add('employeeRoleCode', employeeRoleCode)
          ..add('employeeId', employeeId))
        .toString();
  }
}

class AddressAttachmentsBuilder
    implements Builder<AddressAttachments, AddressAttachmentsBuilder> {
  _$AddressAttachments? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _attachmentId;
  int? get attachmentId => _$this._attachmentId;
  set attachmentId(int? attachmentId) => _$this._attachmentId = attachmentId;

  String? _attachmentUrl;
  String? get attachmentUrl => _$this._attachmentUrl;
  set attachmentUrl(String? attachmentUrl) =>
      _$this._attachmentUrl = attachmentUrl;

  String? _employeeRoleCode;
  String? get employeeRoleCode => _$this._employeeRoleCode;
  set employeeRoleCode(String? employeeRoleCode) =>
      _$this._employeeRoleCode = employeeRoleCode;

  int? _employeeId;
  int? get employeeId => _$this._employeeId;
  set employeeId(int? employeeId) => _$this._employeeId = employeeId;

  AddressAttachmentsBuilder();

  AddressAttachmentsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _attachmentId = $v.attachmentId;
      _attachmentUrl = $v.attachmentUrl;
      _employeeRoleCode = $v.employeeRoleCode;
      _employeeId = $v.employeeId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddressAttachments other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AddressAttachments;
  }

  @override
  void update(void Function(AddressAttachmentsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AddressAttachments build() => _build();

  _$AddressAttachments _build() {
    final _$result = _$v ??
        new _$AddressAttachments._(
            id: id,
            attachmentId: attachmentId,
            attachmentUrl: attachmentUrl,
            employeeRoleCode: employeeRoleCode,
            employeeId: employeeId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
