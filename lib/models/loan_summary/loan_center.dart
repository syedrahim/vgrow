import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'loan_branch.dart';

part 'loan_center.g.dart';

abstract class LoanCenter implements Built<LoanCenter, LoanCenterBuilder>{
  LoanCenter._();
  factory LoanCenter([LoanCenterBuilder updates(LoanCenterBuilder builder)]) = _$LoanCenter;

  int? get id;

  String? get name;

  String? get code;

  LoanBranch? get branch;

  static Serializer<LoanCenter> get serializer => _$loanCenterSerializer;
}


