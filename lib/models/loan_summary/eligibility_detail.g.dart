// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'eligibility_detail.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<EligibilityDetail> _$eligibilityDetailSerializer =
    new _$EligibilityDetailSerializer();

class _$EligibilityDetailSerializer
    implements StructuredSerializer<EligibilityDetail> {
  @override
  final Iterable<Type> types = const [EligibilityDetail, _$EligibilityDetail];
  @override
  final String wireName = 'EligibilityDetail';

  @override
  Iterable<Object?> serialize(Serializers serializers, EligibilityDetail object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanID;
    if (value != null) {
      result
        ..add('loan_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.housingOwnershipStatus;
    if (value != null) {
      result
        ..add('housing_ownership_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.spanOfResidence;
    if (value != null) {
      result
        ..add('span_of_residence')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.areaOfResidence;
    if (value != null) {
      result
        ..add('area_of_residence')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.relationshipTOResidenceOwner;
    if (value != null) {
      result
        ..add('relationship_to_residence_owner')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.typeOfResidence;
    if (value != null) {
      result
        ..add('type_of_residence')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.earningMembersCount;
    if (value != null) {
      result
        ..add('earning_members_count')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.businessBuildingOwnershipStatus;
    if (value != null) {
      result
        ..add('business_building_ownership_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.yearsOfBusiness;
    if (value != null) {
      result
        ..add('years_of_business')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.monthsOfBusiness;
    if (value != null) {
      result
        ..add('months_of_business')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.typeOfBusinessModel;
    if (value != null) {
      result
        ..add('type_of_business_model')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  EligibilityDetail deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new EligibilityDetailBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_id':
          result.loanID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'housing_ownership_status':
          result.housingOwnershipStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'span_of_residence':
          result.spanOfResidence = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'area_of_residence':
          result.areaOfResidence = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'relationship_to_residence_owner':
          result.relationshipTOResidenceOwner = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'type_of_residence':
          result.typeOfResidence = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'earning_members_count':
          result.earningMembersCount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'business_building_ownership_status':
          result.businessBuildingOwnershipStatus = serializers.deserialize(
              value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'years_of_business':
          result.yearsOfBusiness = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'months_of_business':
          result.monthsOfBusiness = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'type_of_business_model':
          result.typeOfBusinessModel = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$EligibilityDetail extends EligibilityDetail {
  @override
  final int? id;
  @override
  final int? loanID;
  @override
  final String? housingOwnershipStatus;
  @override
  final String? spanOfResidence;
  @override
  final int? areaOfResidence;
  @override
  final String? relationshipTOResidenceOwner;
  @override
  final String? typeOfResidence;
  @override
  final int? earningMembersCount;
  @override
  final String? businessBuildingOwnershipStatus;
  @override
  final int? yearsOfBusiness;
  @override
  final int? monthsOfBusiness;
  @override
  final String? typeOfBusinessModel;

  factory _$EligibilityDetail(
          [void Function(EligibilityDetailBuilder)? updates]) =>
      (new EligibilityDetailBuilder()..update(updates))._build();

  _$EligibilityDetail._(
      {this.id,
      this.loanID,
      this.housingOwnershipStatus,
      this.spanOfResidence,
      this.areaOfResidence,
      this.relationshipTOResidenceOwner,
      this.typeOfResidence,
      this.earningMembersCount,
      this.businessBuildingOwnershipStatus,
      this.yearsOfBusiness,
      this.monthsOfBusiness,
      this.typeOfBusinessModel})
      : super._();

  @override
  EligibilityDetail rebuild(void Function(EligibilityDetailBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  EligibilityDetailBuilder toBuilder() =>
      new EligibilityDetailBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is EligibilityDetail &&
        id == other.id &&
        loanID == other.loanID &&
        housingOwnershipStatus == other.housingOwnershipStatus &&
        spanOfResidence == other.spanOfResidence &&
        areaOfResidence == other.areaOfResidence &&
        relationshipTOResidenceOwner == other.relationshipTOResidenceOwner &&
        typeOfResidence == other.typeOfResidence &&
        earningMembersCount == other.earningMembersCount &&
        businessBuildingOwnershipStatus ==
            other.businessBuildingOwnershipStatus &&
        yearsOfBusiness == other.yearsOfBusiness &&
        monthsOfBusiness == other.monthsOfBusiness &&
        typeOfBusinessModel == other.typeOfBusinessModel;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, loanID.hashCode);
    _$hash = $jc(_$hash, housingOwnershipStatus.hashCode);
    _$hash = $jc(_$hash, spanOfResidence.hashCode);
    _$hash = $jc(_$hash, areaOfResidence.hashCode);
    _$hash = $jc(_$hash, relationshipTOResidenceOwner.hashCode);
    _$hash = $jc(_$hash, typeOfResidence.hashCode);
    _$hash = $jc(_$hash, earningMembersCount.hashCode);
    _$hash = $jc(_$hash, businessBuildingOwnershipStatus.hashCode);
    _$hash = $jc(_$hash, yearsOfBusiness.hashCode);
    _$hash = $jc(_$hash, monthsOfBusiness.hashCode);
    _$hash = $jc(_$hash, typeOfBusinessModel.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'EligibilityDetail')
          ..add('id', id)
          ..add('loanID', loanID)
          ..add('housingOwnershipStatus', housingOwnershipStatus)
          ..add('spanOfResidence', spanOfResidence)
          ..add('areaOfResidence', areaOfResidence)
          ..add('relationshipTOResidenceOwner', relationshipTOResidenceOwner)
          ..add('typeOfResidence', typeOfResidence)
          ..add('earningMembersCount', earningMembersCount)
          ..add('businessBuildingOwnershipStatus',
              businessBuildingOwnershipStatus)
          ..add('yearsOfBusiness', yearsOfBusiness)
          ..add('monthsOfBusiness', monthsOfBusiness)
          ..add('typeOfBusinessModel', typeOfBusinessModel))
        .toString();
  }
}

class EligibilityDetailBuilder
    implements Builder<EligibilityDetail, EligibilityDetailBuilder> {
  _$EligibilityDetail? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _loanID;
  int? get loanID => _$this._loanID;
  set loanID(int? loanID) => _$this._loanID = loanID;

  String? _housingOwnershipStatus;
  String? get housingOwnershipStatus => _$this._housingOwnershipStatus;
  set housingOwnershipStatus(String? housingOwnershipStatus) =>
      _$this._housingOwnershipStatus = housingOwnershipStatus;

  String? _spanOfResidence;
  String? get spanOfResidence => _$this._spanOfResidence;
  set spanOfResidence(String? spanOfResidence) =>
      _$this._spanOfResidence = spanOfResidence;

  int? _areaOfResidence;
  int? get areaOfResidence => _$this._areaOfResidence;
  set areaOfResidence(int? areaOfResidence) =>
      _$this._areaOfResidence = areaOfResidence;

  String? _relationshipTOResidenceOwner;
  String? get relationshipTOResidenceOwner =>
      _$this._relationshipTOResidenceOwner;
  set relationshipTOResidenceOwner(String? relationshipTOResidenceOwner) =>
      _$this._relationshipTOResidenceOwner = relationshipTOResidenceOwner;

  String? _typeOfResidence;
  String? get typeOfResidence => _$this._typeOfResidence;
  set typeOfResidence(String? typeOfResidence) =>
      _$this._typeOfResidence = typeOfResidence;

  int? _earningMembersCount;
  int? get earningMembersCount => _$this._earningMembersCount;
  set earningMembersCount(int? earningMembersCount) =>
      _$this._earningMembersCount = earningMembersCount;

  String? _businessBuildingOwnershipStatus;
  String? get businessBuildingOwnershipStatus =>
      _$this._businessBuildingOwnershipStatus;
  set businessBuildingOwnershipStatus(
          String? businessBuildingOwnershipStatus) =>
      _$this._businessBuildingOwnershipStatus = businessBuildingOwnershipStatus;

  int? _yearsOfBusiness;
  int? get yearsOfBusiness => _$this._yearsOfBusiness;
  set yearsOfBusiness(int? yearsOfBusiness) =>
      _$this._yearsOfBusiness = yearsOfBusiness;

  int? _monthsOfBusiness;
  int? get monthsOfBusiness => _$this._monthsOfBusiness;
  set monthsOfBusiness(int? monthsOfBusiness) =>
      _$this._monthsOfBusiness = monthsOfBusiness;

  String? _typeOfBusinessModel;
  String? get typeOfBusinessModel => _$this._typeOfBusinessModel;
  set typeOfBusinessModel(String? typeOfBusinessModel) =>
      _$this._typeOfBusinessModel = typeOfBusinessModel;

  EligibilityDetailBuilder();

  EligibilityDetailBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _loanID = $v.loanID;
      _housingOwnershipStatus = $v.housingOwnershipStatus;
      _spanOfResidence = $v.spanOfResidence;
      _areaOfResidence = $v.areaOfResidence;
      _relationshipTOResidenceOwner = $v.relationshipTOResidenceOwner;
      _typeOfResidence = $v.typeOfResidence;
      _earningMembersCount = $v.earningMembersCount;
      _businessBuildingOwnershipStatus = $v.businessBuildingOwnershipStatus;
      _yearsOfBusiness = $v.yearsOfBusiness;
      _monthsOfBusiness = $v.monthsOfBusiness;
      _typeOfBusinessModel = $v.typeOfBusinessModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(EligibilityDetail other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$EligibilityDetail;
  }

  @override
  void update(void Function(EligibilityDetailBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  EligibilityDetail build() => _build();

  _$EligibilityDetail _build() {
    final _$result = _$v ??
        new _$EligibilityDetail._(
            id: id,
            loanID: loanID,
            housingOwnershipStatus: housingOwnershipStatus,
            spanOfResidence: spanOfResidence,
            areaOfResidence: areaOfResidence,
            relationshipTOResidenceOwner: relationshipTOResidenceOwner,
            typeOfResidence: typeOfResidence,
            earningMembersCount: earningMembersCount,
            businessBuildingOwnershipStatus: businessBuildingOwnershipStatus,
            yearsOfBusiness: yearsOfBusiness,
            monthsOfBusiness: monthsOfBusiness,
            typeOfBusinessModel: typeOfBusinessModel);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
