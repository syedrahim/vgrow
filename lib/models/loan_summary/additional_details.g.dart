// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'additional_details.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AdditionalDetailsJson> _$additionalDetailsJsonSerializer =
    new _$AdditionalDetailsJsonSerializer();

class _$AdditionalDetailsJsonSerializer
    implements StructuredSerializer<AdditionalDetailsJson> {
  @override
  final Iterable<Type> types = const [
    AdditionalDetailsJson,
    _$AdditionalDetailsJson
  ];
  @override
  final String wireName = 'AdditionalDetailsJson';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, AdditionalDetailsJson object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.natureOfBusiness;
    if (value != null) {
      result
        ..add('nature_of_business')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.enterpriseName;
    if (value != null) {
      result
        ..add('enterprise_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.address;
    if (value != null) {
      result
        ..add('address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(LoanAddress)));
    }
    value = object.yearOfEstablishment;
    if (value != null) {
      result
        ..add('year_of_establishment')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.monthlyRevenue;
    if (value != null) {
      result
        ..add('monthly_revenue')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.directCost;
    if (value != null) {
      result
        ..add('direct_cost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.indirectCost;
    if (value != null) {
      result
        ..add('indirect_cost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.noOfGoodsInasset;
    if (value != null) {
      result
        ..add('no_of_goods_in_asset')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.noOfInfrastructureInasset;
    if (value != null) {
      result
        ..add('no_of_infrastructure_in_asset')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.infrastructureInAssetValue;
    if (value != null) {
      result
        ..add('infrastructure_in_asset_value')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.receivableCashOrDebit;
    if (value != null) {
      result
        ..add('receivable_cash_or_debit')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.payableToSuppliers;
    if (value != null) {
      result
        ..add('payables_to_suppliers')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.no_of_infrastructure_in_liability;
    if (value != null) {
      result
        ..add('no_of_infrastructure_in_liability')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.monthly_income;
    if (value != null) {
      result
        ..add('monthly_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.monthly_expense;
    if (value != null) {
      result
        ..add('monthly_expense')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  AdditionalDetailsJson deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AdditionalDetailsJsonBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'nature_of_business':
          result.natureOfBusiness = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'enterprise_name':
          result.enterpriseName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address':
          result.address.replace(serializers.deserialize(value,
              specifiedType: const FullType(LoanAddress))! as LoanAddress);
          break;
        case 'year_of_establishment':
          result.yearOfEstablishment = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'monthly_revenue':
          result.monthlyRevenue = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'direct_cost':
          result.directCost = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'indirect_cost':
          result.indirectCost = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'no_of_goods_in_asset':
          result.noOfGoodsInasset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'no_of_infrastructure_in_asset':
          result.noOfInfrastructureInasset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'infrastructure_in_asset_value':
          result.infrastructureInAssetValue = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'receivable_cash_or_debit':
          result.receivableCashOrDebit = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'payables_to_suppliers':
          result.payableToSuppliers = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'no_of_infrastructure_in_liability':
          result.no_of_infrastructure_in_liability = serializers.deserialize(
              value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'monthly_income':
          result.monthly_income = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'monthly_expense':
          result.monthly_expense = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
      }
    }

    return result.build();
  }
}

class _$AdditionalDetailsJson extends AdditionalDetailsJson {
  @override
  final String? natureOfBusiness;
  @override
  final String? enterpriseName;
  @override
  final LoanAddress? address;
  @override
  final String? yearOfEstablishment;
  @override
  final double? monthlyRevenue;
  @override
  final double? directCost;
  @override
  final double? indirectCost;
  @override
  final int? noOfGoodsInasset;
  @override
  final int? noOfInfrastructureInasset;
  @override
  final int? infrastructureInAssetValue;
  @override
  final double? receivableCashOrDebit;
  @override
  final double? payableToSuppliers;
  @override
  final double? no_of_infrastructure_in_liability;
  @override
  final double? monthly_income;
  @override
  final double? monthly_expense;

  factory _$AdditionalDetailsJson(
          [void Function(AdditionalDetailsJsonBuilder)? updates]) =>
      (new AdditionalDetailsJsonBuilder()..update(updates))._build();

  _$AdditionalDetailsJson._(
      {this.natureOfBusiness,
      this.enterpriseName,
      this.address,
      this.yearOfEstablishment,
      this.monthlyRevenue,
      this.directCost,
      this.indirectCost,
      this.noOfGoodsInasset,
      this.noOfInfrastructureInasset,
      this.infrastructureInAssetValue,
      this.receivableCashOrDebit,
      this.payableToSuppliers,
      this.no_of_infrastructure_in_liability,
      this.monthly_income,
      this.monthly_expense})
      : super._();

  @override
  AdditionalDetailsJson rebuild(
          void Function(AdditionalDetailsJsonBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AdditionalDetailsJsonBuilder toBuilder() =>
      new AdditionalDetailsJsonBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AdditionalDetailsJson &&
        natureOfBusiness == other.natureOfBusiness &&
        enterpriseName == other.enterpriseName &&
        address == other.address &&
        yearOfEstablishment == other.yearOfEstablishment &&
        monthlyRevenue == other.monthlyRevenue &&
        directCost == other.directCost &&
        indirectCost == other.indirectCost &&
        noOfGoodsInasset == other.noOfGoodsInasset &&
        noOfInfrastructureInasset == other.noOfInfrastructureInasset &&
        infrastructureInAssetValue == other.infrastructureInAssetValue &&
        receivableCashOrDebit == other.receivableCashOrDebit &&
        payableToSuppliers == other.payableToSuppliers &&
        no_of_infrastructure_in_liability ==
            other.no_of_infrastructure_in_liability &&
        monthly_income == other.monthly_income &&
        monthly_expense == other.monthly_expense;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, natureOfBusiness.hashCode);
    _$hash = $jc(_$hash, enterpriseName.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jc(_$hash, yearOfEstablishment.hashCode);
    _$hash = $jc(_$hash, monthlyRevenue.hashCode);
    _$hash = $jc(_$hash, directCost.hashCode);
    _$hash = $jc(_$hash, indirectCost.hashCode);
    _$hash = $jc(_$hash, noOfGoodsInasset.hashCode);
    _$hash = $jc(_$hash, noOfInfrastructureInasset.hashCode);
    _$hash = $jc(_$hash, infrastructureInAssetValue.hashCode);
    _$hash = $jc(_$hash, receivableCashOrDebit.hashCode);
    _$hash = $jc(_$hash, payableToSuppliers.hashCode);
    _$hash = $jc(_$hash, no_of_infrastructure_in_liability.hashCode);
    _$hash = $jc(_$hash, monthly_income.hashCode);
    _$hash = $jc(_$hash, monthly_expense.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AdditionalDetailsJson')
          ..add('natureOfBusiness', natureOfBusiness)
          ..add('enterpriseName', enterpriseName)
          ..add('address', address)
          ..add('yearOfEstablishment', yearOfEstablishment)
          ..add('monthlyRevenue', monthlyRevenue)
          ..add('directCost', directCost)
          ..add('indirectCost', indirectCost)
          ..add('noOfGoodsInasset', noOfGoodsInasset)
          ..add('noOfInfrastructureInasset', noOfInfrastructureInasset)
          ..add('infrastructureInAssetValue', infrastructureInAssetValue)
          ..add('receivableCashOrDebit', receivableCashOrDebit)
          ..add('payableToSuppliers', payableToSuppliers)
          ..add('no_of_infrastructure_in_liability',
              no_of_infrastructure_in_liability)
          ..add('monthly_income', monthly_income)
          ..add('monthly_expense', monthly_expense))
        .toString();
  }
}

class AdditionalDetailsJsonBuilder
    implements Builder<AdditionalDetailsJson, AdditionalDetailsJsonBuilder> {
  _$AdditionalDetailsJson? _$v;

  String? _natureOfBusiness;
  String? get natureOfBusiness => _$this._natureOfBusiness;
  set natureOfBusiness(String? natureOfBusiness) =>
      _$this._natureOfBusiness = natureOfBusiness;

  String? _enterpriseName;
  String? get enterpriseName => _$this._enterpriseName;
  set enterpriseName(String? enterpriseName) =>
      _$this._enterpriseName = enterpriseName;

  LoanAddressBuilder? _address;
  LoanAddressBuilder get address =>
      _$this._address ??= new LoanAddressBuilder();
  set address(LoanAddressBuilder? address) => _$this._address = address;

  String? _yearOfEstablishment;
  String? get yearOfEstablishment => _$this._yearOfEstablishment;
  set yearOfEstablishment(String? yearOfEstablishment) =>
      _$this._yearOfEstablishment = yearOfEstablishment;

  double? _monthlyRevenue;
  double? get monthlyRevenue => _$this._monthlyRevenue;
  set monthlyRevenue(double? monthlyRevenue) =>
      _$this._monthlyRevenue = monthlyRevenue;

  double? _directCost;
  double? get directCost => _$this._directCost;
  set directCost(double? directCost) => _$this._directCost = directCost;

  double? _indirectCost;
  double? get indirectCost => _$this._indirectCost;
  set indirectCost(double? indirectCost) => _$this._indirectCost = indirectCost;

  int? _noOfGoodsInasset;
  int? get noOfGoodsInasset => _$this._noOfGoodsInasset;
  set noOfGoodsInasset(int? noOfGoodsInasset) =>
      _$this._noOfGoodsInasset = noOfGoodsInasset;

  int? _noOfInfrastructureInasset;
  int? get noOfInfrastructureInasset => _$this._noOfInfrastructureInasset;
  set noOfInfrastructureInasset(int? noOfInfrastructureInasset) =>
      _$this._noOfInfrastructureInasset = noOfInfrastructureInasset;

  int? _infrastructureInAssetValue;
  int? get infrastructureInAssetValue => _$this._infrastructureInAssetValue;
  set infrastructureInAssetValue(int? infrastructureInAssetValue) =>
      _$this._infrastructureInAssetValue = infrastructureInAssetValue;

  double? _receivableCashOrDebit;
  double? get receivableCashOrDebit => _$this._receivableCashOrDebit;
  set receivableCashOrDebit(double? receivableCashOrDebit) =>
      _$this._receivableCashOrDebit = receivableCashOrDebit;

  double? _payableToSuppliers;
  double? get payableToSuppliers => _$this._payableToSuppliers;
  set payableToSuppliers(double? payableToSuppliers) =>
      _$this._payableToSuppliers = payableToSuppliers;

  double? _no_of_infrastructure_in_liability;
  double? get no_of_infrastructure_in_liability =>
      _$this._no_of_infrastructure_in_liability;
  set no_of_infrastructure_in_liability(
          double? no_of_infrastructure_in_liability) =>
      _$this._no_of_infrastructure_in_liability =
          no_of_infrastructure_in_liability;

  double? _monthly_income;
  double? get monthly_income => _$this._monthly_income;
  set monthly_income(double? monthly_income) =>
      _$this._monthly_income = monthly_income;

  double? _monthly_expense;
  double? get monthly_expense => _$this._monthly_expense;
  set monthly_expense(double? monthly_expense) =>
      _$this._monthly_expense = monthly_expense;

  AdditionalDetailsJsonBuilder();

  AdditionalDetailsJsonBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _natureOfBusiness = $v.natureOfBusiness;
      _enterpriseName = $v.enterpriseName;
      _address = $v.address?.toBuilder();
      _yearOfEstablishment = $v.yearOfEstablishment;
      _monthlyRevenue = $v.monthlyRevenue;
      _directCost = $v.directCost;
      _indirectCost = $v.indirectCost;
      _noOfGoodsInasset = $v.noOfGoodsInasset;
      _noOfInfrastructureInasset = $v.noOfInfrastructureInasset;
      _infrastructureInAssetValue = $v.infrastructureInAssetValue;
      _receivableCashOrDebit = $v.receivableCashOrDebit;
      _payableToSuppliers = $v.payableToSuppliers;
      _no_of_infrastructure_in_liability = $v.no_of_infrastructure_in_liability;
      _monthly_income = $v.monthly_income;
      _monthly_expense = $v.monthly_expense;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AdditionalDetailsJson other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AdditionalDetailsJson;
  }

  @override
  void update(void Function(AdditionalDetailsJsonBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AdditionalDetailsJson build() => _build();

  _$AdditionalDetailsJson _build() {
    _$AdditionalDetailsJson _$result;
    try {
      _$result = _$v ??
          new _$AdditionalDetailsJson._(
              natureOfBusiness: natureOfBusiness,
              enterpriseName: enterpriseName,
              address: _address?.build(),
              yearOfEstablishment: yearOfEstablishment,
              monthlyRevenue: monthlyRevenue,
              directCost: directCost,
              indirectCost: indirectCost,
              noOfGoodsInasset: noOfGoodsInasset,
              noOfInfrastructureInasset: noOfInfrastructureInasset,
              infrastructureInAssetValue: infrastructureInAssetValue,
              receivableCashOrDebit: receivableCashOrDebit,
              payableToSuppliers: payableToSuppliers,
              no_of_infrastructure_in_liability:
                  no_of_infrastructure_in_liability,
              monthly_income: monthly_income,
              monthly_expense: monthly_expense);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'address';
        _address?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'AdditionalDetailsJson', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
