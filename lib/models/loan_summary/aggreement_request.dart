import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'aggreement_request.g.dart';

abstract class AggreementRequest implements Built<AggreementRequest, AggreementRequestBuilder>{
  AggreementRequest._();
  factory AggreementRequest([AggreementRequestBuilder updates(AggreementRequestBuilder builder)]) = _$AggreementRequest;

  @BuiltValueField(wireName: 'document_id')
  int? get documentID;

  @BuiltValueField(wireName: 'requestable_type')
  String? get requestableType;

  @BuiltValueField(wireName: 'requestable_id')
  int? get requestableID;

  String? get status;

  String? get irn;

  static Serializer<AggreementRequest> get serializer => _$aggreementRequestSerializer;
}