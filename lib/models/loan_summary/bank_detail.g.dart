// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bank_detail.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BankDetail> _$bankDetailSerializer = new _$BankDetailSerializer();

class _$BankDetailSerializer implements StructuredSerializer<BankDetail> {
  @override
  final Iterable<Type> types = const [BankDetail, _$BankDetail];
  @override
  final String wireName = 'BankDetail';

  @override
  Iterable<Object?> serialize(Serializers serializers, BankDetail object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.ownerType;
    if (value != null) {
      result
        ..add('owner_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ownerID;
    if (value != null) {
      result
        ..add('owner_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.accountNumber;
    if (value != null) {
      result
        ..add('account_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bankName;
    if (value != null) {
      result
        ..add('bank_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ifscCode;
    if (value != null) {
      result
        ..add('ifsc_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.passbookOrChequeDocumentID;
    if (value != null) {
      result
        ..add('passbook_or_cheque_document_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.passbookOrChequeDocumentUrl;
    if (value != null) {
      result
        ..add('passbook_or_cheque_document_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.branchName;
    if (value != null) {
      result
        ..add('branch_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.registeredName;
    if (value != null) {
      result
        ..add('registered_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.nameMatch;
    if (value != null) {
      result
        ..add('name_match')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.score;
    if (value != null) {
      result
        ..add('score')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BankDetail deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BankDetailBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'owner_type':
          result.ownerType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'owner_id':
          result.ownerID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'account_number':
          result.accountNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bank_name':
          result.bankName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'ifsc_code':
          result.ifscCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'passbook_or_cheque_document_id':
          result.passbookOrChequeDocumentID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'passbook_or_cheque_document_url':
          result.passbookOrChequeDocumentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'branch_name':
          result.branchName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'registered_name':
          result.registeredName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'name_match':
          result.nameMatch = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'score':
          result.score = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$BankDetail extends BankDetail {
  @override
  final int? id;
  @override
  final String? ownerType;
  @override
  final int? ownerID;
  @override
  final String? accountNumber;
  @override
  final String? bankName;
  @override
  final String? ifscCode;
  @override
  final int? passbookOrChequeDocumentID;
  @override
  final String? passbookOrChequeDocumentUrl;
  @override
  final String? branchName;
  @override
  final String? registeredName;
  @override
  final String? nameMatch;
  @override
  final String? score;

  factory _$BankDetail([void Function(BankDetailBuilder)? updates]) =>
      (new BankDetailBuilder()..update(updates))._build();

  _$BankDetail._(
      {this.id,
      this.ownerType,
      this.ownerID,
      this.accountNumber,
      this.bankName,
      this.ifscCode,
      this.passbookOrChequeDocumentID,
      this.passbookOrChequeDocumentUrl,
      this.branchName,
      this.registeredName,
      this.nameMatch,
      this.score})
      : super._();

  @override
  BankDetail rebuild(void Function(BankDetailBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BankDetailBuilder toBuilder() => new BankDetailBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BankDetail &&
        id == other.id &&
        ownerType == other.ownerType &&
        ownerID == other.ownerID &&
        accountNumber == other.accountNumber &&
        bankName == other.bankName &&
        ifscCode == other.ifscCode &&
        passbookOrChequeDocumentID == other.passbookOrChequeDocumentID &&
        passbookOrChequeDocumentUrl == other.passbookOrChequeDocumentUrl &&
        branchName == other.branchName &&
        registeredName == other.registeredName &&
        nameMatch == other.nameMatch &&
        score == other.score;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, ownerType.hashCode);
    _$hash = $jc(_$hash, ownerID.hashCode);
    _$hash = $jc(_$hash, accountNumber.hashCode);
    _$hash = $jc(_$hash, bankName.hashCode);
    _$hash = $jc(_$hash, ifscCode.hashCode);
    _$hash = $jc(_$hash, passbookOrChequeDocumentID.hashCode);
    _$hash = $jc(_$hash, passbookOrChequeDocumentUrl.hashCode);
    _$hash = $jc(_$hash, branchName.hashCode);
    _$hash = $jc(_$hash, registeredName.hashCode);
    _$hash = $jc(_$hash, nameMatch.hashCode);
    _$hash = $jc(_$hash, score.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BankDetail')
          ..add('id', id)
          ..add('ownerType', ownerType)
          ..add('ownerID', ownerID)
          ..add('accountNumber', accountNumber)
          ..add('bankName', bankName)
          ..add('ifscCode', ifscCode)
          ..add('passbookOrChequeDocumentID', passbookOrChequeDocumentID)
          ..add('passbookOrChequeDocumentUrl', passbookOrChequeDocumentUrl)
          ..add('branchName', branchName)
          ..add('registeredName', registeredName)
          ..add('nameMatch', nameMatch)
          ..add('score', score))
        .toString();
  }
}

class BankDetailBuilder implements Builder<BankDetail, BankDetailBuilder> {
  _$BankDetail? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _ownerType;
  String? get ownerType => _$this._ownerType;
  set ownerType(String? ownerType) => _$this._ownerType = ownerType;

  int? _ownerID;
  int? get ownerID => _$this._ownerID;
  set ownerID(int? ownerID) => _$this._ownerID = ownerID;

  String? _accountNumber;
  String? get accountNumber => _$this._accountNumber;
  set accountNumber(String? accountNumber) =>
      _$this._accountNumber = accountNumber;

  String? _bankName;
  String? get bankName => _$this._bankName;
  set bankName(String? bankName) => _$this._bankName = bankName;

  String? _ifscCode;
  String? get ifscCode => _$this._ifscCode;
  set ifscCode(String? ifscCode) => _$this._ifscCode = ifscCode;

  int? _passbookOrChequeDocumentID;
  int? get passbookOrChequeDocumentID => _$this._passbookOrChequeDocumentID;
  set passbookOrChequeDocumentID(int? passbookOrChequeDocumentID) =>
      _$this._passbookOrChequeDocumentID = passbookOrChequeDocumentID;

  String? _passbookOrChequeDocumentUrl;
  String? get passbookOrChequeDocumentUrl =>
      _$this._passbookOrChequeDocumentUrl;
  set passbookOrChequeDocumentUrl(String? passbookOrChequeDocumentUrl) =>
      _$this._passbookOrChequeDocumentUrl = passbookOrChequeDocumentUrl;

  String? _branchName;
  String? get branchName => _$this._branchName;
  set branchName(String? branchName) => _$this._branchName = branchName;

  String? _registeredName;
  String? get registeredName => _$this._registeredName;
  set registeredName(String? registeredName) =>
      _$this._registeredName = registeredName;

  String? _nameMatch;
  String? get nameMatch => _$this._nameMatch;
  set nameMatch(String? nameMatch) => _$this._nameMatch = nameMatch;

  String? _score;
  String? get score => _$this._score;
  set score(String? score) => _$this._score = score;

  BankDetailBuilder();

  BankDetailBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerType = $v.ownerType;
      _ownerID = $v.ownerID;
      _accountNumber = $v.accountNumber;
      _bankName = $v.bankName;
      _ifscCode = $v.ifscCode;
      _passbookOrChequeDocumentID = $v.passbookOrChequeDocumentID;
      _passbookOrChequeDocumentUrl = $v.passbookOrChequeDocumentUrl;
      _branchName = $v.branchName;
      _registeredName = $v.registeredName;
      _nameMatch = $v.nameMatch;
      _score = $v.score;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BankDetail other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BankDetail;
  }

  @override
  void update(void Function(BankDetailBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BankDetail build() => _build();

  _$BankDetail _build() {
    final _$result = _$v ??
        new _$BankDetail._(
            id: id,
            ownerType: ownerType,
            ownerID: ownerID,
            accountNumber: accountNumber,
            bankName: bankName,
            ifscCode: ifscCode,
            passbookOrChequeDocumentID: passbookOrChequeDocumentID,
            passbookOrChequeDocumentUrl: passbookOrChequeDocumentUrl,
            branchName: branchName,
            registeredName: registeredName,
            nameMatch: nameMatch,
            score: score);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
