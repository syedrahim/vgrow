// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_branch.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanBranch> _$loanBranchSerializer = new _$LoanBranchSerializer();

class _$LoanBranchSerializer implements StructuredSerializer<LoanBranch> {
  @override
  final Iterable<Type> types = const [LoanBranch, _$LoanBranch];
  @override
  final String wireName = 'LoanBranch';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanBranch object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.organisationName;
    if (value != null) {
      result
        ..add('organisation_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  LoanBranch deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanBranchBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'organisation_name':
          result.organisationName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$LoanBranch extends LoanBranch {
  @override
  final int? id;
  @override
  final String? name;
  @override
  final String? code;
  @override
  final String? organisationName;

  factory _$LoanBranch([void Function(LoanBranchBuilder)? updates]) =>
      (new LoanBranchBuilder()..update(updates))._build();

  _$LoanBranch._({this.id, this.name, this.code, this.organisationName})
      : super._();

  @override
  LoanBranch rebuild(void Function(LoanBranchBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanBranchBuilder toBuilder() => new LoanBranchBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanBranch &&
        id == other.id &&
        name == other.name &&
        code == other.code &&
        organisationName == other.organisationName;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, organisationName.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanBranch')
          ..add('id', id)
          ..add('name', name)
          ..add('code', code)
          ..add('organisationName', organisationName))
        .toString();
  }
}

class LoanBranchBuilder implements Builder<LoanBranch, LoanBranchBuilder> {
  _$LoanBranch? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _organisationName;
  String? get organisationName => _$this._organisationName;
  set organisationName(String? organisationName) =>
      _$this._organisationName = organisationName;

  LoanBranchBuilder();

  LoanBranchBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _code = $v.code;
      _organisationName = $v.organisationName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanBranch other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanBranch;
  }

  @override
  void update(void Function(LoanBranchBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanBranch build() => _build();

  _$LoanBranch _build() {
    final _$result = _$v ??
        new _$LoanBranch._(
            id: id, name: name, code: code, organisationName: organisationName);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
