// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'owned_entity.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<OwnedEntity> _$ownedEntitySerializer = new _$OwnedEntitySerializer();

class _$OwnedEntitySerializer implements StructuredSerializer<OwnedEntity> {
  @override
  final Iterable<Type> types = const [OwnedEntity, _$OwnedEntity];
  @override
  final String wireName = 'OwnedEntity';

  @override
  Iterable<Object?> serialize(Serializers serializers, OwnedEntity object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.ownerType;
    if (value != null) {
      result
        ..add('owner_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ownerID;
    if (value != null) {
      result
        ..add('owner_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.entityCategory;
    if (value != null) {
      result
        ..add('entity_category')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.entityType;
    if (value != null) {
      result
        ..add('entity_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.entityWorth;
    if (value != null) {
      result
        ..add('entity_worth')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.numberOfEntities;
    if (value != null) {
      result
        ..add('number_of_entities')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  OwnedEntity deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new OwnedEntityBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'owner_type':
          result.ownerType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'owner_id':
          result.ownerID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'entity_category':
          result.entityCategory = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'entity_type':
          result.entityType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'entity_worth':
          result.entityWorth = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'number_of_entities':
          result.numberOfEntities = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$OwnedEntity extends OwnedEntity {
  @override
  final int? id;
  @override
  final String? ownerType;
  @override
  final int? ownerID;
  @override
  final String? entityCategory;
  @override
  final String? entityType;
  @override
  final double? entityWorth;
  @override
  final int? numberOfEntities;
  @override
  final String? code;

  factory _$OwnedEntity([void Function(OwnedEntityBuilder)? updates]) =>
      (new OwnedEntityBuilder()..update(updates))._build();

  _$OwnedEntity._(
      {this.id,
      this.ownerType,
      this.ownerID,
      this.entityCategory,
      this.entityType,
      this.entityWorth,
      this.numberOfEntities,
      this.code})
      : super._();

  @override
  OwnedEntity rebuild(void Function(OwnedEntityBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  OwnedEntityBuilder toBuilder() => new OwnedEntityBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is OwnedEntity &&
        id == other.id &&
        ownerType == other.ownerType &&
        ownerID == other.ownerID &&
        entityCategory == other.entityCategory &&
        entityType == other.entityType &&
        entityWorth == other.entityWorth &&
        numberOfEntities == other.numberOfEntities &&
        code == other.code;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, ownerType.hashCode);
    _$hash = $jc(_$hash, ownerID.hashCode);
    _$hash = $jc(_$hash, entityCategory.hashCode);
    _$hash = $jc(_$hash, entityType.hashCode);
    _$hash = $jc(_$hash, entityWorth.hashCode);
    _$hash = $jc(_$hash, numberOfEntities.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'OwnedEntity')
          ..add('id', id)
          ..add('ownerType', ownerType)
          ..add('ownerID', ownerID)
          ..add('entityCategory', entityCategory)
          ..add('entityType', entityType)
          ..add('entityWorth', entityWorth)
          ..add('numberOfEntities', numberOfEntities)
          ..add('code', code))
        .toString();
  }
}

class OwnedEntityBuilder implements Builder<OwnedEntity, OwnedEntityBuilder> {
  _$OwnedEntity? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _ownerType;
  String? get ownerType => _$this._ownerType;
  set ownerType(String? ownerType) => _$this._ownerType = ownerType;

  int? _ownerID;
  int? get ownerID => _$this._ownerID;
  set ownerID(int? ownerID) => _$this._ownerID = ownerID;

  String? _entityCategory;
  String? get entityCategory => _$this._entityCategory;
  set entityCategory(String? entityCategory) =>
      _$this._entityCategory = entityCategory;

  String? _entityType;
  String? get entityType => _$this._entityType;
  set entityType(String? entityType) => _$this._entityType = entityType;

  double? _entityWorth;
  double? get entityWorth => _$this._entityWorth;
  set entityWorth(double? entityWorth) => _$this._entityWorth = entityWorth;

  int? _numberOfEntities;
  int? get numberOfEntities => _$this._numberOfEntities;
  set numberOfEntities(int? numberOfEntities) =>
      _$this._numberOfEntities = numberOfEntities;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  OwnedEntityBuilder();

  OwnedEntityBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerType = $v.ownerType;
      _ownerID = $v.ownerID;
      _entityCategory = $v.entityCategory;
      _entityType = $v.entityType;
      _entityWorth = $v.entityWorth;
      _numberOfEntities = $v.numberOfEntities;
      _code = $v.code;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(OwnedEntity other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$OwnedEntity;
  }

  @override
  void update(void Function(OwnedEntityBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  OwnedEntity build() => _build();

  _$OwnedEntity _build() {
    final _$result = _$v ??
        new _$OwnedEntity._(
            id: id,
            ownerType: ownerType,
            ownerID: ownerID,
            entityCategory: entityCategory,
            entityType: entityType,
            entityWorth: entityWorth,
            numberOfEntities: numberOfEntities,
            code: code);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
