import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'existing_loan.g.dart';

abstract class ExistingLoan implements Built<ExistingLoan, ExistingLoanBuilder>{
  ExistingLoan._();
  factory ExistingLoan([ExistingLoanBuilder updates(ExistingLoanBuilder builder)]) = _$ExistingLoan;

  int? get id;

  @BuiltValueField(wireName: 'loan_type')
  String? get loanType;

  @BuiltValueField(wireName: 'institution_name')
  String? get institutionName;

  @BuiltValueField(wireName: 'monthly_emi')
  double? get monthlyEMI;

  double? get amount;

  static Serializer<ExistingLoan> get serializer => _$existingLoanSerializer;
}