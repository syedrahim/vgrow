import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'created_by.dart';

import 'loan_center.dart';
import 'loan_address.dart';
import 'bank_detail.dart';
import 'crif_score.dart';
import 'loan_document.dart';
import 'existing_loan.dart';
import 'family_detail.dart';
import 'family_member.dart';
import 'loan_center.dart';
import 'owned_entity.dart';

part 'loan_customer.g.dart';

abstract class LoanCustomer implements Built<LoanCustomer, LoanCustomerBuilder>{
  LoanCustomer._();
  factory LoanCustomer([LoanCustomerBuilder updates(LoanCustomerBuilder builder)]) = _$LoanCustomer;

  int? get id;

  String? get code;

  String? get email;

  String? get firstname;

  String? get lastname;

  @BuiltValueField(wireName: 'isd_code')
  String? get isdCode;

  @BuiltValueField(wireName: 'phone_number')
  String? get phoneNumber;

  @BuiltValueField(wireName: 'profile_pic_id')
  int? get profilePictureID;

  @BuiltValueField(wireName: 'profile_pic_url')
  String? get profilePictureUrl;

  String? get gender;

  @BuiltValueField(wireName: 'is_active')
  bool? get isActive;

  // LoanCenter? get center;

  String? get dob;

  @BuiltValueField(wireName: 'pan_number')
  String? get panNumber;

  @BuiltValueField(wireName: 'pan_document_id')
  int? get panDocumentID;

  @BuiltValueField(wireName: 'pan_document_url')
  String? get panDocumentUrl;

  @BuiltValueField(wireName: 'educational_qualification')
  String? get educationalQualification;

  @BuiltValueField(wireName: 'aadhaar_number')
  String? get aadhaarNumber;

  int? get age;

  String? get occupation;

  @BuiltValueField(wireName: 'monthly_income')
  double? get monthlyIncome;

  @BuiltValueField(wireName: 'does_own_business')
  bool? get doesOwnBusiness;

  @BuiltValueField(wireName: 'business_income')
  double? get businessIncome;

  @BuiltValueField(wireName: 'address')
  LoanAddress? get address;

  @BuiltValueField(wireName: 'crif_score')
  CrifScore? get crifScore;

  @BuiltValueField(wireName: 'aadhaar_front_image_id')
  int? get aadharFrontImageID;

  @BuiltValueField(wireName: 'aadhaar_front_image_url')
  String? get aadharFrontImageUrl;

  @BuiltValueField(wireName: 'aadhaar_back_image_id')
  int? get aadharBackImageID;

  @BuiltValueField(wireName: 'aadhaar_back_image_url')
  String? get aadharBackImageUrl;

  @BuiltValueField(wireName: 'is_email_verified')
  bool? get isEmailVerified;

  @BuiltValueField(wireName: 'is_mobile_verified')
  bool? get isMobileVerified;

  BuiltList<LoanDocument>? get documents;

  @BuiltValueField(wireName: 'owned_entities')
  BuiltList<OwnedEntity>? get ownedEntities;

  @BuiltValueField(wireName: 'bank_detail')
  BankDetail? get bankDetail;

  @BuiltValueField(wireName: 'family_members')
  BuiltList<FamilyMember> get familyMembers;

  @BuiltValueField(wireName: 'family_detail')
  FamilyDetail? get familyDetail;

  @BuiltValueField(wireName: 'existing_loans')
  BuiltList<ExistingLoan> get existingLoans;

  static Serializer<LoanCustomer> get serializer => _$loanCustomerSerializer;
}
