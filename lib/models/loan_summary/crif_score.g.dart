// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'crif_score.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CrifScore> _$crifScoreSerializer = new _$CrifScoreSerializer();

class _$CrifScoreSerializer implements StructuredSerializer<CrifScore> {
  @override
  final Iterable<Type> types = const [CrifScore, _$CrifScore];
  @override
  final String wireName = 'CrifScore';

  @override
  Iterable<Object?> serialize(Serializers serializers, CrifScore object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.score;
    if (value != null) {
      result
        ..add('score')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdAt;
    if (value != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.comment;
    if (value != null) {
      result
        ..add('comment')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CrifScore deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CrifScoreBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'score':
          result.score = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'comment':
          result.comment = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$CrifScore extends CrifScore {
  @override
  final double? score;
  @override
  final String? status;
  @override
  final String? createdAt;
  @override
  final String? comment;

  factory _$CrifScore([void Function(CrifScoreBuilder)? updates]) =>
      (new CrifScoreBuilder()..update(updates))._build();

  _$CrifScore._({this.score, this.status, this.createdAt, this.comment})
      : super._();

  @override
  CrifScore rebuild(void Function(CrifScoreBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CrifScoreBuilder toBuilder() => new CrifScoreBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CrifScore &&
        score == other.score &&
        status == other.status &&
        createdAt == other.createdAt &&
        comment == other.comment;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, score.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, comment.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CrifScore')
          ..add('score', score)
          ..add('status', status)
          ..add('createdAt', createdAt)
          ..add('comment', comment))
        .toString();
  }
}

class CrifScoreBuilder implements Builder<CrifScore, CrifScoreBuilder> {
  _$CrifScore? _$v;

  double? _score;
  double? get score => _$this._score;
  set score(double? score) => _$this._score = score;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  String? _createdAt;
  String? get createdAt => _$this._createdAt;
  set createdAt(String? createdAt) => _$this._createdAt = createdAt;

  String? _comment;
  String? get comment => _$this._comment;
  set comment(String? comment) => _$this._comment = comment;

  CrifScoreBuilder();

  CrifScoreBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _score = $v.score;
      _status = $v.status;
      _createdAt = $v.createdAt;
      _comment = $v.comment;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CrifScore other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CrifScore;
  }

  @override
  void update(void Function(CrifScoreBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CrifScore build() => _build();

  _$CrifScore _build() {
    final _$result = _$v ??
        new _$CrifScore._(
            score: score,
            status: status,
            createdAt: createdAt,
            comment: comment);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
