import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'loan_document.g.dart';

abstract class LoanDocument implements Built<LoanDocument, LoanDocumentBuilder>{
  LoanDocument._();
  factory LoanDocument([LoanDocumentBuilder updates(LoanDocumentBuilder builder)]) = _$LoanDocument;

  int? get id;

  @BuiltValueField(wireName: 'owner_type')
  String? get ownerType;

  @BuiltValueField(wireName: 'owner_id')
  int? get ownerID;

  @BuiltValueField(wireName: 'document_type')
  String? get documentType;

  @BuiltValueField(wireName: 'document_name')
  String? get documentName;

  @BuiltValueField(wireName: 'created_by_id')
  int? get createdByID;

  @BuiltValueField(wireName: 'created_by_role_code')
  String? get createdByRoleCode;

  String? get code;

  @BuiltValueField(wireName: 'front_side_image_id')
  int? get frontSideImageID;

  @BuiltValueField(wireName: 'back_side_image_id')
  int? get backSideImageID;

  @BuiltValueField(wireName: 'front_side_image_url')
  String? get frontSideImageUrl;

  @BuiltValueField(wireName: 'back_side_image_url')
  String? get backSideImageUrl;

  static Serializer<LoanDocument> get serializer => _$loanDocumentSerializer;
}