// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'existing_loan.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ExistingLoan> _$existingLoanSerializer =
    new _$ExistingLoanSerializer();

class _$ExistingLoanSerializer implements StructuredSerializer<ExistingLoan> {
  @override
  final Iterable<Type> types = const [ExistingLoan, _$ExistingLoan];
  @override
  final String wireName = 'ExistingLoan';

  @override
  Iterable<Object?> serialize(Serializers serializers, ExistingLoan object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanType;
    if (value != null) {
      result
        ..add('loan_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.institutionName;
    if (value != null) {
      result
        ..add('institution_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.monthlyEMI;
    if (value != null) {
      result
        ..add('monthly_emi')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.amount;
    if (value != null) {
      result
        ..add('amount')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  ExistingLoan deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ExistingLoanBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_type':
          result.loanType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'institution_name':
          result.institutionName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'monthly_emi':
          result.monthlyEMI = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'amount':
          result.amount = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
      }
    }

    return result.build();
  }
}

class _$ExistingLoan extends ExistingLoan {
  @override
  final int? id;
  @override
  final String? loanType;
  @override
  final String? institutionName;
  @override
  final double? monthlyEMI;
  @override
  final double? amount;

  factory _$ExistingLoan([void Function(ExistingLoanBuilder)? updates]) =>
      (new ExistingLoanBuilder()..update(updates))._build();

  _$ExistingLoan._(
      {this.id,
      this.loanType,
      this.institutionName,
      this.monthlyEMI,
      this.amount})
      : super._();

  @override
  ExistingLoan rebuild(void Function(ExistingLoanBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ExistingLoanBuilder toBuilder() => new ExistingLoanBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ExistingLoan &&
        id == other.id &&
        loanType == other.loanType &&
        institutionName == other.institutionName &&
        monthlyEMI == other.monthlyEMI &&
        amount == other.amount;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, loanType.hashCode);
    _$hash = $jc(_$hash, institutionName.hashCode);
    _$hash = $jc(_$hash, monthlyEMI.hashCode);
    _$hash = $jc(_$hash, amount.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ExistingLoan')
          ..add('id', id)
          ..add('loanType', loanType)
          ..add('institutionName', institutionName)
          ..add('monthlyEMI', monthlyEMI)
          ..add('amount', amount))
        .toString();
  }
}

class ExistingLoanBuilder
    implements Builder<ExistingLoan, ExistingLoanBuilder> {
  _$ExistingLoan? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _loanType;
  String? get loanType => _$this._loanType;
  set loanType(String? loanType) => _$this._loanType = loanType;

  String? _institutionName;
  String? get institutionName => _$this._institutionName;
  set institutionName(String? institutionName) =>
      _$this._institutionName = institutionName;

  double? _monthlyEMI;
  double? get monthlyEMI => _$this._monthlyEMI;
  set monthlyEMI(double? monthlyEMI) => _$this._monthlyEMI = monthlyEMI;

  double? _amount;
  double? get amount => _$this._amount;
  set amount(double? amount) => _$this._amount = amount;

  ExistingLoanBuilder();

  ExistingLoanBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _loanType = $v.loanType;
      _institutionName = $v.institutionName;
      _monthlyEMI = $v.monthlyEMI;
      _amount = $v.amount;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ExistingLoan other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ExistingLoan;
  }

  @override
  void update(void Function(ExistingLoanBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ExistingLoan build() => _build();

  _$ExistingLoan _build() {
    final _$result = _$v ??
        new _$ExistingLoan._(
            id: id,
            loanType: loanType,
            institutionName: institutionName,
            monthlyEMI: monthlyEMI,
            amount: amount);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
