// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_summary_model.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanSummary> _$loanSummarySerializer = new _$LoanSummarySerializer();

class _$LoanSummarySerializer implements StructuredSerializer<LoanSummary> {
  @override
  final Iterable<Type> types = const [LoanSummary, _$LoanSummary];
  @override
  final String wireName = 'LoanSummary';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanSummary object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.additionalDetailsJson;
    if (value != null) {
      result
        ..add('additional_details_json')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(AdditionalDetailsJson)));
    }
    value = object.outstandingAmount;
    if (value != null) {
      result
        ..add('outstanding_amount')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.numberOfPendingDues;
    if (value != null) {
      result
        ..add('number_of_pending_dues')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.appliedDate;
    if (value != null) {
      result
        ..add('applied_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.preferredDueDate;
    if (value != null) {
      result
        ..add('preferred_due_date')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(Null)));
    }
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.centerId;
    if (value != null) {
      result
        ..add('center_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.centerName;
    if (value != null) {
      result
        ..add('center_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdBy;
    if (value != null) {
      result
        ..add('created_by')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CreatedBy)));
    }
    value = object.createdById;
    if (value != null) {
      result
        ..add('created_by_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanTypeId;
    if (value != null) {
      result
        ..add('loan_type_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanTypeName;
    if (value != null) {
      result
        ..add('loan_type_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.loanTypeCode;
    if (value != null) {
      result
        ..add('loan_type_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.estimatedCost;
    if (value != null) {
      result
        ..add('estimated_cost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.requestedLoanAmt;
    if (value != null) {
      result
        ..add('requested_loan_amt')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.loanPurposeId;
    if (value != null) {
      result
        ..add('loan_purpose_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanPurposeName;
    if (value != null) {
      result
        ..add('loan_purpose_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.organisationId;
    if (value != null) {
      result
        ..add('organisation_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.organisationName;
    if (value != null) {
      result
        ..add('organisation_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.croPictureId;
    if (value != null) {
      result
        ..add('cro_picture_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.croPictureUrl;
    if (value != null) {
      result
        ..add('cro_picture_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.croName;
    if (value != null) {
      result
        ..add('cro_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.croCapturedTime;
    if (value != null) {
      result
        ..add('cro_captured_time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.currentStep;
    if (value != null) {
      result
        ..add('current_step')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.tenureInMonths;
    if (value != null) {
      result
        ..add('tenure_in_months')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.interestRate;
    if (value != null) {
      result
        ..add('interest_rate')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.expectedStartDate;
    if (value != null) {
      result
        ..add('expected_start_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bmPictureId;
    if (value != null) {
      result
        ..add('bm_picture_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.bmPictureUrl;
    if (value != null) {
      result
        ..add('bm_picture_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bmName;
    if (value != null) {
      result
        ..add('bm_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bmCapturedTime;
    if (value != null) {
      result
        ..add('bm_captured_time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.iaPictureId;
    if (value != null) {
      result
        ..add('ia_picture_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.iaPictureUrl;
    if (value != null) {
      result
        ..add('ia_picture_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.iaName;
    if (value != null) {
      result
        ..add('ia_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.iaCapturedTime;
    if (value != null) {
      result
        ..add('ia_captured_time')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.customer;
    if (value != null) {
      result
        ..add('customer')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(LoanCustomer)));
    }
    value = object.eligibilityDetail;
    if (value != null) {
      result
        ..add('eligibility_detail')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(EligibilityDetail)));
    }
    return result;
  }

  @override
  LoanSummary deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanSummaryBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'additional_details_json':
          result.additionalDetailsJson.replace(serializers.deserialize(value,
                  specifiedType: const FullType(AdditionalDetailsJson))!
              as AdditionalDetailsJson);
          break;
        case 'outstanding_amount':
          result.outstandingAmount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'number_of_pending_dues':
          result.numberOfPendingDues = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'applied_date':
          result.appliedDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'preferred_due_date':
          result.preferredDueDate = serializers.deserialize(value,
              specifiedType: const FullType(Null)) as Null?;
          break;
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'center_id':
          result.centerId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'center_name':
          result.centerName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_by':
          result.createdBy.replace(serializers.deserialize(value,
              specifiedType: const FullType(CreatedBy))! as CreatedBy);
          break;
        case 'created_by_id':
          result.createdById = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_type_id':
          result.loanTypeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_type_name':
          result.loanTypeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'loan_type_code':
          result.loanTypeCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'estimated_cost':
          result.estimatedCost = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'requested_loan_amt':
          result.requestedLoanAmt = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'loan_purpose_id':
          result.loanPurposeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_purpose_name':
          result.loanPurposeName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'organisation_id':
          result.organisationId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'organisation_name':
          result.organisationName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'cro_picture_id':
          result.croPictureId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'cro_picture_url':
          result.croPictureUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'cro_name':
          result.croName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'cro_captured_time':
          result.croCapturedTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'current_step':
          result.currentStep = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'tenure_in_months':
          result.tenureInMonths = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'interest_rate':
          result.interestRate = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'expected_start_date':
          result.expectedStartDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bm_picture_id':
          result.bmPictureId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'bm_picture_url':
          result.bmPictureUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bm_name':
          result.bmName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bm_captured_time':
          result.bmCapturedTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'ia_picture_id':
          result.iaPictureId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'ia_picture_url':
          result.iaPictureUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'ia_name':
          result.iaName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'ia_captured_time':
          result.iaCapturedTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'customer':
          result.customer.replace(serializers.deserialize(value,
              specifiedType: const FullType(LoanCustomer))! as LoanCustomer);
          break;
        case 'eligibility_detail':
          result.eligibilityDetail.replace(serializers.deserialize(value,
                  specifiedType: const FullType(EligibilityDetail))!
              as EligibilityDetail);
          break;
      }
    }

    return result.build();
  }
}

class _$LoanSummary extends LoanSummary {
  @override
  final AdditionalDetailsJson? additionalDetailsJson;
  @override
  final int? outstandingAmount;
  @override
  final int? numberOfPendingDues;
  @override
  final String? appliedDate;
  @override
  final Null? preferredDueDate;
  @override
  final int? id;
  @override
  final String? code;
  @override
  final int? centerId;
  @override
  final String? centerName;
  @override
  final CreatedBy? createdBy;
  @override
  final int? createdById;
  @override
  final int? loanTypeId;
  @override
  final String? loanTypeName;
  @override
  final String? loanTypeCode;
  @override
  final double? estimatedCost;
  @override
  final double? requestedLoanAmt;
  @override
  final String? status;
  @override
  final int? loanPurposeId;
  @override
  final String? loanPurposeName;
  @override
  final int? organisationId;
  @override
  final String? organisationName;
  @override
  final int? croPictureId;
  @override
  final String? croPictureUrl;
  @override
  final String? croName;
  @override
  final String? croCapturedTime;
  @override
  final String? currentStep;
  @override
  final int? tenureInMonths;
  @override
  final double? interestRate;
  @override
  final String? expectedStartDate;
  @override
  final int? bmPictureId;
  @override
  final String? bmPictureUrl;
  @override
  final String? bmName;
  @override
  final String? bmCapturedTime;
  @override
  final int? iaPictureId;
  @override
  final String? iaPictureUrl;
  @override
  final String? iaName;
  @override
  final String? iaCapturedTime;
  @override
  final LoanCustomer? customer;
  @override
  final EligibilityDetail? eligibilityDetail;

  factory _$LoanSummary([void Function(LoanSummaryBuilder)? updates]) =>
      (new LoanSummaryBuilder()..update(updates))._build();

  _$LoanSummary._(
      {this.additionalDetailsJson,
      this.outstandingAmount,
      this.numberOfPendingDues,
      this.appliedDate,
      this.preferredDueDate,
      this.id,
      this.code,
      this.centerId,
      this.centerName,
      this.createdBy,
      this.createdById,
      this.loanTypeId,
      this.loanTypeName,
      this.loanTypeCode,
      this.estimatedCost,
      this.requestedLoanAmt,
      this.status,
      this.loanPurposeId,
      this.loanPurposeName,
      this.organisationId,
      this.organisationName,
      this.croPictureId,
      this.croPictureUrl,
      this.croName,
      this.croCapturedTime,
      this.currentStep,
      this.tenureInMonths,
      this.interestRate,
      this.expectedStartDate,
      this.bmPictureId,
      this.bmPictureUrl,
      this.bmName,
      this.bmCapturedTime,
      this.iaPictureId,
      this.iaPictureUrl,
      this.iaName,
      this.iaCapturedTime,
      this.customer,
      this.eligibilityDetail})
      : super._();

  @override
  LoanSummary rebuild(void Function(LoanSummaryBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanSummaryBuilder toBuilder() => new LoanSummaryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanSummary &&
        additionalDetailsJson == other.additionalDetailsJson &&
        outstandingAmount == other.outstandingAmount &&
        numberOfPendingDues == other.numberOfPendingDues &&
        appliedDate == other.appliedDate &&
        preferredDueDate == other.preferredDueDate &&
        id == other.id &&
        code == other.code &&
        centerId == other.centerId &&
        centerName == other.centerName &&
        createdBy == other.createdBy &&
        createdById == other.createdById &&
        loanTypeId == other.loanTypeId &&
        loanTypeName == other.loanTypeName &&
        loanTypeCode == other.loanTypeCode &&
        estimatedCost == other.estimatedCost &&
        requestedLoanAmt == other.requestedLoanAmt &&
        status == other.status &&
        loanPurposeId == other.loanPurposeId &&
        loanPurposeName == other.loanPurposeName &&
        organisationId == other.organisationId &&
        organisationName == other.organisationName &&
        croPictureId == other.croPictureId &&
        croPictureUrl == other.croPictureUrl &&
        croName == other.croName &&
        croCapturedTime == other.croCapturedTime &&
        currentStep == other.currentStep &&
        tenureInMonths == other.tenureInMonths &&
        interestRate == other.interestRate &&
        expectedStartDate == other.expectedStartDate &&
        bmPictureId == other.bmPictureId &&
        bmPictureUrl == other.bmPictureUrl &&
        bmName == other.bmName &&
        bmCapturedTime == other.bmCapturedTime &&
        iaPictureId == other.iaPictureId &&
        iaPictureUrl == other.iaPictureUrl &&
        iaName == other.iaName &&
        iaCapturedTime == other.iaCapturedTime &&
        customer == other.customer &&
        eligibilityDetail == other.eligibilityDetail;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, additionalDetailsJson.hashCode);
    _$hash = $jc(_$hash, outstandingAmount.hashCode);
    _$hash = $jc(_$hash, numberOfPendingDues.hashCode);
    _$hash = $jc(_$hash, appliedDate.hashCode);
    _$hash = $jc(_$hash, preferredDueDate.hashCode);
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, centerId.hashCode);
    _$hash = $jc(_$hash, centerName.hashCode);
    _$hash = $jc(_$hash, createdBy.hashCode);
    _$hash = $jc(_$hash, createdById.hashCode);
    _$hash = $jc(_$hash, loanTypeId.hashCode);
    _$hash = $jc(_$hash, loanTypeName.hashCode);
    _$hash = $jc(_$hash, loanTypeCode.hashCode);
    _$hash = $jc(_$hash, estimatedCost.hashCode);
    _$hash = $jc(_$hash, requestedLoanAmt.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, loanPurposeId.hashCode);
    _$hash = $jc(_$hash, loanPurposeName.hashCode);
    _$hash = $jc(_$hash, organisationId.hashCode);
    _$hash = $jc(_$hash, organisationName.hashCode);
    _$hash = $jc(_$hash, croPictureId.hashCode);
    _$hash = $jc(_$hash, croPictureUrl.hashCode);
    _$hash = $jc(_$hash, croName.hashCode);
    _$hash = $jc(_$hash, croCapturedTime.hashCode);
    _$hash = $jc(_$hash, currentStep.hashCode);
    _$hash = $jc(_$hash, tenureInMonths.hashCode);
    _$hash = $jc(_$hash, interestRate.hashCode);
    _$hash = $jc(_$hash, expectedStartDate.hashCode);
    _$hash = $jc(_$hash, bmPictureId.hashCode);
    _$hash = $jc(_$hash, bmPictureUrl.hashCode);
    _$hash = $jc(_$hash, bmName.hashCode);
    _$hash = $jc(_$hash, bmCapturedTime.hashCode);
    _$hash = $jc(_$hash, iaPictureId.hashCode);
    _$hash = $jc(_$hash, iaPictureUrl.hashCode);
    _$hash = $jc(_$hash, iaName.hashCode);
    _$hash = $jc(_$hash, iaCapturedTime.hashCode);
    _$hash = $jc(_$hash, customer.hashCode);
    _$hash = $jc(_$hash, eligibilityDetail.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanSummary')
          ..add('additionalDetailsJson', additionalDetailsJson)
          ..add('outstandingAmount', outstandingAmount)
          ..add('numberOfPendingDues', numberOfPendingDues)
          ..add('appliedDate', appliedDate)
          ..add('preferredDueDate', preferredDueDate)
          ..add('id', id)
          ..add('code', code)
          ..add('centerId', centerId)
          ..add('centerName', centerName)
          ..add('createdBy', createdBy)
          ..add('createdById', createdById)
          ..add('loanTypeId', loanTypeId)
          ..add('loanTypeName', loanTypeName)
          ..add('loanTypeCode', loanTypeCode)
          ..add('estimatedCost', estimatedCost)
          ..add('requestedLoanAmt', requestedLoanAmt)
          ..add('status', status)
          ..add('loanPurposeId', loanPurposeId)
          ..add('loanPurposeName', loanPurposeName)
          ..add('organisationId', organisationId)
          ..add('organisationName', organisationName)
          ..add('croPictureId', croPictureId)
          ..add('croPictureUrl', croPictureUrl)
          ..add('croName', croName)
          ..add('croCapturedTime', croCapturedTime)
          ..add('currentStep', currentStep)
          ..add('tenureInMonths', tenureInMonths)
          ..add('interestRate', interestRate)
          ..add('expectedStartDate', expectedStartDate)
          ..add('bmPictureId', bmPictureId)
          ..add('bmPictureUrl', bmPictureUrl)
          ..add('bmName', bmName)
          ..add('bmCapturedTime', bmCapturedTime)
          ..add('iaPictureId', iaPictureId)
          ..add('iaPictureUrl', iaPictureUrl)
          ..add('iaName', iaName)
          ..add('iaCapturedTime', iaCapturedTime)
          ..add('customer', customer)
          ..add('eligibilityDetail', eligibilityDetail))
        .toString();
  }
}

class LoanSummaryBuilder implements Builder<LoanSummary, LoanSummaryBuilder> {
  _$LoanSummary? _$v;

  AdditionalDetailsJsonBuilder? _additionalDetailsJson;
  AdditionalDetailsJsonBuilder get additionalDetailsJson =>
      _$this._additionalDetailsJson ??= new AdditionalDetailsJsonBuilder();
  set additionalDetailsJson(
          AdditionalDetailsJsonBuilder? additionalDetailsJson) =>
      _$this._additionalDetailsJson = additionalDetailsJson;

  int? _outstandingAmount;
  int? get outstandingAmount => _$this._outstandingAmount;
  set outstandingAmount(int? outstandingAmount) =>
      _$this._outstandingAmount = outstandingAmount;

  int? _numberOfPendingDues;
  int? get numberOfPendingDues => _$this._numberOfPendingDues;
  set numberOfPendingDues(int? numberOfPendingDues) =>
      _$this._numberOfPendingDues = numberOfPendingDues;

  String? _appliedDate;
  String? get appliedDate => _$this._appliedDate;
  set appliedDate(String? appliedDate) => _$this._appliedDate = appliedDate;

  Null? _preferredDueDate;
  Null? get preferredDueDate => _$this._preferredDueDate;
  set preferredDueDate(Null? preferredDueDate) =>
      _$this._preferredDueDate = preferredDueDate;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  int? _centerId;
  int? get centerId => _$this._centerId;
  set centerId(int? centerId) => _$this._centerId = centerId;

  String? _centerName;
  String? get centerName => _$this._centerName;
  set centerName(String? centerName) => _$this._centerName = centerName;

  CreatedByBuilder? _createdBy;
  CreatedByBuilder get createdBy =>
      _$this._createdBy ??= new CreatedByBuilder();
  set createdBy(CreatedByBuilder? createdBy) => _$this._createdBy = createdBy;

  int? _createdById;
  int? get createdById => _$this._createdById;
  set createdById(int? createdById) => _$this._createdById = createdById;

  int? _loanTypeId;
  int? get loanTypeId => _$this._loanTypeId;
  set loanTypeId(int? loanTypeId) => _$this._loanTypeId = loanTypeId;

  String? _loanTypeName;
  String? get loanTypeName => _$this._loanTypeName;
  set loanTypeName(String? loanTypeName) => _$this._loanTypeName = loanTypeName;

  String? _loanTypeCode;
  String? get loanTypeCode => _$this._loanTypeCode;
  set loanTypeCode(String? loanTypeCode) => _$this._loanTypeCode = loanTypeCode;

  double? _estimatedCost;
  double? get estimatedCost => _$this._estimatedCost;
  set estimatedCost(double? estimatedCost) =>
      _$this._estimatedCost = estimatedCost;

  double? _requestedLoanAmt;
  double? get requestedLoanAmt => _$this._requestedLoanAmt;
  set requestedLoanAmt(double? requestedLoanAmt) =>
      _$this._requestedLoanAmt = requestedLoanAmt;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  int? _loanPurposeId;
  int? get loanPurposeId => _$this._loanPurposeId;
  set loanPurposeId(int? loanPurposeId) =>
      _$this._loanPurposeId = loanPurposeId;

  String? _loanPurposeName;
  String? get loanPurposeName => _$this._loanPurposeName;
  set loanPurposeName(String? loanPurposeName) =>
      _$this._loanPurposeName = loanPurposeName;

  int? _organisationId;
  int? get organisationId => _$this._organisationId;
  set organisationId(int? organisationId) =>
      _$this._organisationId = organisationId;

  String? _organisationName;
  String? get organisationName => _$this._organisationName;
  set organisationName(String? organisationName) =>
      _$this._organisationName = organisationName;

  int? _croPictureId;
  int? get croPictureId => _$this._croPictureId;
  set croPictureId(int? croPictureId) => _$this._croPictureId = croPictureId;

  String? _croPictureUrl;
  String? get croPictureUrl => _$this._croPictureUrl;
  set croPictureUrl(String? croPictureUrl) =>
      _$this._croPictureUrl = croPictureUrl;

  String? _croName;
  String? get croName => _$this._croName;
  set croName(String? croName) => _$this._croName = croName;

  String? _croCapturedTime;
  String? get croCapturedTime => _$this._croCapturedTime;
  set croCapturedTime(String? croCapturedTime) =>
      _$this._croCapturedTime = croCapturedTime;

  String? _currentStep;
  String? get currentStep => _$this._currentStep;
  set currentStep(String? currentStep) => _$this._currentStep = currentStep;

  int? _tenureInMonths;
  int? get tenureInMonths => _$this._tenureInMonths;
  set tenureInMonths(int? tenureInMonths) =>
      _$this._tenureInMonths = tenureInMonths;

  double? _interestRate;
  double? get interestRate => _$this._interestRate;
  set interestRate(double? interestRate) => _$this._interestRate = interestRate;

  String? _expectedStartDate;
  String? get expectedStartDate => _$this._expectedStartDate;
  set expectedStartDate(String? expectedStartDate) =>
      _$this._expectedStartDate = expectedStartDate;

  int? _bmPictureId;
  int? get bmPictureId => _$this._bmPictureId;
  set bmPictureId(int? bmPictureId) => _$this._bmPictureId = bmPictureId;

  String? _bmPictureUrl;
  String? get bmPictureUrl => _$this._bmPictureUrl;
  set bmPictureUrl(String? bmPictureUrl) => _$this._bmPictureUrl = bmPictureUrl;

  String? _bmName;
  String? get bmName => _$this._bmName;
  set bmName(String? bmName) => _$this._bmName = bmName;

  String? _bmCapturedTime;
  String? get bmCapturedTime => _$this._bmCapturedTime;
  set bmCapturedTime(String? bmCapturedTime) =>
      _$this._bmCapturedTime = bmCapturedTime;

  int? _iaPictureId;
  int? get iaPictureId => _$this._iaPictureId;
  set iaPictureId(int? iaPictureId) => _$this._iaPictureId = iaPictureId;

  String? _iaPictureUrl;
  String? get iaPictureUrl => _$this._iaPictureUrl;
  set iaPictureUrl(String? iaPictureUrl) => _$this._iaPictureUrl = iaPictureUrl;

  String? _iaName;
  String? get iaName => _$this._iaName;
  set iaName(String? iaName) => _$this._iaName = iaName;

  String? _iaCapturedTime;
  String? get iaCapturedTime => _$this._iaCapturedTime;
  set iaCapturedTime(String? iaCapturedTime) =>
      _$this._iaCapturedTime = iaCapturedTime;

  LoanCustomerBuilder? _customer;
  LoanCustomerBuilder get customer =>
      _$this._customer ??= new LoanCustomerBuilder();
  set customer(LoanCustomerBuilder? customer) => _$this._customer = customer;

  EligibilityDetailBuilder? _eligibilityDetail;
  EligibilityDetailBuilder get eligibilityDetail =>
      _$this._eligibilityDetail ??= new EligibilityDetailBuilder();
  set eligibilityDetail(EligibilityDetailBuilder? eligibilityDetail) =>
      _$this._eligibilityDetail = eligibilityDetail;

  LoanSummaryBuilder();

  LoanSummaryBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _additionalDetailsJson = $v.additionalDetailsJson?.toBuilder();
      _outstandingAmount = $v.outstandingAmount;
      _numberOfPendingDues = $v.numberOfPendingDues;
      _appliedDate = $v.appliedDate;
      _preferredDueDate = $v.preferredDueDate;
      _id = $v.id;
      _code = $v.code;
      _centerId = $v.centerId;
      _centerName = $v.centerName;
      _createdBy = $v.createdBy?.toBuilder();
      _createdById = $v.createdById;
      _loanTypeId = $v.loanTypeId;
      _loanTypeName = $v.loanTypeName;
      _loanTypeCode = $v.loanTypeCode;
      _estimatedCost = $v.estimatedCost;
      _requestedLoanAmt = $v.requestedLoanAmt;
      _status = $v.status;
      _loanPurposeId = $v.loanPurposeId;
      _loanPurposeName = $v.loanPurposeName;
      _organisationId = $v.organisationId;
      _organisationName = $v.organisationName;
      _croPictureId = $v.croPictureId;
      _croPictureUrl = $v.croPictureUrl;
      _croName = $v.croName;
      _croCapturedTime = $v.croCapturedTime;
      _currentStep = $v.currentStep;
      _tenureInMonths = $v.tenureInMonths;
      _interestRate = $v.interestRate;
      _expectedStartDate = $v.expectedStartDate;
      _bmPictureId = $v.bmPictureId;
      _bmPictureUrl = $v.bmPictureUrl;
      _bmName = $v.bmName;
      _bmCapturedTime = $v.bmCapturedTime;
      _iaPictureId = $v.iaPictureId;
      _iaPictureUrl = $v.iaPictureUrl;
      _iaName = $v.iaName;
      _iaCapturedTime = $v.iaCapturedTime;
      _customer = $v.customer?.toBuilder();
      _eligibilityDetail = $v.eligibilityDetail?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanSummary other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanSummary;
  }

  @override
  void update(void Function(LoanSummaryBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanSummary build() => _build();

  _$LoanSummary _build() {
    _$LoanSummary _$result;
    try {
      _$result = _$v ??
          new _$LoanSummary._(
              additionalDetailsJson: _additionalDetailsJson?.build(),
              outstandingAmount: outstandingAmount,
              numberOfPendingDues: numberOfPendingDues,
              appliedDate: appliedDate,
              preferredDueDate: preferredDueDate,
              id: id,
              code: code,
              centerId: centerId,
              centerName: centerName,
              createdBy: _createdBy?.build(),
              createdById: createdById,
              loanTypeId: loanTypeId,
              loanTypeName: loanTypeName,
              loanTypeCode: loanTypeCode,
              estimatedCost: estimatedCost,
              requestedLoanAmt: requestedLoanAmt,
              status: status,
              loanPurposeId: loanPurposeId,
              loanPurposeName: loanPurposeName,
              organisationId: organisationId,
              organisationName: organisationName,
              croPictureId: croPictureId,
              croPictureUrl: croPictureUrl,
              croName: croName,
              croCapturedTime: croCapturedTime,
              currentStep: currentStep,
              tenureInMonths: tenureInMonths,
              interestRate: interestRate,
              expectedStartDate: expectedStartDate,
              bmPictureId: bmPictureId,
              bmPictureUrl: bmPictureUrl,
              bmName: bmName,
              bmCapturedTime: bmCapturedTime,
              iaPictureId: iaPictureId,
              iaPictureUrl: iaPictureUrl,
              iaName: iaName,
              iaCapturedTime: iaCapturedTime,
              customer: _customer?.build(),
              eligibilityDetail: _eligibilityDetail?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'additionalDetailsJson';
        _additionalDetailsJson?.build();

        _$failedField = 'createdBy';
        _createdBy?.build();

        _$failedField = 'customer';
        _customer?.build();
        _$failedField = 'eligibilityDetail';
        _eligibilityDetail?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'LoanSummary', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
