// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'family_detail.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<FamilyDetail> _$familyDetailSerializer =
    new _$FamilyDetailSerializer();

class _$FamilyDetailSerializer implements StructuredSerializer<FamilyDetail> {
  @override
  final Iterable<Type> types = const [FamilyDetail, _$FamilyDetail];
  @override
  final String wireName = 'FamilyDetail';

  @override
  Iterable<Object?> serialize(Serializers serializers, FamilyDetail object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.houseRentExpense;
    if (value != null) {
      result
        ..add('house_rent_expense')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.educationExpense;
    if (value != null) {
      result
        ..add('education_expense')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.otherExpenditure;
    if (value != null) {
      result
        ..add('other_expenditure')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.electricityExpense;
    if (value != null) {
      result
        ..add('electricity_expense')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.totalMonthlyIncome;
    if (value != null) {
      result
        ..add('total_monthly_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.totalMonthlyExpense;
    if (value != null) {
      result
        ..add('total_monthly_expense')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.totalMonthlyEMI;
    if (value != null) {
      result
        ..add('total_monthly_emi')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.totalMonthlyBalance;
    if (value != null) {
      result
        ..add('total_monthly_balance')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  FamilyDetail deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FamilyDetailBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'house_rent_expense':
          result.houseRentExpense = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'education_expense':
          result.educationExpense = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'other_expenditure':
          result.otherExpenditure = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'electricity_expense':
          result.electricityExpense = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'total_monthly_income':
          result.totalMonthlyIncome = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'total_monthly_expense':
          result.totalMonthlyExpense = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'total_monthly_emi':
          result.totalMonthlyEMI = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'total_monthly_balance':
          result.totalMonthlyBalance = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
      }
    }

    return result.build();
  }
}

class _$FamilyDetail extends FamilyDetail {
  @override
  final int? id;
  @override
  final double? houseRentExpense;
  @override
  final double? educationExpense;
  @override
  final double? otherExpenditure;
  @override
  final double? electricityExpense;
  @override
  final double? totalMonthlyIncome;
  @override
  final double? totalMonthlyExpense;
  @override
  final double? totalMonthlyEMI;
  @override
  final double? totalMonthlyBalance;

  factory _$FamilyDetail([void Function(FamilyDetailBuilder)? updates]) =>
      (new FamilyDetailBuilder()..update(updates))._build();

  _$FamilyDetail._(
      {this.id,
      this.houseRentExpense,
      this.educationExpense,
      this.otherExpenditure,
      this.electricityExpense,
      this.totalMonthlyIncome,
      this.totalMonthlyExpense,
      this.totalMonthlyEMI,
      this.totalMonthlyBalance})
      : super._();

  @override
  FamilyDetail rebuild(void Function(FamilyDetailBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FamilyDetailBuilder toBuilder() => new FamilyDetailBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FamilyDetail &&
        id == other.id &&
        houseRentExpense == other.houseRentExpense &&
        educationExpense == other.educationExpense &&
        otherExpenditure == other.otherExpenditure &&
        electricityExpense == other.electricityExpense &&
        totalMonthlyIncome == other.totalMonthlyIncome &&
        totalMonthlyExpense == other.totalMonthlyExpense &&
        totalMonthlyEMI == other.totalMonthlyEMI &&
        totalMonthlyBalance == other.totalMonthlyBalance;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, houseRentExpense.hashCode);
    _$hash = $jc(_$hash, educationExpense.hashCode);
    _$hash = $jc(_$hash, otherExpenditure.hashCode);
    _$hash = $jc(_$hash, electricityExpense.hashCode);
    _$hash = $jc(_$hash, totalMonthlyIncome.hashCode);
    _$hash = $jc(_$hash, totalMonthlyExpense.hashCode);
    _$hash = $jc(_$hash, totalMonthlyEMI.hashCode);
    _$hash = $jc(_$hash, totalMonthlyBalance.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FamilyDetail')
          ..add('id', id)
          ..add('houseRentExpense', houseRentExpense)
          ..add('educationExpense', educationExpense)
          ..add('otherExpenditure', otherExpenditure)
          ..add('electricityExpense', electricityExpense)
          ..add('totalMonthlyIncome', totalMonthlyIncome)
          ..add('totalMonthlyExpense', totalMonthlyExpense)
          ..add('totalMonthlyEMI', totalMonthlyEMI)
          ..add('totalMonthlyBalance', totalMonthlyBalance))
        .toString();
  }
}

class FamilyDetailBuilder
    implements Builder<FamilyDetail, FamilyDetailBuilder> {
  _$FamilyDetail? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  double? _houseRentExpense;
  double? get houseRentExpense => _$this._houseRentExpense;
  set houseRentExpense(double? houseRentExpense) =>
      _$this._houseRentExpense = houseRentExpense;

  double? _educationExpense;
  double? get educationExpense => _$this._educationExpense;
  set educationExpense(double? educationExpense) =>
      _$this._educationExpense = educationExpense;

  double? _otherExpenditure;
  double? get otherExpenditure => _$this._otherExpenditure;
  set otherExpenditure(double? otherExpenditure) =>
      _$this._otherExpenditure = otherExpenditure;

  double? _electricityExpense;
  double? get electricityExpense => _$this._electricityExpense;
  set electricityExpense(double? electricityExpense) =>
      _$this._electricityExpense = electricityExpense;

  double? _totalMonthlyIncome;
  double? get totalMonthlyIncome => _$this._totalMonthlyIncome;
  set totalMonthlyIncome(double? totalMonthlyIncome) =>
      _$this._totalMonthlyIncome = totalMonthlyIncome;

  double? _totalMonthlyExpense;
  double? get totalMonthlyExpense => _$this._totalMonthlyExpense;
  set totalMonthlyExpense(double? totalMonthlyExpense) =>
      _$this._totalMonthlyExpense = totalMonthlyExpense;

  double? _totalMonthlyEMI;
  double? get totalMonthlyEMI => _$this._totalMonthlyEMI;
  set totalMonthlyEMI(double? totalMonthlyEMI) =>
      _$this._totalMonthlyEMI = totalMonthlyEMI;

  double? _totalMonthlyBalance;
  double? get totalMonthlyBalance => _$this._totalMonthlyBalance;
  set totalMonthlyBalance(double? totalMonthlyBalance) =>
      _$this._totalMonthlyBalance = totalMonthlyBalance;

  FamilyDetailBuilder();

  FamilyDetailBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _houseRentExpense = $v.houseRentExpense;
      _educationExpense = $v.educationExpense;
      _otherExpenditure = $v.otherExpenditure;
      _electricityExpense = $v.electricityExpense;
      _totalMonthlyIncome = $v.totalMonthlyIncome;
      _totalMonthlyExpense = $v.totalMonthlyExpense;
      _totalMonthlyEMI = $v.totalMonthlyEMI;
      _totalMonthlyBalance = $v.totalMonthlyBalance;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FamilyDetail other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FamilyDetail;
  }

  @override
  void update(void Function(FamilyDetailBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FamilyDetail build() => _build();

  _$FamilyDetail _build() {
    final _$result = _$v ??
        new _$FamilyDetail._(
            id: id,
            houseRentExpense: houseRentExpense,
            educationExpense: educationExpense,
            otherExpenditure: otherExpenditure,
            electricityExpense: electricityExpense,
            totalMonthlyIncome: totalMonthlyIncome,
            totalMonthlyExpense: totalMonthlyExpense,
            totalMonthlyEMI: totalMonthlyEMI,
            totalMonthlyBalance: totalMonthlyBalance);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
