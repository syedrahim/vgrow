import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'loan_address.g.dart';

abstract class LoanAddress implements Built<LoanAddress, LoanAddressBuilder> {
  LoanAddress._();
  factory LoanAddress(
      [LoanAddressBuilder updates(LoanAddressBuilder builder)]) = _$LoanAddress;

  int? get id;

  @BuiltValueField(wireName: 'addressable_type')
  String? get addressableType;

  @BuiltValueField(wireName: 'addressable_id')
  int? get addressableID;

  @BuiltValueField(wireName: 'address_line_1')
  String? get addressLine1;

  @BuiltValueField(wireName: 'address_line_2')
  String? get addressLine2;

  @BuiltValueField(wireName: 'city_id')
  int? get cityID;

  @BuiltValueField(wireName: 'city_name')
  String? get cityName;

  @BuiltValueField(wireName: 'state_id')
  int? get stateID;

  @BuiltValueField(wireName: 'state_name')
  String? get stateName;

  String? get pincode;

  String? get landmark;

  double? get latitude;

  double? get longitude;

  BuiltList<LoanAddressAttachment> get address_attachments;

  static Serializer<LoanAddress> get serializer => _$loanAddressSerializer;

  @override
  String toString() {
    // TODO: implement toString
    return "$addressLine1 $addressLine2 $cityName $stateName $pincode";
  }
}

abstract class LoanAddressAttachment
    implements Built<LoanAddressAttachment, LoanAddressAttachmentBuilder> {
  LoanAddressAttachment._();
  factory LoanAddressAttachment(
      [LoanAddressAttachmentBuilder updates(
          LoanAddressAttachmentBuilder builder)]) = _$LoanAddressAttachment;

  static Serializer<LoanAddressAttachment> get serializer =>
      _$loanAddressAttachmentSerializer;

  int? get id;
  int? get attachmentId;
  String? get attachmentUrl;
  String? get employeeRoleCode;
  int? get employeeId;
}
