// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_document.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanDocument> _$loanDocumentSerializer =
    new _$LoanDocumentSerializer();

class _$LoanDocumentSerializer implements StructuredSerializer<LoanDocument> {
  @override
  final Iterable<Type> types = const [LoanDocument, _$LoanDocument];
  @override
  final String wireName = 'LoanDocument';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanDocument object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.ownerType;
    if (value != null) {
      result
        ..add('owner_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.ownerID;
    if (value != null) {
      result
        ..add('owner_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.documentType;
    if (value != null) {
      result
        ..add('document_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.documentName;
    if (value != null) {
      result
        ..add('document_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdByID;
    if (value != null) {
      result
        ..add('created_by_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.createdByRoleCode;
    if (value != null) {
      result
        ..add('created_by_role_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.frontSideImageID;
    if (value != null) {
      result
        ..add('front_side_image_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.backSideImageID;
    if (value != null) {
      result
        ..add('back_side_image_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.frontSideImageUrl;
    if (value != null) {
      result
        ..add('front_side_image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.backSideImageUrl;
    if (value != null) {
      result
        ..add('back_side_image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  LoanDocument deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanDocumentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'owner_type':
          result.ownerType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'owner_id':
          result.ownerID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'document_type':
          result.documentType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'document_name':
          result.documentName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_by_id':
          result.createdByID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'created_by_role_code':
          result.createdByRoleCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'front_side_image_id':
          result.frontSideImageID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'back_side_image_id':
          result.backSideImageID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'front_side_image_url':
          result.frontSideImageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'back_side_image_url':
          result.backSideImageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$LoanDocument extends LoanDocument {
  @override
  final int? id;
  @override
  final String? ownerType;
  @override
  final int? ownerID;
  @override
  final String? documentType;
  @override
  final String? documentName;
  @override
  final int? createdByID;
  @override
  final String? createdByRoleCode;
  @override
  final String? code;
  @override
  final int? frontSideImageID;
  @override
  final int? backSideImageID;
  @override
  final String? frontSideImageUrl;
  @override
  final String? backSideImageUrl;

  factory _$LoanDocument([void Function(LoanDocumentBuilder)? updates]) =>
      (new LoanDocumentBuilder()..update(updates))._build();

  _$LoanDocument._(
      {this.id,
      this.ownerType,
      this.ownerID,
      this.documentType,
      this.documentName,
      this.createdByID,
      this.createdByRoleCode,
      this.code,
      this.frontSideImageID,
      this.backSideImageID,
      this.frontSideImageUrl,
      this.backSideImageUrl})
      : super._();

  @override
  LoanDocument rebuild(void Function(LoanDocumentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanDocumentBuilder toBuilder() => new LoanDocumentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanDocument &&
        id == other.id &&
        ownerType == other.ownerType &&
        ownerID == other.ownerID &&
        documentType == other.documentType &&
        documentName == other.documentName &&
        createdByID == other.createdByID &&
        createdByRoleCode == other.createdByRoleCode &&
        code == other.code &&
        frontSideImageID == other.frontSideImageID &&
        backSideImageID == other.backSideImageID &&
        frontSideImageUrl == other.frontSideImageUrl &&
        backSideImageUrl == other.backSideImageUrl;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, ownerType.hashCode);
    _$hash = $jc(_$hash, ownerID.hashCode);
    _$hash = $jc(_$hash, documentType.hashCode);
    _$hash = $jc(_$hash, documentName.hashCode);
    _$hash = $jc(_$hash, createdByID.hashCode);
    _$hash = $jc(_$hash, createdByRoleCode.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, frontSideImageID.hashCode);
    _$hash = $jc(_$hash, backSideImageID.hashCode);
    _$hash = $jc(_$hash, frontSideImageUrl.hashCode);
    _$hash = $jc(_$hash, backSideImageUrl.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanDocument')
          ..add('id', id)
          ..add('ownerType', ownerType)
          ..add('ownerID', ownerID)
          ..add('documentType', documentType)
          ..add('documentName', documentName)
          ..add('createdByID', createdByID)
          ..add('createdByRoleCode', createdByRoleCode)
          ..add('code', code)
          ..add('frontSideImageID', frontSideImageID)
          ..add('backSideImageID', backSideImageID)
          ..add('frontSideImageUrl', frontSideImageUrl)
          ..add('backSideImageUrl', backSideImageUrl))
        .toString();
  }
}

class LoanDocumentBuilder
    implements Builder<LoanDocument, LoanDocumentBuilder> {
  _$LoanDocument? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _ownerType;
  String? get ownerType => _$this._ownerType;
  set ownerType(String? ownerType) => _$this._ownerType = ownerType;

  int? _ownerID;
  int? get ownerID => _$this._ownerID;
  set ownerID(int? ownerID) => _$this._ownerID = ownerID;

  String? _documentType;
  String? get documentType => _$this._documentType;
  set documentType(String? documentType) => _$this._documentType = documentType;

  String? _documentName;
  String? get documentName => _$this._documentName;
  set documentName(String? documentName) => _$this._documentName = documentName;

  int? _createdByID;
  int? get createdByID => _$this._createdByID;
  set createdByID(int? createdByID) => _$this._createdByID = createdByID;

  String? _createdByRoleCode;
  String? get createdByRoleCode => _$this._createdByRoleCode;
  set createdByRoleCode(String? createdByRoleCode) =>
      _$this._createdByRoleCode = createdByRoleCode;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  int? _frontSideImageID;
  int? get frontSideImageID => _$this._frontSideImageID;
  set frontSideImageID(int? frontSideImageID) =>
      _$this._frontSideImageID = frontSideImageID;

  int? _backSideImageID;
  int? get backSideImageID => _$this._backSideImageID;
  set backSideImageID(int? backSideImageID) =>
      _$this._backSideImageID = backSideImageID;

  String? _frontSideImageUrl;
  String? get frontSideImageUrl => _$this._frontSideImageUrl;
  set frontSideImageUrl(String? frontSideImageUrl) =>
      _$this._frontSideImageUrl = frontSideImageUrl;

  String? _backSideImageUrl;
  String? get backSideImageUrl => _$this._backSideImageUrl;
  set backSideImageUrl(String? backSideImageUrl) =>
      _$this._backSideImageUrl = backSideImageUrl;

  LoanDocumentBuilder();

  LoanDocumentBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _ownerType = $v.ownerType;
      _ownerID = $v.ownerID;
      _documentType = $v.documentType;
      _documentName = $v.documentName;
      _createdByID = $v.createdByID;
      _createdByRoleCode = $v.createdByRoleCode;
      _code = $v.code;
      _frontSideImageID = $v.frontSideImageID;
      _backSideImageID = $v.backSideImageID;
      _frontSideImageUrl = $v.frontSideImageUrl;
      _backSideImageUrl = $v.backSideImageUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanDocument other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanDocument;
  }

  @override
  void update(void Function(LoanDocumentBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanDocument build() => _build();

  _$LoanDocument _build() {
    final _$result = _$v ??
        new _$LoanDocument._(
            id: id,
            ownerType: ownerType,
            ownerID: ownerID,
            documentType: documentType,
            documentName: documentName,
            createdByID: createdByID,
            createdByRoleCode: createdByRoleCode,
            code: code,
            frontSideImageID: frontSideImageID,
            backSideImageID: backSideImageID,
            frontSideImageUrl: frontSideImageUrl,
            backSideImageUrl: backSideImageUrl);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
