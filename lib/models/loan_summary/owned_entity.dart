import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'owned_entity.g.dart';

abstract class OwnedEntity implements Built<OwnedEntity, OwnedEntityBuilder>{
  OwnedEntity._();
  factory OwnedEntity([OwnedEntityBuilder updates(OwnedEntityBuilder builder)]) = _$OwnedEntity;

  int? get id;

  @BuiltValueField(wireName: 'owner_type')
  String? get ownerType;

  @BuiltValueField(wireName: 'owner_id')
  int? get ownerID;

  @BuiltValueField(wireName: 'entity_category')
  String? get entityCategory;

  @BuiltValueField(wireName: 'entity_type')
  String? get entityType;

  @BuiltValueField(wireName: 'entity_worth')
  double? get entityWorth;

  @BuiltValueField(wireName: 'number_of_entities')
  int? get numberOfEntities;

  String? get code;

  static Serializer<OwnedEntity> get serializer => _$ownedEntitySerializer;
}