import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'bank_detail.g.dart';

abstract class BankDetail implements Built<BankDetail, BankDetailBuilder>{
  BankDetail._();
  factory BankDetail([BankDetailBuilder updates(BankDetailBuilder builder)]) = _$BankDetail;

  int? get id;

  @BuiltValueField(wireName: 'owner_type')
  String? get ownerType;

  @BuiltValueField(wireName: 'owner_id')
  int? get ownerID;

  @BuiltValueField(wireName: 'account_number')
  String? get accountNumber;

  @BuiltValueField(wireName: 'bank_name')
  String? get bankName;

  @BuiltValueField(wireName: 'ifsc_code')
  String? get ifscCode;

  @BuiltValueField(wireName: 'passbook_or_cheque_document_id')
  int? get passbookOrChequeDocumentID;

  @BuiltValueField(wireName: 'passbook_or_cheque_document_url')
  String? get passbookOrChequeDocumentUrl;

  @BuiltValueField(wireName: 'branch_name')
  String? get branchName;

  @BuiltValueField(wireName: 'registered_name')
  String? get registeredName;

  @BuiltValueField(wireName: 'name_match')
  String? get nameMatch;

  String? get score;

  static Serializer<BankDetail> get serializer => _$bankDetailSerializer;
}