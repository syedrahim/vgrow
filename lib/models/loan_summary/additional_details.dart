import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'loan_address.dart';

part 'additional_details.g.dart';

abstract class AdditionalDetailsJson implements Built<AdditionalDetailsJson, AdditionalDetailsJsonBuilder>{
  AdditionalDetailsJson._();
  factory AdditionalDetailsJson([AdditionalDetailsJsonBuilder updates(AdditionalDetailsJsonBuilder builder)]) = _$AdditionalDetailsJson;

  // @BuiltValueField(wireName: 'type_of_agriculture')
  // String? get typeOfAgriculture;
  //
  // String? get acres;

  @BuiltValueField(wireName: 'nature_of_business')
  String? get natureOfBusiness;

  @BuiltValueField(wireName: 'enterprise_name')
  String? get enterpriseName;

  @BuiltValueField(wireName: 'address')
  LoanAddress? get address;

  @BuiltValueField(wireName: 'year_of_establishment')
  String? get yearOfEstablishment;

  @BuiltValueField(wireName: 'monthly_revenue')
  double? get monthlyRevenue;

  @BuiltValueField(wireName: 'direct_cost')
  double? get directCost;

  @BuiltValueField(wireName: 'indirect_cost')
  double? get indirectCost;

  @BuiltValueField(wireName: 'no_of_goods_in_asset')
  int? get noOfGoodsInasset;

  @BuiltValueField(wireName: 'no_of_infrastructure_in_asset')
  int? get noOfInfrastructureInasset;

  @BuiltValueField(wireName: 'infrastructure_in_asset_value')
  int? get infrastructureInAssetValue;

  @BuiltValueField(wireName: 'receivable_cash_or_debit')
  double? get receivableCashOrDebit;

  @BuiltValueField(wireName: 'payables_to_suppliers')
  double? get payableToSuppliers;

  @BuiltValueField(wireName: 'no_of_infrastructure_in_liability')
  double? get no_of_infrastructure_in_liability;

  @BuiltValueField(wireName: 'monthly_income')
  double? get monthly_income;

  @BuiltValueField(wireName: 'monthly_expense')
  double? get monthly_expense;

  static Serializer<AdditionalDetailsJson> get serializer => _$additionalDetailsJsonSerializer;
}