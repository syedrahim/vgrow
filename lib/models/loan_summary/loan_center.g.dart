// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_center.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanCenter> _$loanCenterSerializer = new _$LoanCenterSerializer();

class _$LoanCenterSerializer implements StructuredSerializer<LoanCenter> {
  @override
  final Iterable<Type> types = const [LoanCenter, _$LoanCenter];
  @override
  final String wireName = 'LoanCenter';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanCenter object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.branch;
    if (value != null) {
      result
        ..add('branch')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(LoanBranch)));
    }
    return result;
  }

  @override
  LoanCenter deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanCenterBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'branch':
          result.branch.replace(serializers.deserialize(value,
              specifiedType: const FullType(LoanBranch))! as LoanBranch);
          break;
      }
    }

    return result.build();
  }
}

class _$LoanCenter extends LoanCenter {
  @override
  final int? id;
  @override
  final String? name;
  @override
  final String? code;
  @override
  final LoanBranch? branch;

  factory _$LoanCenter([void Function(LoanCenterBuilder)? updates]) =>
      (new LoanCenterBuilder()..update(updates))._build();

  _$LoanCenter._({this.id, this.name, this.code, this.branch}) : super._();

  @override
  LoanCenter rebuild(void Function(LoanCenterBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanCenterBuilder toBuilder() => new LoanCenterBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanCenter &&
        id == other.id &&
        name == other.name &&
        code == other.code &&
        branch == other.branch;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, branch.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanCenter')
          ..add('id', id)
          ..add('name', name)
          ..add('code', code)
          ..add('branch', branch))
        .toString();
  }
}

class LoanCenterBuilder implements Builder<LoanCenter, LoanCenterBuilder> {
  _$LoanCenter? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  LoanBranchBuilder? _branch;
  LoanBranchBuilder get branch => _$this._branch ??= new LoanBranchBuilder();
  set branch(LoanBranchBuilder? branch) => _$this._branch = branch;

  LoanCenterBuilder();

  LoanCenterBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _code = $v.code;
      _branch = $v.branch?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanCenter other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanCenter;
  }

  @override
  void update(void Function(LoanCenterBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanCenter build() => _build();

  _$LoanCenter _build() {
    _$LoanCenter _$result;
    try {
      _$result = _$v ??
          new _$LoanCenter._(
              id: id, name: name, code: code, branch: _branch?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'branch';
        _branch?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'LoanCenter', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
