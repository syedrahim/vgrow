// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_customer.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanCustomer> _$loanCustomerSerializer =
    new _$LoanCustomerSerializer();

class _$LoanCustomerSerializer implements StructuredSerializer<LoanCustomer> {
  @override
  final Iterable<Type> types = const [LoanCustomer, _$LoanCustomer];
  @override
  final String wireName = 'LoanCustomer';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanCustomer object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'family_members',
      serializers.serialize(object.familyMembers,
          specifiedType:
              const FullType(BuiltList, const [const FullType(FamilyMember)])),
      'existing_loans',
      serializers.serialize(object.existingLoans,
          specifiedType:
              const FullType(BuiltList, const [const FullType(ExistingLoan)])),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.email;
    if (value != null) {
      result
        ..add('email')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.firstname;
    if (value != null) {
      result
        ..add('firstname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastname;
    if (value != null) {
      result
        ..add('lastname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isdCode;
    if (value != null) {
      result
        ..add('isd_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.phoneNumber;
    if (value != null) {
      result
        ..add('phone_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.profilePictureID;
    if (value != null) {
      result
        ..add('profile_pic_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.profilePictureUrl;
    if (value != null) {
      result
        ..add('profile_pic_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.gender;
    if (value != null) {
      result
        ..add('gender')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isActive;
    if (value != null) {
      result
        ..add('is_active')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.dob;
    if (value != null) {
      result
        ..add('dob')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.panNumber;
    if (value != null) {
      result
        ..add('pan_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.panDocumentID;
    if (value != null) {
      result
        ..add('pan_document_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.panDocumentUrl;
    if (value != null) {
      result
        ..add('pan_document_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.educationalQualification;
    if (value != null) {
      result
        ..add('educational_qualification')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.aadhaarNumber;
    if (value != null) {
      result
        ..add('aadhaar_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.age;
    if (value != null) {
      result
        ..add('age')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.occupation;
    if (value != null) {
      result
        ..add('occupation')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.monthlyIncome;
    if (value != null) {
      result
        ..add('monthly_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.doesOwnBusiness;
    if (value != null) {
      result
        ..add('does_own_business')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.businessIncome;
    if (value != null) {
      result
        ..add('business_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.address;
    if (value != null) {
      result
        ..add('address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(LoanAddress)));
    }
    value = object.crifScore;
    if (value != null) {
      result
        ..add('crif_score')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CrifScore)));
    }
    value = object.aadharFrontImageID;
    if (value != null) {
      result
        ..add('aadhaar_front_image_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.aadharFrontImageUrl;
    if (value != null) {
      result
        ..add('aadhaar_front_image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.aadharBackImageID;
    if (value != null) {
      result
        ..add('aadhaar_back_image_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.aadharBackImageUrl;
    if (value != null) {
      result
        ..add('aadhaar_back_image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isEmailVerified;
    if (value != null) {
      result
        ..add('is_email_verified')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.isMobileVerified;
    if (value != null) {
      result
        ..add('is_mobile_verified')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.documents;
    if (value != null) {
      result
        ..add('documents')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(LoanDocument)])));
    }
    value = object.ownedEntities;
    if (value != null) {
      result
        ..add('owned_entities')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(OwnedEntity)])));
    }
    value = object.bankDetail;
    if (value != null) {
      result
        ..add('bank_detail')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BankDetail)));
    }
    value = object.familyDetail;
    if (value != null) {
      result
        ..add('family_detail')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(FamilyDetail)));
    }
    return result;
  }

  @override
  LoanCustomer deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanCustomerBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'firstname':
          result.firstname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'lastname':
          result.lastname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'isd_code':
          result.isdCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'phone_number':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'profile_pic_id':
          result.profilePictureID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'profile_pic_url':
          result.profilePictureUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'is_active':
          result.isActive = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'dob':
          result.dob = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pan_number':
          result.panNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pan_document_id':
          result.panDocumentID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'pan_document_url':
          result.panDocumentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'educational_qualification':
          result.educationalQualification = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'aadhaar_number':
          result.aadhaarNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'age':
          result.age = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'occupation':
          result.occupation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'monthly_income':
          result.monthlyIncome = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'does_own_business':
          result.doesOwnBusiness = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'business_income':
          result.businessIncome = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'address':
          result.address.replace(serializers.deserialize(value,
              specifiedType: const FullType(LoanAddress))! as LoanAddress);
          break;
        case 'crif_score':
          result.crifScore.replace(serializers.deserialize(value,
              specifiedType: const FullType(CrifScore))! as CrifScore);
          break;
        case 'aadhaar_front_image_id':
          result.aadharFrontImageID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'aadhaar_front_image_url':
          result.aadharFrontImageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'aadhaar_back_image_id':
          result.aadharBackImageID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'aadhaar_back_image_url':
          result.aadharBackImageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'is_email_verified':
          result.isEmailVerified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'is_mobile_verified':
          result.isMobileVerified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'documents':
          result.documents.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(LoanDocument)]))!
              as BuiltList<Object?>);
          break;
        case 'owned_entities':
          result.ownedEntities.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(OwnedEntity)]))!
              as BuiltList<Object?>);
          break;
        case 'bank_detail':
          result.bankDetail.replace(serializers.deserialize(value,
              specifiedType: const FullType(BankDetail))! as BankDetail);
          break;
        case 'family_members':
          result.familyMembers.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(FamilyMember)]))!
              as BuiltList<Object?>);
          break;
        case 'family_detail':
          result.familyDetail.replace(serializers.deserialize(value,
              specifiedType: const FullType(FamilyDetail))! as FamilyDetail);
          break;
        case 'existing_loans':
          result.existingLoans.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ExistingLoan)]))!
              as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$LoanCustomer extends LoanCustomer {
  @override
  final int? id;
  @override
  final String? code;
  @override
  final String? email;
  @override
  final String? firstname;
  @override
  final String? lastname;
  @override
  final String? isdCode;
  @override
  final String? phoneNumber;
  @override
  final int? profilePictureID;
  @override
  final String? profilePictureUrl;
  @override
  final String? gender;
  @override
  final bool? isActive;
  @override
  final String? dob;
  @override
  final String? panNumber;
  @override
  final int? panDocumentID;
  @override
  final String? panDocumentUrl;
  @override
  final String? educationalQualification;
  @override
  final String? aadhaarNumber;
  @override
  final int? age;
  @override
  final String? occupation;
  @override
  final double? monthlyIncome;
  @override
  final bool? doesOwnBusiness;
  @override
  final double? businessIncome;
  @override
  final LoanAddress? address;
  @override
  final CrifScore? crifScore;
  @override
  final int? aadharFrontImageID;
  @override
  final String? aadharFrontImageUrl;
  @override
  final int? aadharBackImageID;
  @override
  final String? aadharBackImageUrl;
  @override
  final bool? isEmailVerified;
  @override
  final bool? isMobileVerified;
  @override
  final BuiltList<LoanDocument>? documents;
  @override
  final BuiltList<OwnedEntity>? ownedEntities;
  @override
  final BankDetail? bankDetail;
  @override
  final BuiltList<FamilyMember> familyMembers;
  @override
  final FamilyDetail? familyDetail;
  @override
  final BuiltList<ExistingLoan> existingLoans;

  factory _$LoanCustomer([void Function(LoanCustomerBuilder)? updates]) =>
      (new LoanCustomerBuilder()..update(updates))._build();

  _$LoanCustomer._(
      {this.id,
      this.code,
      this.email,
      this.firstname,
      this.lastname,
      this.isdCode,
      this.phoneNumber,
      this.profilePictureID,
      this.profilePictureUrl,
      this.gender,
      this.isActive,
      this.dob,
      this.panNumber,
      this.panDocumentID,
      this.panDocumentUrl,
      this.educationalQualification,
      this.aadhaarNumber,
      this.age,
      this.occupation,
      this.monthlyIncome,
      this.doesOwnBusiness,
      this.businessIncome,
      this.address,
      this.crifScore,
      this.aadharFrontImageID,
      this.aadharFrontImageUrl,
      this.aadharBackImageID,
      this.aadharBackImageUrl,
      this.isEmailVerified,
      this.isMobileVerified,
      this.documents,
      this.ownedEntities,
      this.bankDetail,
      required this.familyMembers,
      this.familyDetail,
      required this.existingLoans})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        familyMembers, r'LoanCustomer', 'familyMembers');
    BuiltValueNullFieldError.checkNotNull(
        existingLoans, r'LoanCustomer', 'existingLoans');
  }

  @override
  LoanCustomer rebuild(void Function(LoanCustomerBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanCustomerBuilder toBuilder() => new LoanCustomerBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanCustomer &&
        id == other.id &&
        code == other.code &&
        email == other.email &&
        firstname == other.firstname &&
        lastname == other.lastname &&
        isdCode == other.isdCode &&
        phoneNumber == other.phoneNumber &&
        profilePictureID == other.profilePictureID &&
        profilePictureUrl == other.profilePictureUrl &&
        gender == other.gender &&
        isActive == other.isActive &&
        dob == other.dob &&
        panNumber == other.panNumber &&
        panDocumentID == other.panDocumentID &&
        panDocumentUrl == other.panDocumentUrl &&
        educationalQualification == other.educationalQualification &&
        aadhaarNumber == other.aadhaarNumber &&
        age == other.age &&
        occupation == other.occupation &&
        monthlyIncome == other.monthlyIncome &&
        doesOwnBusiness == other.doesOwnBusiness &&
        businessIncome == other.businessIncome &&
        address == other.address &&
        crifScore == other.crifScore &&
        aadharFrontImageID == other.aadharFrontImageID &&
        aadharFrontImageUrl == other.aadharFrontImageUrl &&
        aadharBackImageID == other.aadharBackImageID &&
        aadharBackImageUrl == other.aadharBackImageUrl &&
        isEmailVerified == other.isEmailVerified &&
        isMobileVerified == other.isMobileVerified &&
        documents == other.documents &&
        ownedEntities == other.ownedEntities &&
        bankDetail == other.bankDetail &&
        familyMembers == other.familyMembers &&
        familyDetail == other.familyDetail &&
        existingLoans == other.existingLoans;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, firstname.hashCode);
    _$hash = $jc(_$hash, lastname.hashCode);
    _$hash = $jc(_$hash, isdCode.hashCode);
    _$hash = $jc(_$hash, phoneNumber.hashCode);
    _$hash = $jc(_$hash, profilePictureID.hashCode);
    _$hash = $jc(_$hash, profilePictureUrl.hashCode);
    _$hash = $jc(_$hash, gender.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, dob.hashCode);
    _$hash = $jc(_$hash, panNumber.hashCode);
    _$hash = $jc(_$hash, panDocumentID.hashCode);
    _$hash = $jc(_$hash, panDocumentUrl.hashCode);
    _$hash = $jc(_$hash, educationalQualification.hashCode);
    _$hash = $jc(_$hash, aadhaarNumber.hashCode);
    _$hash = $jc(_$hash, age.hashCode);
    _$hash = $jc(_$hash, occupation.hashCode);
    _$hash = $jc(_$hash, monthlyIncome.hashCode);
    _$hash = $jc(_$hash, doesOwnBusiness.hashCode);
    _$hash = $jc(_$hash, businessIncome.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jc(_$hash, crifScore.hashCode);
    _$hash = $jc(_$hash, aadharFrontImageID.hashCode);
    _$hash = $jc(_$hash, aadharFrontImageUrl.hashCode);
    _$hash = $jc(_$hash, aadharBackImageID.hashCode);
    _$hash = $jc(_$hash, aadharBackImageUrl.hashCode);
    _$hash = $jc(_$hash, isEmailVerified.hashCode);
    _$hash = $jc(_$hash, isMobileVerified.hashCode);
    _$hash = $jc(_$hash, documents.hashCode);
    _$hash = $jc(_$hash, ownedEntities.hashCode);
    _$hash = $jc(_$hash, bankDetail.hashCode);
    _$hash = $jc(_$hash, familyMembers.hashCode);
    _$hash = $jc(_$hash, familyDetail.hashCode);
    _$hash = $jc(_$hash, existingLoans.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanCustomer')
          ..add('id', id)
          ..add('code', code)
          ..add('email', email)
          ..add('firstname', firstname)
          ..add('lastname', lastname)
          ..add('isdCode', isdCode)
          ..add('phoneNumber', phoneNumber)
          ..add('profilePictureID', profilePictureID)
          ..add('profilePictureUrl', profilePictureUrl)
          ..add('gender', gender)
          ..add('isActive', isActive)
          ..add('dob', dob)
          ..add('panNumber', panNumber)
          ..add('panDocumentID', panDocumentID)
          ..add('panDocumentUrl', panDocumentUrl)
          ..add('educationalQualification', educationalQualification)
          ..add('aadhaarNumber', aadhaarNumber)
          ..add('age', age)
          ..add('occupation', occupation)
          ..add('monthlyIncome', monthlyIncome)
          ..add('doesOwnBusiness', doesOwnBusiness)
          ..add('businessIncome', businessIncome)
          ..add('address', address)
          ..add('crifScore', crifScore)
          ..add('aadharFrontImageID', aadharFrontImageID)
          ..add('aadharFrontImageUrl', aadharFrontImageUrl)
          ..add('aadharBackImageID', aadharBackImageID)
          ..add('aadharBackImageUrl', aadharBackImageUrl)
          ..add('isEmailVerified', isEmailVerified)
          ..add('isMobileVerified', isMobileVerified)
          ..add('documents', documents)
          ..add('ownedEntities', ownedEntities)
          ..add('bankDetail', bankDetail)
          ..add('familyMembers', familyMembers)
          ..add('familyDetail', familyDetail)
          ..add('existingLoans', existingLoans))
        .toString();
  }
}

class LoanCustomerBuilder
    implements Builder<LoanCustomer, LoanCustomerBuilder> {
  _$LoanCustomer? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _firstname;
  String? get firstname => _$this._firstname;
  set firstname(String? firstname) => _$this._firstname = firstname;

  String? _lastname;
  String? get lastname => _$this._lastname;
  set lastname(String? lastname) => _$this._lastname = lastname;

  String? _isdCode;
  String? get isdCode => _$this._isdCode;
  set isdCode(String? isdCode) => _$this._isdCode = isdCode;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  int? _profilePictureID;
  int? get profilePictureID => _$this._profilePictureID;
  set profilePictureID(int? profilePictureID) =>
      _$this._profilePictureID = profilePictureID;

  String? _profilePictureUrl;
  String? get profilePictureUrl => _$this._profilePictureUrl;
  set profilePictureUrl(String? profilePictureUrl) =>
      _$this._profilePictureUrl = profilePictureUrl;

  String? _gender;
  String? get gender => _$this._gender;
  set gender(String? gender) => _$this._gender = gender;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  String? _dob;
  String? get dob => _$this._dob;
  set dob(String? dob) => _$this._dob = dob;

  String? _panNumber;
  String? get panNumber => _$this._panNumber;
  set panNumber(String? panNumber) => _$this._panNumber = panNumber;

  int? _panDocumentID;
  int? get panDocumentID => _$this._panDocumentID;
  set panDocumentID(int? panDocumentID) =>
      _$this._panDocumentID = panDocumentID;

  String? _panDocumentUrl;
  String? get panDocumentUrl => _$this._panDocumentUrl;
  set panDocumentUrl(String? panDocumentUrl) =>
      _$this._panDocumentUrl = panDocumentUrl;

  String? _educationalQualification;
  String? get educationalQualification => _$this._educationalQualification;
  set educationalQualification(String? educationalQualification) =>
      _$this._educationalQualification = educationalQualification;

  String? _aadhaarNumber;
  String? get aadhaarNumber => _$this._aadhaarNumber;
  set aadhaarNumber(String? aadhaarNumber) =>
      _$this._aadhaarNumber = aadhaarNumber;

  int? _age;
  int? get age => _$this._age;
  set age(int? age) => _$this._age = age;

  String? _occupation;
  String? get occupation => _$this._occupation;
  set occupation(String? occupation) => _$this._occupation = occupation;

  double? _monthlyIncome;
  double? get monthlyIncome => _$this._monthlyIncome;
  set monthlyIncome(double? monthlyIncome) =>
      _$this._monthlyIncome = monthlyIncome;

  bool? _doesOwnBusiness;
  bool? get doesOwnBusiness => _$this._doesOwnBusiness;
  set doesOwnBusiness(bool? doesOwnBusiness) =>
      _$this._doesOwnBusiness = doesOwnBusiness;

  double? _businessIncome;
  double? get businessIncome => _$this._businessIncome;
  set businessIncome(double? businessIncome) =>
      _$this._businessIncome = businessIncome;

  LoanAddressBuilder? _address;
  LoanAddressBuilder get address =>
      _$this._address ??= new LoanAddressBuilder();
  set address(LoanAddressBuilder? address) => _$this._address = address;

  CrifScoreBuilder? _crifScore;
  CrifScoreBuilder get crifScore =>
      _$this._crifScore ??= new CrifScoreBuilder();
  set crifScore(CrifScoreBuilder? crifScore) => _$this._crifScore = crifScore;

  int? _aadharFrontImageID;
  int? get aadharFrontImageID => _$this._aadharFrontImageID;
  set aadharFrontImageID(int? aadharFrontImageID) =>
      _$this._aadharFrontImageID = aadharFrontImageID;

  String? _aadharFrontImageUrl;
  String? get aadharFrontImageUrl => _$this._aadharFrontImageUrl;
  set aadharFrontImageUrl(String? aadharFrontImageUrl) =>
      _$this._aadharFrontImageUrl = aadharFrontImageUrl;

  int? _aadharBackImageID;
  int? get aadharBackImageID => _$this._aadharBackImageID;
  set aadharBackImageID(int? aadharBackImageID) =>
      _$this._aadharBackImageID = aadharBackImageID;

  String? _aadharBackImageUrl;
  String? get aadharBackImageUrl => _$this._aadharBackImageUrl;
  set aadharBackImageUrl(String? aadharBackImageUrl) =>
      _$this._aadharBackImageUrl = aadharBackImageUrl;

  bool? _isEmailVerified;
  bool? get isEmailVerified => _$this._isEmailVerified;
  set isEmailVerified(bool? isEmailVerified) =>
      _$this._isEmailVerified = isEmailVerified;

  bool? _isMobileVerified;
  bool? get isMobileVerified => _$this._isMobileVerified;
  set isMobileVerified(bool? isMobileVerified) =>
      _$this._isMobileVerified = isMobileVerified;

  ListBuilder<LoanDocument>? _documents;
  ListBuilder<LoanDocument> get documents =>
      _$this._documents ??= new ListBuilder<LoanDocument>();
  set documents(ListBuilder<LoanDocument>? documents) =>
      _$this._documents = documents;

  ListBuilder<OwnedEntity>? _ownedEntities;
  ListBuilder<OwnedEntity> get ownedEntities =>
      _$this._ownedEntities ??= new ListBuilder<OwnedEntity>();
  set ownedEntities(ListBuilder<OwnedEntity>? ownedEntities) =>
      _$this._ownedEntities = ownedEntities;

  BankDetailBuilder? _bankDetail;
  BankDetailBuilder get bankDetail =>
      _$this._bankDetail ??= new BankDetailBuilder();
  set bankDetail(BankDetailBuilder? bankDetail) =>
      _$this._bankDetail = bankDetail;

  ListBuilder<FamilyMember>? _familyMembers;
  ListBuilder<FamilyMember> get familyMembers =>
      _$this._familyMembers ??= new ListBuilder<FamilyMember>();
  set familyMembers(ListBuilder<FamilyMember>? familyMembers) =>
      _$this._familyMembers = familyMembers;

  FamilyDetailBuilder? _familyDetail;
  FamilyDetailBuilder get familyDetail =>
      _$this._familyDetail ??= new FamilyDetailBuilder();
  set familyDetail(FamilyDetailBuilder? familyDetail) =>
      _$this._familyDetail = familyDetail;

  ListBuilder<ExistingLoan>? _existingLoans;
  ListBuilder<ExistingLoan> get existingLoans =>
      _$this._existingLoans ??= new ListBuilder<ExistingLoan>();
  set existingLoans(ListBuilder<ExistingLoan>? existingLoans) =>
      _$this._existingLoans = existingLoans;

  LoanCustomerBuilder();

  LoanCustomerBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _code = $v.code;
      _email = $v.email;
      _firstname = $v.firstname;
      _lastname = $v.lastname;
      _isdCode = $v.isdCode;
      _phoneNumber = $v.phoneNumber;
      _profilePictureID = $v.profilePictureID;
      _profilePictureUrl = $v.profilePictureUrl;
      _gender = $v.gender;
      _isActive = $v.isActive;
      _dob = $v.dob;
      _panNumber = $v.panNumber;
      _panDocumentID = $v.panDocumentID;
      _panDocumentUrl = $v.panDocumentUrl;
      _educationalQualification = $v.educationalQualification;
      _aadhaarNumber = $v.aadhaarNumber;
      _age = $v.age;
      _occupation = $v.occupation;
      _monthlyIncome = $v.monthlyIncome;
      _doesOwnBusiness = $v.doesOwnBusiness;
      _businessIncome = $v.businessIncome;
      _address = $v.address?.toBuilder();
      _crifScore = $v.crifScore?.toBuilder();
      _aadharFrontImageID = $v.aadharFrontImageID;
      _aadharFrontImageUrl = $v.aadharFrontImageUrl;
      _aadharBackImageID = $v.aadharBackImageID;
      _aadharBackImageUrl = $v.aadharBackImageUrl;
      _isEmailVerified = $v.isEmailVerified;
      _isMobileVerified = $v.isMobileVerified;
      _documents = $v.documents?.toBuilder();
      _ownedEntities = $v.ownedEntities?.toBuilder();
      _bankDetail = $v.bankDetail?.toBuilder();
      _familyMembers = $v.familyMembers.toBuilder();
      _familyDetail = $v.familyDetail?.toBuilder();
      _existingLoans = $v.existingLoans.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanCustomer other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanCustomer;
  }

  @override
  void update(void Function(LoanCustomerBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanCustomer build() => _build();

  _$LoanCustomer _build() {
    _$LoanCustomer _$result;
    try {
      _$result = _$v ??
          new _$LoanCustomer._(
              id: id,
              code: code,
              email: email,
              firstname: firstname,
              lastname: lastname,
              isdCode: isdCode,
              phoneNumber: phoneNumber,
              profilePictureID: profilePictureID,
              profilePictureUrl: profilePictureUrl,
              gender: gender,
              isActive: isActive,
              dob: dob,
              panNumber: panNumber,
              panDocumentID: panDocumentID,
              panDocumentUrl: panDocumentUrl,
              educationalQualification: educationalQualification,
              aadhaarNumber: aadhaarNumber,
              age: age,
              occupation: occupation,
              monthlyIncome: monthlyIncome,
              doesOwnBusiness: doesOwnBusiness,
              businessIncome: businessIncome,
              address: _address?.build(),
              crifScore: _crifScore?.build(),
              aadharFrontImageID: aadharFrontImageID,
              aadharFrontImageUrl: aadharFrontImageUrl,
              aadharBackImageID: aadharBackImageID,
              aadharBackImageUrl: aadharBackImageUrl,
              isEmailVerified: isEmailVerified,
              isMobileVerified: isMobileVerified,
              documents: _documents?.build(),
              ownedEntities: _ownedEntities?.build(),
              bankDetail: _bankDetail?.build(),
              familyMembers: familyMembers.build(),
              familyDetail: _familyDetail?.build(),
              existingLoans: existingLoans.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'address';
        _address?.build();
        _$failedField = 'crifScore';
        _crifScore?.build();

        _$failedField = 'documents';
        _documents?.build();
        _$failedField = 'ownedEntities';
        _ownedEntities?.build();
        _$failedField = 'bankDetail';
        _bankDetail?.build();
        _$failedField = 'familyMembers';
        familyMembers.build();
        _$failedField = 'familyDetail';
        _familyDetail?.build();
        _$failedField = 'existingLoans';
        existingLoans.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'LoanCustomer', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
