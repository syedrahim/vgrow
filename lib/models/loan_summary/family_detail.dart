import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'family_detail.g.dart';

abstract class FamilyDetail implements Built<FamilyDetail, FamilyDetailBuilder>{
  FamilyDetail._();
  factory FamilyDetail([FamilyDetailBuilder updates(FamilyDetailBuilder builder)]) = _$FamilyDetail;

  int? get id;

  @BuiltValueField(wireName: 'house_rent_expense')
  double? get houseRentExpense;

  @BuiltValueField(wireName: 'education_expense')
  double? get educationExpense;

  @BuiltValueField(wireName: 'other_expenditure')
  double? get otherExpenditure;

  @BuiltValueField(wireName: 'electricity_expense')
  double? get electricityExpense;

  @BuiltValueField(wireName: 'total_monthly_income')
  double? get totalMonthlyIncome;

  @BuiltValueField(wireName: 'total_monthly_expense')
  double? get totalMonthlyExpense;

  @BuiltValueField(wireName: 'total_monthly_emi')
  double? get totalMonthlyEMI;

  @BuiltValueField(wireName: 'total_monthly_balance')
  double? get totalMonthlyBalance;

  static Serializer<FamilyDetail> get serializer => _$familyDetailSerializer;
}