// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aggreement_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AggreementRequest> _$aggreementRequestSerializer =
    new _$AggreementRequestSerializer();

class _$AggreementRequestSerializer
    implements StructuredSerializer<AggreementRequest> {
  @override
  final Iterable<Type> types = const [AggreementRequest, _$AggreementRequest];
  @override
  final String wireName = 'AggreementRequest';

  @override
  Iterable<Object?> serialize(Serializers serializers, AggreementRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.documentID;
    if (value != null) {
      result
        ..add('document_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.requestableType;
    if (value != null) {
      result
        ..add('requestable_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.requestableID;
    if (value != null) {
      result
        ..add('requestable_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.irn;
    if (value != null) {
      result
        ..add('irn')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  AggreementRequest deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AggreementRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'document_id':
          result.documentID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'requestable_type':
          result.requestableType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'requestable_id':
          result.requestableID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'irn':
          result.irn = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$AggreementRequest extends AggreementRequest {
  @override
  final int? documentID;
  @override
  final String? requestableType;
  @override
  final int? requestableID;
  @override
  final String? status;
  @override
  final String? irn;

  factory _$AggreementRequest(
          [void Function(AggreementRequestBuilder)? updates]) =>
      (new AggreementRequestBuilder()..update(updates))._build();

  _$AggreementRequest._(
      {this.documentID,
      this.requestableType,
      this.requestableID,
      this.status,
      this.irn})
      : super._();

  @override
  AggreementRequest rebuild(void Function(AggreementRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AggreementRequestBuilder toBuilder() =>
      new AggreementRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AggreementRequest &&
        documentID == other.documentID &&
        requestableType == other.requestableType &&
        requestableID == other.requestableID &&
        status == other.status &&
        irn == other.irn;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, documentID.hashCode);
    _$hash = $jc(_$hash, requestableType.hashCode);
    _$hash = $jc(_$hash, requestableID.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, irn.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AggreementRequest')
          ..add('documentID', documentID)
          ..add('requestableType', requestableType)
          ..add('requestableID', requestableID)
          ..add('status', status)
          ..add('irn', irn))
        .toString();
  }
}

class AggreementRequestBuilder
    implements Builder<AggreementRequest, AggreementRequestBuilder> {
  _$AggreementRequest? _$v;

  int? _documentID;
  int? get documentID => _$this._documentID;
  set documentID(int? documentID) => _$this._documentID = documentID;

  String? _requestableType;
  String? get requestableType => _$this._requestableType;
  set requestableType(String? requestableType) =>
      _$this._requestableType = requestableType;

  int? _requestableID;
  int? get requestableID => _$this._requestableID;
  set requestableID(int? requestableID) =>
      _$this._requestableID = requestableID;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  String? _irn;
  String? get irn => _$this._irn;
  set irn(String? irn) => _$this._irn = irn;

  AggreementRequestBuilder();

  AggreementRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _documentID = $v.documentID;
      _requestableType = $v.requestableType;
      _requestableID = $v.requestableID;
      _status = $v.status;
      _irn = $v.irn;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AggreementRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AggreementRequest;
  }

  @override
  void update(void Function(AggreementRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AggreementRequest build() => _build();

  _$AggreementRequest _build() {
    final _$result = _$v ??
        new _$AggreementRequest._(
            documentID: documentID,
            requestableType: requestableType,
            requestableID: requestableID,
            status: status,
            irn: irn);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
