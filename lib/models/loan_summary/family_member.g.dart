// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'family_member.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<FamilyMember> _$familyMemberSerializer =
    new _$FamilyMemberSerializer();

class _$FamilyMemberSerializer implements StructuredSerializer<FamilyMember> {
  @override
  final Iterable<Type> types = const [FamilyMember, _$FamilyMember];
  @override
  final String wireName = 'FamilyMember';

  @override
  Iterable<Object?> serialize(Serializers serializers, FamilyMember object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.dob;
    if (value != null) {
      result
        ..add('dob')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.age;
    if (value != null) {
      result
        ..add('age')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.relationship;
    if (value != null) {
      result
        ..add('relationship')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.occupation;
    if (value != null) {
      result
        ..add('occupation')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.occupationDetails;
    if (value != null) {
      result
        ..add('occupation_details')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isHouseOwner;
    if (value != null) {
      result
        ..add('is_house_owner')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.addressProofNumber;
    if (value != null) {
      result
        ..add('address_proof_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.addressProofID;
    if (value != null) {
      result
        ..add('address_proof_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.addressProofUrl;
    if (value != null) {
      result
        ..add('address_proof_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isCoBorrower;
    if (value != null) {
      result
        ..add('is_co_borrower')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.isNominee;
    if (value != null) {
      result
        ..add('is_nominee')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.location;
    if (value != null) {
      result
        ..add('location')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.mobileNumber;
    if (value != null) {
      result
        ..add('mobile_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.aadhaarNumber;
    if (value != null) {
      result
        ..add('aadhaar_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.panNumber;
    if (value != null) {
      result
        ..add('pan_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.panDocumentID;
    if (value != null) {
      result
        ..add('pan_document_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.panDocumentUrl;
    if (value != null) {
      result
        ..add('pan_document_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.monthlyIncome;
    if (value != null) {
      result
        ..add('monthly_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.doesOwnBussiness;
    if (value != null) {
      result
        ..add('does_own_business')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.bussinessIncome;
    if (value != null) {
      result
        ..add('business_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.crifScore;
    if (value != null) {
      result
        ..add('crif_score')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CrifScore)));
    }
    value = object.aadhaarFrontImageID;
    if (value != null) {
      result
        ..add('aadhaar_front_image_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.aadhaarFrontImageUrl;
    if (value != null) {
      result
        ..add('aadhaar_front_image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.aadhaarBackImageID;
    if (value != null) {
      result
        ..add('aadhaar_back_image_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.aadhaarBackImageUrl;
    if (value != null) {
      result
        ..add('aadhaar_back_image_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.profilePictureID;
    if (value != null) {
      result
        ..add('profile_pic_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.profilePictureUrl;
    if (value != null) {
      result
        ..add('profile_pic_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  FamilyMember deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FamilyMemberBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'dob':
          result.dob = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'age':
          result.age = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'relationship':
          result.relationship = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'occupation':
          result.occupation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'occupation_details':
          result.occupationDetails = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'is_house_owner':
          result.isHouseOwner = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'address_proof_number':
          result.addressProofNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address_proof_id':
          result.addressProofID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'address_proof_url':
          result.addressProofUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'is_co_borrower':
          result.isCoBorrower = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'is_nominee':
          result.isNominee = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'location':
          result.location = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'mobile_number':
          result.mobileNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'aadhaar_number':
          result.aadhaarNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pan_number':
          result.panNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pan_document_id':
          result.panDocumentID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'pan_document_url':
          result.panDocumentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'monthly_income':
          result.monthlyIncome = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'does_own_business':
          result.doesOwnBussiness = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'business_income':
          result.bussinessIncome = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'crif_score':
          result.crifScore.replace(serializers.deserialize(value,
              specifiedType: const FullType(CrifScore))! as CrifScore);
          break;
        case 'aadhaar_front_image_id':
          result.aadhaarFrontImageID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'aadhaar_front_image_url':
          result.aadhaarFrontImageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'aadhaar_back_image_id':
          result.aadhaarBackImageID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'aadhaar_back_image_url':
          result.aadhaarBackImageUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'profile_pic_id':
          result.profilePictureID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'profile_pic_url':
          result.profilePictureUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$FamilyMember extends FamilyMember {
  @override
  final int? id;
  @override
  final String? name;
  @override
  final String? dob;
  @override
  final int? age;
  @override
  final String? relationship;
  @override
  final String? occupation;
  @override
  final String? occupationDetails;
  @override
  final bool? isHouseOwner;
  @override
  final String? addressProofNumber;
  @override
  final int? addressProofID;
  @override
  final String? addressProofUrl;
  @override
  final bool? isCoBorrower;
  @override
  final bool? isNominee;
  @override
  final String? location;
  @override
  final String? mobileNumber;
  @override
  final String? aadhaarNumber;
  @override
  final String? panNumber;
  @override
  final int? panDocumentID;
  @override
  final String? panDocumentUrl;
  @override
  final double? monthlyIncome;
  @override
  final bool? doesOwnBussiness;
  @override
  final double? bussinessIncome;
  @override
  final CrifScore? crifScore;
  @override
  final int? aadhaarFrontImageID;
  @override
  final String? aadhaarFrontImageUrl;
  @override
  final int? aadhaarBackImageID;
  @override
  final String? aadhaarBackImageUrl;
  @override
  final int? profilePictureID;
  @override
  final String? profilePictureUrl;

  factory _$FamilyMember([void Function(FamilyMemberBuilder)? updates]) =>
      (new FamilyMemberBuilder()..update(updates))._build();

  _$FamilyMember._(
      {this.id,
      this.name,
      this.dob,
      this.age,
      this.relationship,
      this.occupation,
      this.occupationDetails,
      this.isHouseOwner,
      this.addressProofNumber,
      this.addressProofID,
      this.addressProofUrl,
      this.isCoBorrower,
      this.isNominee,
      this.location,
      this.mobileNumber,
      this.aadhaarNumber,
      this.panNumber,
      this.panDocumentID,
      this.panDocumentUrl,
      this.monthlyIncome,
      this.doesOwnBussiness,
      this.bussinessIncome,
      this.crifScore,
      this.aadhaarFrontImageID,
      this.aadhaarFrontImageUrl,
      this.aadhaarBackImageID,
      this.aadhaarBackImageUrl,
      this.profilePictureID,
      this.profilePictureUrl})
      : super._();

  @override
  FamilyMember rebuild(void Function(FamilyMemberBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FamilyMemberBuilder toBuilder() => new FamilyMemberBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FamilyMember &&
        id == other.id &&
        name == other.name &&
        dob == other.dob &&
        age == other.age &&
        relationship == other.relationship &&
        occupation == other.occupation &&
        occupationDetails == other.occupationDetails &&
        isHouseOwner == other.isHouseOwner &&
        addressProofNumber == other.addressProofNumber &&
        addressProofID == other.addressProofID &&
        addressProofUrl == other.addressProofUrl &&
        isCoBorrower == other.isCoBorrower &&
        isNominee == other.isNominee &&
        location == other.location &&
        mobileNumber == other.mobileNumber &&
        aadhaarNumber == other.aadhaarNumber &&
        panNumber == other.panNumber &&
        panDocumentID == other.panDocumentID &&
        panDocumentUrl == other.panDocumentUrl &&
        monthlyIncome == other.monthlyIncome &&
        doesOwnBussiness == other.doesOwnBussiness &&
        bussinessIncome == other.bussinessIncome &&
        crifScore == other.crifScore &&
        aadhaarFrontImageID == other.aadhaarFrontImageID &&
        aadhaarFrontImageUrl == other.aadhaarFrontImageUrl &&
        aadhaarBackImageID == other.aadhaarBackImageID &&
        aadhaarBackImageUrl == other.aadhaarBackImageUrl &&
        profilePictureID == other.profilePictureID &&
        profilePictureUrl == other.profilePictureUrl;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, dob.hashCode);
    _$hash = $jc(_$hash, age.hashCode);
    _$hash = $jc(_$hash, relationship.hashCode);
    _$hash = $jc(_$hash, occupation.hashCode);
    _$hash = $jc(_$hash, occupationDetails.hashCode);
    _$hash = $jc(_$hash, isHouseOwner.hashCode);
    _$hash = $jc(_$hash, addressProofNumber.hashCode);
    _$hash = $jc(_$hash, addressProofID.hashCode);
    _$hash = $jc(_$hash, addressProofUrl.hashCode);
    _$hash = $jc(_$hash, isCoBorrower.hashCode);
    _$hash = $jc(_$hash, isNominee.hashCode);
    _$hash = $jc(_$hash, location.hashCode);
    _$hash = $jc(_$hash, mobileNumber.hashCode);
    _$hash = $jc(_$hash, aadhaarNumber.hashCode);
    _$hash = $jc(_$hash, panNumber.hashCode);
    _$hash = $jc(_$hash, panDocumentID.hashCode);
    _$hash = $jc(_$hash, panDocumentUrl.hashCode);
    _$hash = $jc(_$hash, monthlyIncome.hashCode);
    _$hash = $jc(_$hash, doesOwnBussiness.hashCode);
    _$hash = $jc(_$hash, bussinessIncome.hashCode);
    _$hash = $jc(_$hash, crifScore.hashCode);
    _$hash = $jc(_$hash, aadhaarFrontImageID.hashCode);
    _$hash = $jc(_$hash, aadhaarFrontImageUrl.hashCode);
    _$hash = $jc(_$hash, aadhaarBackImageID.hashCode);
    _$hash = $jc(_$hash, aadhaarBackImageUrl.hashCode);
    _$hash = $jc(_$hash, profilePictureID.hashCode);
    _$hash = $jc(_$hash, profilePictureUrl.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FamilyMember')
          ..add('id', id)
          ..add('name', name)
          ..add('dob', dob)
          ..add('age', age)
          ..add('relationship', relationship)
          ..add('occupation', occupation)
          ..add('occupationDetails', occupationDetails)
          ..add('isHouseOwner', isHouseOwner)
          ..add('addressProofNumber', addressProofNumber)
          ..add('addressProofID', addressProofID)
          ..add('addressProofUrl', addressProofUrl)
          ..add('isCoBorrower', isCoBorrower)
          ..add('isNominee', isNominee)
          ..add('location', location)
          ..add('mobileNumber', mobileNumber)
          ..add('aadhaarNumber', aadhaarNumber)
          ..add('panNumber', panNumber)
          ..add('panDocumentID', panDocumentID)
          ..add('panDocumentUrl', panDocumentUrl)
          ..add('monthlyIncome', monthlyIncome)
          ..add('doesOwnBussiness', doesOwnBussiness)
          ..add('bussinessIncome', bussinessIncome)
          ..add('crifScore', crifScore)
          ..add('aadhaarFrontImageID', aadhaarFrontImageID)
          ..add('aadhaarFrontImageUrl', aadhaarFrontImageUrl)
          ..add('aadhaarBackImageID', aadhaarBackImageID)
          ..add('aadhaarBackImageUrl', aadhaarBackImageUrl)
          ..add('profilePictureID', profilePictureID)
          ..add('profilePictureUrl', profilePictureUrl))
        .toString();
  }
}

class FamilyMemberBuilder
    implements Builder<FamilyMember, FamilyMemberBuilder> {
  _$FamilyMember? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _dob;
  String? get dob => _$this._dob;
  set dob(String? dob) => _$this._dob = dob;

  int? _age;
  int? get age => _$this._age;
  set age(int? age) => _$this._age = age;

  String? _relationship;
  String? get relationship => _$this._relationship;
  set relationship(String? relationship) => _$this._relationship = relationship;

  String? _occupation;
  String? get occupation => _$this._occupation;
  set occupation(String? occupation) => _$this._occupation = occupation;

  String? _occupationDetails;
  String? get occupationDetails => _$this._occupationDetails;
  set occupationDetails(String? occupationDetails) =>
      _$this._occupationDetails = occupationDetails;

  bool? _isHouseOwner;
  bool? get isHouseOwner => _$this._isHouseOwner;
  set isHouseOwner(bool? isHouseOwner) => _$this._isHouseOwner = isHouseOwner;

  String? _addressProofNumber;
  String? get addressProofNumber => _$this._addressProofNumber;
  set addressProofNumber(String? addressProofNumber) =>
      _$this._addressProofNumber = addressProofNumber;

  int? _addressProofID;
  int? get addressProofID => _$this._addressProofID;
  set addressProofID(int? addressProofID) =>
      _$this._addressProofID = addressProofID;

  String? _addressProofUrl;
  String? get addressProofUrl => _$this._addressProofUrl;
  set addressProofUrl(String? addressProofUrl) =>
      _$this._addressProofUrl = addressProofUrl;

  bool? _isCoBorrower;
  bool? get isCoBorrower => _$this._isCoBorrower;
  set isCoBorrower(bool? isCoBorrower) => _$this._isCoBorrower = isCoBorrower;

  bool? _isNominee;
  bool? get isNominee => _$this._isNominee;
  set isNominee(bool? isNominee) => _$this._isNominee = isNominee;

  String? _location;
  String? get location => _$this._location;
  set location(String? location) => _$this._location = location;

  String? _mobileNumber;
  String? get mobileNumber => _$this._mobileNumber;
  set mobileNumber(String? mobileNumber) => _$this._mobileNumber = mobileNumber;

  String? _aadhaarNumber;
  String? get aadhaarNumber => _$this._aadhaarNumber;
  set aadhaarNumber(String? aadhaarNumber) =>
      _$this._aadhaarNumber = aadhaarNumber;

  String? _panNumber;
  String? get panNumber => _$this._panNumber;
  set panNumber(String? panNumber) => _$this._panNumber = panNumber;

  int? _panDocumentID;
  int? get panDocumentID => _$this._panDocumentID;
  set panDocumentID(int? panDocumentID) =>
      _$this._panDocumentID = panDocumentID;

  String? _panDocumentUrl;
  String? get panDocumentUrl => _$this._panDocumentUrl;
  set panDocumentUrl(String? panDocumentUrl) =>
      _$this._panDocumentUrl = panDocumentUrl;

  double? _monthlyIncome;
  double? get monthlyIncome => _$this._monthlyIncome;
  set monthlyIncome(double? monthlyIncome) =>
      _$this._monthlyIncome = monthlyIncome;

  bool? _doesOwnBussiness;
  bool? get doesOwnBussiness => _$this._doesOwnBussiness;
  set doesOwnBussiness(bool? doesOwnBussiness) =>
      _$this._doesOwnBussiness = doesOwnBussiness;

  double? _bussinessIncome;
  double? get bussinessIncome => _$this._bussinessIncome;
  set bussinessIncome(double? bussinessIncome) =>
      _$this._bussinessIncome = bussinessIncome;

  CrifScoreBuilder? _crifScore;
  CrifScoreBuilder get crifScore =>
      _$this._crifScore ??= new CrifScoreBuilder();
  set crifScore(CrifScoreBuilder? crifScore) => _$this._crifScore = crifScore;

  int? _aadhaarFrontImageID;
  int? get aadhaarFrontImageID => _$this._aadhaarFrontImageID;
  set aadhaarFrontImageID(int? aadhaarFrontImageID) =>
      _$this._aadhaarFrontImageID = aadhaarFrontImageID;

  String? _aadhaarFrontImageUrl;
  String? get aadhaarFrontImageUrl => _$this._aadhaarFrontImageUrl;
  set aadhaarFrontImageUrl(String? aadhaarFrontImageUrl) =>
      _$this._aadhaarFrontImageUrl = aadhaarFrontImageUrl;

  int? _aadhaarBackImageID;
  int? get aadhaarBackImageID => _$this._aadhaarBackImageID;
  set aadhaarBackImageID(int? aadhaarBackImageID) =>
      _$this._aadhaarBackImageID = aadhaarBackImageID;

  String? _aadhaarBackImageUrl;
  String? get aadhaarBackImageUrl => _$this._aadhaarBackImageUrl;
  set aadhaarBackImageUrl(String? aadhaarBackImageUrl) =>
      _$this._aadhaarBackImageUrl = aadhaarBackImageUrl;

  int? _profilePictureID;
  int? get profilePictureID => _$this._profilePictureID;
  set profilePictureID(int? profilePictureID) =>
      _$this._profilePictureID = profilePictureID;

  String? _profilePictureUrl;
  String? get profilePictureUrl => _$this._profilePictureUrl;
  set profilePictureUrl(String? profilePictureUrl) =>
      _$this._profilePictureUrl = profilePictureUrl;

  FamilyMemberBuilder();

  FamilyMemberBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _dob = $v.dob;
      _age = $v.age;
      _relationship = $v.relationship;
      _occupation = $v.occupation;
      _occupationDetails = $v.occupationDetails;
      _isHouseOwner = $v.isHouseOwner;
      _addressProofNumber = $v.addressProofNumber;
      _addressProofID = $v.addressProofID;
      _addressProofUrl = $v.addressProofUrl;
      _isCoBorrower = $v.isCoBorrower;
      _isNominee = $v.isNominee;
      _location = $v.location;
      _mobileNumber = $v.mobileNumber;
      _aadhaarNumber = $v.aadhaarNumber;
      _panNumber = $v.panNumber;
      _panDocumentID = $v.panDocumentID;
      _panDocumentUrl = $v.panDocumentUrl;
      _monthlyIncome = $v.monthlyIncome;
      _doesOwnBussiness = $v.doesOwnBussiness;
      _bussinessIncome = $v.bussinessIncome;
      _crifScore = $v.crifScore?.toBuilder();
      _aadhaarFrontImageID = $v.aadhaarFrontImageID;
      _aadhaarFrontImageUrl = $v.aadhaarFrontImageUrl;
      _aadhaarBackImageID = $v.aadhaarBackImageID;
      _aadhaarBackImageUrl = $v.aadhaarBackImageUrl;
      _profilePictureID = $v.profilePictureID;
      _profilePictureUrl = $v.profilePictureUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FamilyMember other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FamilyMember;
  }

  @override
  void update(void Function(FamilyMemberBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FamilyMember build() => _build();

  _$FamilyMember _build() {
    _$FamilyMember _$result;
    try {
      _$result = _$v ??
          new _$FamilyMember._(
              id: id,
              name: name,
              dob: dob,
              age: age,
              relationship: relationship,
              occupation: occupation,
              occupationDetails: occupationDetails,
              isHouseOwner: isHouseOwner,
              addressProofNumber: addressProofNumber,
              addressProofID: addressProofID,
              addressProofUrl: addressProofUrl,
              isCoBorrower: isCoBorrower,
              isNominee: isNominee,
              location: location,
              mobileNumber: mobileNumber,
              aadhaarNumber: aadhaarNumber,
              panNumber: panNumber,
              panDocumentID: panDocumentID,
              panDocumentUrl: panDocumentUrl,
              monthlyIncome: monthlyIncome,
              doesOwnBussiness: doesOwnBussiness,
              bussinessIncome: bussinessIncome,
              crifScore: _crifScore?.build(),
              aadhaarFrontImageID: aadhaarFrontImageID,
              aadhaarFrontImageUrl: aadhaarFrontImageUrl,
              aadhaarBackImageID: aadhaarBackImageID,
              aadhaarBackImageUrl: aadhaarBackImageUrl,
              profilePictureID: profilePictureID,
              profilePictureUrl: profilePictureUrl);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'crifScore';
        _crifScore?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'FamilyMember', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
