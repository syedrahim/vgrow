import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'loan_comment.g.dart';

abstract class LoanComment implements Built<LoanComment, LoanCommentBuilder>{
  LoanComment._();
  factory LoanComment([LoanCommentBuilder updates(LoanCommentBuilder builder)]) = _$LoanComment;

  int? get id;

  String? get description;

  String? get step;

  @BuiltValueField(wireName: 'loan_id')
  int? get loanID;

  @BuiltValueField(wireName: 'creator_type')
  String? get creatorType;

  @BuiltValueField(wireName: 'creator_id')
  int? get creatorID;

  @BuiltValueField(wireName: 'creator_firstname')
  String? get creatorFirstname;

  @BuiltValueField(wireName: 'creator_lastname')
  String? get creatorLastname;

  @BuiltValueField(wireName: 'creator_profile_pic_url')
  String? get cratorProfilePictureUrl;

  @BuiltValueField(wireName: 'created_at')
  String? get createdAt;

  @BuiltValueField(wireName: 'creator_branch')
  String? get creatorBranch;

 String? get attachment;

  static Serializer<LoanComment> get serializer => _$loanCommentSerializer;
}