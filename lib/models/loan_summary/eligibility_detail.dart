import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'eligibility_detail.g.dart';

abstract class EligibilityDetail implements Built<EligibilityDetail, EligibilityDetailBuilder>{
  EligibilityDetail._();
  factory EligibilityDetail([EligibilityDetailBuilder updates(EligibilityDetailBuilder builder)]) = _$EligibilityDetail;

  int? get id;

  @BuiltValueField(wireName: 'loan_id')
  int? get loanID;

  @BuiltValueField(wireName: 'housing_ownership_status')
  String? get housingOwnershipStatus;

  @BuiltValueField(wireName: 'span_of_residence')
  String? get spanOfResidence;

  @BuiltValueField(wireName: 'area_of_residence')
  int? get areaOfResidence;

  @BuiltValueField(wireName: 'relationship_to_residence_owner')
  String? get relationshipTOResidenceOwner;

  @BuiltValueField(wireName: 'type_of_residence')
  String? get typeOfResidence;

  @BuiltValueField(wireName: 'earning_members_count')
  int? get earningMembersCount;

  @BuiltValueField(wireName: 'business_building_ownership_status')
  String? get businessBuildingOwnershipStatus;

  @BuiltValueField(wireName: 'years_of_business')
  int? get yearsOfBusiness;

  @BuiltValueField(wireName: 'months_of_business')
  int? get monthsOfBusiness;

  @BuiltValueField(wireName:'type_of_business_model')
  String? get typeOfBusinessModel;

  static Serializer<EligibilityDetail> get serializer => _$eligibilityDetailSerializer;
}