import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'created_by.g.dart';

abstract class CreatedBy implements Built<CreatedBy, CreatedByBuilder> {
  CreatedBy._();
  factory CreatedBy([updates(CreatedByBuilder b)]) = _$CreatedBy;

  static Serializer<CreatedBy> get serializer => _$createdBySerializer;

  int? get id;
  String? get email;
  @BuiltValueField(wireName: 'created_at')
  String? get createdAt;
  @BuiltValueField(wireName: 'updated_at')
  String? get updatedAt;
  @BuiltValueField(wireName: 'firstname')
  String? get firstname;
  @BuiltValueField(wireName: 'lastname')
  String? get lastname;
  String? get code;
  String? get gender;
  @BuiltValueField(wireName: 'isd_code')
  String? get isdCode;
  @BuiltValueField(wireName: 'phone_number')
  String? get phoneNumber;
  @BuiltValueField(wireName: 'is_active')
  bool? get isActive;
  @BuiltValueField(wireName: 'role_id')
  int? get roleId;

}
