import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'created_by.dart';
import 'loan_customer.dart';
import 'additional_details.dart';
import 'eligibility_detail.dart';

part 'loan_summary_model.g.dart';

abstract class LoanSummary implements Built<LoanSummary, LoanSummaryBuilder> {
  LoanSummary._();
  factory LoanSummary([updates(LoanSummaryBuilder b)]) = _$LoanSummary;

  static Serializer<LoanSummary> get serializer => _$loanSummarySerializer;

  @BuiltValueField(wireName: 'additional_details_json')
  AdditionalDetailsJson? get additionalDetailsJson;
  @BuiltValueField(wireName: 'outstanding_amount')
  int? get outstandingAmount;
  @BuiltValueField(wireName: 'number_of_pending_dues')
  int? get numberOfPendingDues;
  @BuiltValueField(wireName: 'applied_date')
  String? get appliedDate;
  @BuiltValueField(wireName: 'preferred_due_date')
  Null? get preferredDueDate;
  int? get id;
  String? get code;
  @BuiltValueField(wireName: 'center_id')
  int? get centerId;
  @BuiltValueField(wireName: 'center_name')
  String? get centerName;
  @BuiltValueField(wireName: 'created_by')
  CreatedBy? get createdBy;
  @BuiltValueField(wireName: 'created_by_id')
  int? get createdById;
  @BuiltValueField(wireName: 'loan_type_id')
  int? get loanTypeId;
  @BuiltValueField(wireName: 'loan_type_name')
  String? get loanTypeName;
  @BuiltValueField(wireName: 'loan_type_code')
  String? get loanTypeCode;
  @BuiltValueField(wireName: 'estimated_cost')
  double? get estimatedCost;
  @BuiltValueField(wireName: 'requested_loan_amt')
  double? get requestedLoanAmt;

  @BuiltValueField(wireName: 'status')
  String? get status;
  @BuiltValueField(wireName: 'loan_purpose_id')
  int? get loanPurposeId;
  @BuiltValueField(wireName: 'loan_purpose_name')
  String? get loanPurposeName;
  @BuiltValueField(wireName: 'organisation_id')
  int? get organisationId;
  @BuiltValueField(wireName: 'organisation_name')
  String? get organisationName;
  @BuiltValueField(wireName: 'cro_picture_id')
  int? get croPictureId;
  @BuiltValueField(wireName: 'cro_picture_url')
  String? get croPictureUrl;
  @BuiltValueField(wireName: 'cro_name')
  String? get croName;
  @BuiltValueField(wireName: 'cro_captured_time')
  String? get croCapturedTime;
  @BuiltValueField(wireName: 'current_step')
  String? get currentStep;
  @BuiltValueField(wireName: 'tenure_in_months')
  int? get tenureInMonths;
  @BuiltValueField(wireName: 'interest_rate')
  double? get interestRate;
  @BuiltValueField(wireName: 'expected_start_date')
  String? get expectedStartDate;
  @BuiltValueField(wireName: 'bm_picture_id')
  int? get bmPictureId;
  @BuiltValueField(wireName: 'bm_picture_url')
  String? get bmPictureUrl;
  @BuiltValueField(wireName: 'bm_name')
  String? get bmName;
  @BuiltValueField(wireName: 'bm_captured_time')
  String? get bmCapturedTime;
  @BuiltValueField(wireName: 'ia_picture_id')
  int? get iaPictureId;
  @BuiltValueField(wireName: 'ia_picture_url')
  String? get iaPictureUrl;
  @BuiltValueField(wireName: 'ia_name')
  String? get iaName;
  @BuiltValueField(wireName: 'ia_captured_time')
  String? get iaCapturedTime;

  @BuiltValueField(wireName: 'customer')
  LoanCustomer? get customer;

  @BuiltValueField(wireName: 'eligibility_detail')
  EligibilityDetail? get eligibilityDetail;

}
