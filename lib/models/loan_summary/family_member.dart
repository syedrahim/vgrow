import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'crif_score.dart';

part 'family_member.g.dart';

abstract class FamilyMember implements Built<FamilyMember, FamilyMemberBuilder>{
  FamilyMember._();
  factory FamilyMember([FamilyMemberBuilder updates(FamilyMemberBuilder builder)]) = _$FamilyMember;

  int? get id;

  String? get name;

  String? get dob;

  int? get age;

  String? get relationship;

  String? get occupation;

  @BuiltValueField(wireName: 'occupation_details')
  String? get occupationDetails;

  @BuiltValueField(wireName: 'is_house_owner')
  bool? get isHouseOwner;

  @BuiltValueField(wireName: 'address_proof_number')
  String? get addressProofNumber;

  @BuiltValueField(wireName: 'address_proof_id')
  int? get addressProofID;

  @BuiltValueField(wireName: 'address_proof_url')
  String? get addressProofUrl;

  @BuiltValueField(wireName: 'is_co_borrower')
  bool? get isCoBorrower;

  @BuiltValueField(wireName: 'is_nominee')
  bool? get isNominee;

  String? get location;

  @BuiltValueField(wireName: 'mobile_number')
  String? get mobileNumber;

  @BuiltValueField(wireName: 'aadhaar_number')
  String? get aadhaarNumber;

  @BuiltValueField(wireName: 'pan_number')
  String? get panNumber;

  @BuiltValueField(wireName: 'pan_document_id')
  int? get panDocumentID;

  @BuiltValueField(wireName: 'pan_document_url')
  String? get panDocumentUrl;

  @BuiltValueField(wireName: 'monthly_income')
  double? get monthlyIncome;

  @BuiltValueField(wireName: 'does_own_business')
  bool? get doesOwnBussiness;

  @BuiltValueField(wireName: 'business_income')
  double? get bussinessIncome;

  @BuiltValueField(wireName: 'crif_score')
  CrifScore? get crifScore;

  @BuiltValueField(wireName: 'aadhaar_front_image_id')
  int? get aadhaarFrontImageID;

  @BuiltValueField(wireName:'aadhaar_front_image_url')
  String? get aadhaarFrontImageUrl;

  @BuiltValueField(wireName: 'aadhaar_back_image_id')
  int? get aadhaarBackImageID;

  @BuiltValueField(wireName: 'aadhaar_back_image_url')
  String? get aadhaarBackImageUrl;

  @BuiltValueField(wireName: 'profile_pic_id')
  int? get profilePictureID;

  @BuiltValueField(wireName: 'profile_pic_url')
  String? get profilePictureUrl;


  static Serializer<FamilyMember> get serializer => _$familyMemberSerializer;
}