import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'crif_score.g.dart';

abstract class CrifScore implements Built<CrifScore, CrifScoreBuilder>{
  CrifScore._();
  factory CrifScore([CrifScoreBuilder updates(CrifScoreBuilder builder)]) = _$CrifScore;

  double? get score;

  String? get status;

  @BuiltValueField(wireName: 'created_at')
  String? get createdAt;

  String? get comment;

  static Serializer<CrifScore> get serializer => _$crifScoreSerializer;
}