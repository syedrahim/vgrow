// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_address.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanAddress> _$loanAddressSerializer = new _$LoanAddressSerializer();
Serializer<LoanAddressAttachment> _$loanAddressAttachmentSerializer =
    new _$LoanAddressAttachmentSerializer();

class _$LoanAddressSerializer implements StructuredSerializer<LoanAddress> {
  @override
  final Iterable<Type> types = const [LoanAddress, _$LoanAddress];
  @override
  final String wireName = 'LoanAddress';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanAddress object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[
      'address_attachments',
      serializers.serialize(object.address_attachments,
          specifiedType: const FullType(
              BuiltList, const [const FullType(LoanAddressAttachment)])),
    ];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.addressableType;
    if (value != null) {
      result
        ..add('addressable_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.addressableID;
    if (value != null) {
      result
        ..add('addressable_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.addressLine1;
    if (value != null) {
      result
        ..add('address_line_1')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.addressLine2;
    if (value != null) {
      result
        ..add('address_line_2')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.cityID;
    if (value != null) {
      result
        ..add('city_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.cityName;
    if (value != null) {
      result
        ..add('city_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.stateID;
    if (value != null) {
      result
        ..add('state_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.stateName;
    if (value != null) {
      result
        ..add('state_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.pincode;
    if (value != null) {
      result
        ..add('pincode')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.landmark;
    if (value != null) {
      result
        ..add('landmark')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.latitude;
    if (value != null) {
      result
        ..add('latitude')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.longitude;
    if (value != null) {
      result
        ..add('longitude')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  LoanAddress deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanAddressBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'addressable_type':
          result.addressableType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'addressable_id':
          result.addressableID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'address_line_1':
          result.addressLine1 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address_line_2':
          result.addressLine2 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'city_id':
          result.cityID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'city_name':
          result.cityName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'state_id':
          result.stateID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'state_name':
          result.stateName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pincode':
          result.pincode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'landmark':
          result.landmark = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'latitude':
          result.latitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'longitude':
          result.longitude = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'address_attachments':
          result.address_attachments.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltList, const [
                const FullType(LoanAddressAttachment)
              ]))! as BuiltList<Object?>);
          break;
      }
    }

    return result.build();
  }
}

class _$LoanAddressAttachmentSerializer
    implements StructuredSerializer<LoanAddressAttachment> {
  @override
  final Iterable<Type> types = const [
    LoanAddressAttachment,
    _$LoanAddressAttachment
  ];
  @override
  final String wireName = 'LoanAddressAttachment';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, LoanAddressAttachment object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.attachmentId;
    if (value != null) {
      result
        ..add('attachmentId')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.attachmentUrl;
    if (value != null) {
      result
        ..add('attachmentUrl')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.employeeRoleCode;
    if (value != null) {
      result
        ..add('employeeRoleCode')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.employeeId;
    if (value != null) {
      result
        ..add('employeeId')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  LoanAddressAttachment deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanAddressAttachmentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'attachmentId':
          result.attachmentId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'attachmentUrl':
          result.attachmentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'employeeRoleCode':
          result.employeeRoleCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'employeeId':
          result.employeeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$LoanAddress extends LoanAddress {
  @override
  final int? id;
  @override
  final String? addressableType;
  @override
  final int? addressableID;
  @override
  final String? addressLine1;
  @override
  final String? addressLine2;
  @override
  final int? cityID;
  @override
  final String? cityName;
  @override
  final int? stateID;
  @override
  final String? stateName;
  @override
  final String? pincode;
  @override
  final String? landmark;
  @override
  final double? latitude;
  @override
  final double? longitude;
  @override
  final BuiltList<LoanAddressAttachment> address_attachments;

  factory _$LoanAddress([void Function(LoanAddressBuilder)? updates]) =>
      (new LoanAddressBuilder()..update(updates))._build();

  _$LoanAddress._(
      {this.id,
      this.addressableType,
      this.addressableID,
      this.addressLine1,
      this.addressLine2,
      this.cityID,
      this.cityName,
      this.stateID,
      this.stateName,
      this.pincode,
      this.landmark,
      this.latitude,
      this.longitude,
      required this.address_attachments})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        address_attachments, r'LoanAddress', 'address_attachments');
  }

  @override
  LoanAddress rebuild(void Function(LoanAddressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanAddressBuilder toBuilder() => new LoanAddressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanAddress &&
        id == other.id &&
        addressableType == other.addressableType &&
        addressableID == other.addressableID &&
        addressLine1 == other.addressLine1 &&
        addressLine2 == other.addressLine2 &&
        cityID == other.cityID &&
        cityName == other.cityName &&
        stateID == other.stateID &&
        stateName == other.stateName &&
        pincode == other.pincode &&
        landmark == other.landmark &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        address_attachments == other.address_attachments;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, addressableType.hashCode);
    _$hash = $jc(_$hash, addressableID.hashCode);
    _$hash = $jc(_$hash, addressLine1.hashCode);
    _$hash = $jc(_$hash, addressLine2.hashCode);
    _$hash = $jc(_$hash, cityID.hashCode);
    _$hash = $jc(_$hash, cityName.hashCode);
    _$hash = $jc(_$hash, stateID.hashCode);
    _$hash = $jc(_$hash, stateName.hashCode);
    _$hash = $jc(_$hash, pincode.hashCode);
    _$hash = $jc(_$hash, landmark.hashCode);
    _$hash = $jc(_$hash, latitude.hashCode);
    _$hash = $jc(_$hash, longitude.hashCode);
    _$hash = $jc(_$hash, address_attachments.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }
}

class LoanAddressBuilder implements Builder<LoanAddress, LoanAddressBuilder> {
  _$LoanAddress? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _addressableType;
  String? get addressableType => _$this._addressableType;
  set addressableType(String? addressableType) =>
      _$this._addressableType = addressableType;

  int? _addressableID;
  int? get addressableID => _$this._addressableID;
  set addressableID(int? addressableID) =>
      _$this._addressableID = addressableID;

  String? _addressLine1;
  String? get addressLine1 => _$this._addressLine1;
  set addressLine1(String? addressLine1) => _$this._addressLine1 = addressLine1;

  String? _addressLine2;
  String? get addressLine2 => _$this._addressLine2;
  set addressLine2(String? addressLine2) => _$this._addressLine2 = addressLine2;

  int? _cityID;
  int? get cityID => _$this._cityID;
  set cityID(int? cityID) => _$this._cityID = cityID;

  String? _cityName;
  String? get cityName => _$this._cityName;
  set cityName(String? cityName) => _$this._cityName = cityName;

  int? _stateID;
  int? get stateID => _$this._stateID;
  set stateID(int? stateID) => _$this._stateID = stateID;

  String? _stateName;
  String? get stateName => _$this._stateName;
  set stateName(String? stateName) => _$this._stateName = stateName;

  String? _pincode;
  String? get pincode => _$this._pincode;
  set pincode(String? pincode) => _$this._pincode = pincode;

  String? _landmark;
  String? get landmark => _$this._landmark;
  set landmark(String? landmark) => _$this._landmark = landmark;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  ListBuilder<LoanAddressAttachment>? _address_attachments;
  ListBuilder<LoanAddressAttachment> get address_attachments =>
      _$this._address_attachments ??= new ListBuilder<LoanAddressAttachment>();
  set address_attachments(
          ListBuilder<LoanAddressAttachment>? address_attachments) =>
      _$this._address_attachments = address_attachments;

  LoanAddressBuilder();

  LoanAddressBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _addressableType = $v.addressableType;
      _addressableID = $v.addressableID;
      _addressLine1 = $v.addressLine1;
      _addressLine2 = $v.addressLine2;
      _cityID = $v.cityID;
      _cityName = $v.cityName;
      _stateID = $v.stateID;
      _stateName = $v.stateName;
      _pincode = $v.pincode;
      _landmark = $v.landmark;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _address_attachments = $v.address_attachments.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanAddress other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanAddress;
  }

  @override
  void update(void Function(LoanAddressBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanAddress build() => _build();

  _$LoanAddress _build() {
    _$LoanAddress _$result;
    try {
      _$result = _$v ??
          new _$LoanAddress._(
              id: id,
              addressableType: addressableType,
              addressableID: addressableID,
              addressLine1: addressLine1,
              addressLine2: addressLine2,
              cityID: cityID,
              cityName: cityName,
              stateID: stateID,
              stateName: stateName,
              pincode: pincode,
              landmark: landmark,
              latitude: latitude,
              longitude: longitude,
              address_attachments: address_attachments.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'address_attachments';
        address_attachments.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'LoanAddress', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$LoanAddressAttachment extends LoanAddressAttachment {
  @override
  final int? id;
  @override
  final int? attachmentId;
  @override
  final String? attachmentUrl;
  @override
  final String? employeeRoleCode;
  @override
  final int? employeeId;

  factory _$LoanAddressAttachment(
          [void Function(LoanAddressAttachmentBuilder)? updates]) =>
      (new LoanAddressAttachmentBuilder()..update(updates))._build();

  _$LoanAddressAttachment._(
      {this.id,
      this.attachmentId,
      this.attachmentUrl,
      this.employeeRoleCode,
      this.employeeId})
      : super._();

  @override
  LoanAddressAttachment rebuild(
          void Function(LoanAddressAttachmentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanAddressAttachmentBuilder toBuilder() =>
      new LoanAddressAttachmentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanAddressAttachment &&
        id == other.id &&
        attachmentId == other.attachmentId &&
        attachmentUrl == other.attachmentUrl &&
        employeeRoleCode == other.employeeRoleCode &&
        employeeId == other.employeeId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, attachmentId.hashCode);
    _$hash = $jc(_$hash, attachmentUrl.hashCode);
    _$hash = $jc(_$hash, employeeRoleCode.hashCode);
    _$hash = $jc(_$hash, employeeId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanAddressAttachment')
          ..add('id', id)
          ..add('attachmentId', attachmentId)
          ..add('attachmentUrl', attachmentUrl)
          ..add('employeeRoleCode', employeeRoleCode)
          ..add('employeeId', employeeId))
        .toString();
  }
}

class LoanAddressAttachmentBuilder
    implements Builder<LoanAddressAttachment, LoanAddressAttachmentBuilder> {
  _$LoanAddressAttachment? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _attachmentId;
  int? get attachmentId => _$this._attachmentId;
  set attachmentId(int? attachmentId) => _$this._attachmentId = attachmentId;

  String? _attachmentUrl;
  String? get attachmentUrl => _$this._attachmentUrl;
  set attachmentUrl(String? attachmentUrl) =>
      _$this._attachmentUrl = attachmentUrl;

  String? _employeeRoleCode;
  String? get employeeRoleCode => _$this._employeeRoleCode;
  set employeeRoleCode(String? employeeRoleCode) =>
      _$this._employeeRoleCode = employeeRoleCode;

  int? _employeeId;
  int? get employeeId => _$this._employeeId;
  set employeeId(int? employeeId) => _$this._employeeId = employeeId;

  LoanAddressAttachmentBuilder();

  LoanAddressAttachmentBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _attachmentId = $v.attachmentId;
      _attachmentUrl = $v.attachmentUrl;
      _employeeRoleCode = $v.employeeRoleCode;
      _employeeId = $v.employeeId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanAddressAttachment other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanAddressAttachment;
  }

  @override
  void update(void Function(LoanAddressAttachmentBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanAddressAttachment build() => _build();

  _$LoanAddressAttachment _build() {
    final _$result = _$v ??
        new _$LoanAddressAttachment._(
            id: id,
            attachmentId: attachmentId,
            attachmentUrl: attachmentUrl,
            employeeRoleCode: employeeRoleCode,
            employeeId: employeeId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
