// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'created_by.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<CreatedBy> _$createdBySerializer = new _$CreatedBySerializer();

class _$CreatedBySerializer implements StructuredSerializer<CreatedBy> {
  @override
  final Iterable<Type> types = const [CreatedBy, _$CreatedBy];
  @override
  final String wireName = 'CreatedBy';

  @override
  Iterable<Object?> serialize(Serializers serializers, CreatedBy object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.email;
    if (value != null) {
      result
        ..add('email')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdAt;
    if (value != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.updatedAt;
    if (value != null) {
      result
        ..add('updated_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.firstname;
    if (value != null) {
      result
        ..add('firstname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastname;
    if (value != null) {
      result
        ..add('lastname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.code;
    if (value != null) {
      result
        ..add('code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.gender;
    if (value != null) {
      result
        ..add('gender')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isdCode;
    if (value != null) {
      result
        ..add('isd_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.phoneNumber;
    if (value != null) {
      result
        ..add('phone_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isActive;
    if (value != null) {
      result
        ..add('is_active')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.roleId;
    if (value != null) {
      result
        ..add('role_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  CreatedBy deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CreatedByBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'updated_at':
          result.updatedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'firstname':
          result.firstname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'lastname':
          result.lastname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'isd_code':
          result.isdCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'phone_number':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'is_active':
          result.isActive = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'role_id':
          result.roleId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$CreatedBy extends CreatedBy {
  @override
  final int? id;
  @override
  final String? email;
  @override
  final String? createdAt;
  @override
  final String? updatedAt;
  @override
  final String? firstname;
  @override
  final String? lastname;
  @override
  final String? code;
  @override
  final String? gender;
  @override
  final String? isdCode;
  @override
  final String? phoneNumber;
  @override
  final bool? isActive;
  @override
  final int? roleId;

  factory _$CreatedBy([void Function(CreatedByBuilder)? updates]) =>
      (new CreatedByBuilder()..update(updates))._build();

  _$CreatedBy._(
      {this.id,
      this.email,
      this.createdAt,
      this.updatedAt,
      this.firstname,
      this.lastname,
      this.code,
      this.gender,
      this.isdCode,
      this.phoneNumber,
      this.isActive,
      this.roleId})
      : super._();

  @override
  CreatedBy rebuild(void Function(CreatedByBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreatedByBuilder toBuilder() => new CreatedByBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreatedBy &&
        id == other.id &&
        email == other.email &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        firstname == other.firstname &&
        lastname == other.lastname &&
        code == other.code &&
        gender == other.gender &&
        isdCode == other.isdCode &&
        phoneNumber == other.phoneNumber &&
        isActive == other.isActive &&
        roleId == other.roleId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, updatedAt.hashCode);
    _$hash = $jc(_$hash, firstname.hashCode);
    _$hash = $jc(_$hash, lastname.hashCode);
    _$hash = $jc(_$hash, code.hashCode);
    _$hash = $jc(_$hash, gender.hashCode);
    _$hash = $jc(_$hash, isdCode.hashCode);
    _$hash = $jc(_$hash, phoneNumber.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, roleId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CreatedBy')
          ..add('id', id)
          ..add('email', email)
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('firstname', firstname)
          ..add('lastname', lastname)
          ..add('code', code)
          ..add('gender', gender)
          ..add('isdCode', isdCode)
          ..add('phoneNumber', phoneNumber)
          ..add('isActive', isActive)
          ..add('roleId', roleId))
        .toString();
  }
}

class CreatedByBuilder implements Builder<CreatedBy, CreatedByBuilder> {
  _$CreatedBy? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _createdAt;
  String? get createdAt => _$this._createdAt;
  set createdAt(String? createdAt) => _$this._createdAt = createdAt;

  String? _updatedAt;
  String? get updatedAt => _$this._updatedAt;
  set updatedAt(String? updatedAt) => _$this._updatedAt = updatedAt;

  String? _firstname;
  String? get firstname => _$this._firstname;
  set firstname(String? firstname) => _$this._firstname = firstname;

  String? _lastname;
  String? get lastname => _$this._lastname;
  set lastname(String? lastname) => _$this._lastname = lastname;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  String? _gender;
  String? get gender => _$this._gender;
  set gender(String? gender) => _$this._gender = gender;

  String? _isdCode;
  String? get isdCode => _$this._isdCode;
  set isdCode(String? isdCode) => _$this._isdCode = isdCode;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  bool? _isActive;
  bool? get isActive => _$this._isActive;
  set isActive(bool? isActive) => _$this._isActive = isActive;

  int? _roleId;
  int? get roleId => _$this._roleId;
  set roleId(int? roleId) => _$this._roleId = roleId;

  CreatedByBuilder();

  CreatedByBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _email = $v.email;
      _createdAt = $v.createdAt;
      _updatedAt = $v.updatedAt;
      _firstname = $v.firstname;
      _lastname = $v.lastname;
      _code = $v.code;
      _gender = $v.gender;
      _isdCode = $v.isdCode;
      _phoneNumber = $v.phoneNumber;
      _isActive = $v.isActive;
      _roleId = $v.roleId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreatedBy other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreatedBy;
  }

  @override
  void update(void Function(CreatedByBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CreatedBy build() => _build();

  _$CreatedBy _build() {
    final _$result = _$v ??
        new _$CreatedBy._(
            id: id,
            email: email,
            createdAt: createdAt,
            updatedAt: updatedAt,
            firstname: firstname,
            lastname: lastname,
            code: code,
            gender: gender,
            isdCode: isdCode,
            phoneNumber: phoneNumber,
            isActive: isActive,
            roleId: roleId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
