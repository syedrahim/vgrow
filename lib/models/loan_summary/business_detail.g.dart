// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'business_detail.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BusinessDetail> _$businessDetailSerializer =
    new _$BusinessDetailSerializer();

class _$BusinessDetailSerializer
    implements StructuredSerializer<BusinessDetail> {
  @override
  final Iterable<Type> types = const [BusinessDetail, _$BusinessDetail];
  @override
  final String wireName = 'BusinessDetail';

  @override
  Iterable<Object?> serialize(Serializers serializers, BusinessDetail object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.loanID;
    if (value != null) {
      result
        ..add('loan_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.nature;
    if (value != null) {
      result
        ..add('nature')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.establishmentYear;
    if (value != null) {
      result
        ..add('establishment_year')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.rcNumber;
    if (value != null) {
      result
        ..add('rc_number')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.rcDocumentID;
    if (value != null) {
      result
        ..add('rc_document_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.rcDocumentUrl;
    if (value != null) {
      result
        ..add('rc_document_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.gstNumber;
    if (value != null) {
      result
        ..add('gst_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.gstDocumentID;
    if (value != null) {
      result
        ..add('gst_document_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.gstDocumentUrl;
    if (value != null) {
      result
        ..add('gst_document_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.monthlyRevenue;
    if (value != null) {
      result
        ..add('monthly_revenue')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.directCost;
    if (value != null) {
      result
        ..add('direct_cost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.indirectCost;
    if (value != null) {
      result
        ..add('indirect_cost')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.numberOfGoodsInAsset;
    if (value != null) {
      result
        ..add('no_of_goods_in_asset')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.numberOfInfrastructureInAsset;
    if (value != null) {
      result
        ..add('no_of_infrastructure_in_asset')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.infrastructureInAssetValue;
    if (value != null) {
      result
        ..add('infrastructure_in_asset_value')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.receivableCashOrDebit;
    if (value != null) {
      result
        ..add('receivable_cash_or_debit')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.payablesToSuppliers;
    if (value != null) {
      result
        ..add('payables_to_suppliers')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.numberOfInfrastructureInLiability;
    if (value != null) {
      result
        ..add('no_of_infrastructure_in_liability')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.monthlyExpense;
    if (value != null) {
      result
        ..add('monthly_expense')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.monthlyIncome;
    if (value != null) {
      result
        ..add('monthly_income')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.address;
    if (value != null) {
      result
        ..add('address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BusinessDetail deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BusinessDetailBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'loan_id':
          result.loanID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'nature':
          result.nature = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'establishment_year':
          result.establishmentYear = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'rc_number':
          result.rcNumber = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'rc_document_id':
          result.rcDocumentID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'rc_document_url':
          result.rcDocumentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'gst_number':
          result.gstNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'gst_document_id':
          result.gstDocumentID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'gst_document_url':
          result.gstDocumentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'monthly_revenue':
          result.monthlyRevenue = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'direct_cost':
          result.directCost = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'indirect_cost':
          result.indirectCost = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'no_of_goods_in_asset':
          result.numberOfGoodsInAsset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'no_of_infrastructure_in_asset':
          result.numberOfInfrastructureInAsset = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'infrastructure_in_asset_value':
          result.infrastructureInAssetValue = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'receivable_cash_or_debit':
          result.receivableCashOrDebit = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'payables_to_suppliers':
          result.payablesToSuppliers = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'no_of_infrastructure_in_liability':
          result.numberOfInfrastructureInLiability = serializers
              .deserialize(value, specifiedType: const FullType(int)) as int?;
          break;
        case 'monthly_expense':
          result.monthlyExpense = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'monthly_income':
          result.monthlyIncome = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$BusinessDetail extends BusinessDetail {
  @override
  final int? id;
  @override
  final int? loanID;
  @override
  final String? nature;
  @override
  final String? name;
  @override
  final int? establishmentYear;
  @override
  final int? rcNumber;
  @override
  final int? rcDocumentID;
  @override
  final String? rcDocumentUrl;
  @override
  final String? gstNumber;
  @override
  final int? gstDocumentID;
  @override
  final String? gstDocumentUrl;
  @override
  final double? monthlyRevenue;
  @override
  final double? directCost;
  @override
  final double? indirectCost;
  @override
  final int? numberOfGoodsInAsset;
  @override
  final int? numberOfInfrastructureInAsset;
  @override
  final double? infrastructureInAssetValue;
  @override
  final double? receivableCashOrDebit;
  @override
  final String? payablesToSuppliers;
  @override
  final int? numberOfInfrastructureInLiability;
  @override
  final double? monthlyExpense;
  @override
  final double? monthlyIncome;
  @override
  final String? address;

  factory _$BusinessDetail([void Function(BusinessDetailBuilder)? updates]) =>
      (new BusinessDetailBuilder()..update(updates))._build();

  _$BusinessDetail._(
      {this.id,
      this.loanID,
      this.nature,
      this.name,
      this.establishmentYear,
      this.rcNumber,
      this.rcDocumentID,
      this.rcDocumentUrl,
      this.gstNumber,
      this.gstDocumentID,
      this.gstDocumentUrl,
      this.monthlyRevenue,
      this.directCost,
      this.indirectCost,
      this.numberOfGoodsInAsset,
      this.numberOfInfrastructureInAsset,
      this.infrastructureInAssetValue,
      this.receivableCashOrDebit,
      this.payablesToSuppliers,
      this.numberOfInfrastructureInLiability,
      this.monthlyExpense,
      this.monthlyIncome,
      this.address})
      : super._();

  @override
  BusinessDetail rebuild(void Function(BusinessDetailBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BusinessDetailBuilder toBuilder() =>
      new BusinessDetailBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BusinessDetail &&
        id == other.id &&
        loanID == other.loanID &&
        nature == other.nature &&
        name == other.name &&
        establishmentYear == other.establishmentYear &&
        rcNumber == other.rcNumber &&
        rcDocumentID == other.rcDocumentID &&
        rcDocumentUrl == other.rcDocumentUrl &&
        gstNumber == other.gstNumber &&
        gstDocumentID == other.gstDocumentID &&
        gstDocumentUrl == other.gstDocumentUrl &&
        monthlyRevenue == other.monthlyRevenue &&
        directCost == other.directCost &&
        indirectCost == other.indirectCost &&
        numberOfGoodsInAsset == other.numberOfGoodsInAsset &&
        numberOfInfrastructureInAsset == other.numberOfInfrastructureInAsset &&
        infrastructureInAssetValue == other.infrastructureInAssetValue &&
        receivableCashOrDebit == other.receivableCashOrDebit &&
        payablesToSuppliers == other.payablesToSuppliers &&
        numberOfInfrastructureInLiability ==
            other.numberOfInfrastructureInLiability &&
        monthlyExpense == other.monthlyExpense &&
        monthlyIncome == other.monthlyIncome &&
        address == other.address;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, loanID.hashCode);
    _$hash = $jc(_$hash, nature.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, establishmentYear.hashCode);
    _$hash = $jc(_$hash, rcNumber.hashCode);
    _$hash = $jc(_$hash, rcDocumentID.hashCode);
    _$hash = $jc(_$hash, rcDocumentUrl.hashCode);
    _$hash = $jc(_$hash, gstNumber.hashCode);
    _$hash = $jc(_$hash, gstDocumentID.hashCode);
    _$hash = $jc(_$hash, gstDocumentUrl.hashCode);
    _$hash = $jc(_$hash, monthlyRevenue.hashCode);
    _$hash = $jc(_$hash, directCost.hashCode);
    _$hash = $jc(_$hash, indirectCost.hashCode);
    _$hash = $jc(_$hash, numberOfGoodsInAsset.hashCode);
    _$hash = $jc(_$hash, numberOfInfrastructureInAsset.hashCode);
    _$hash = $jc(_$hash, infrastructureInAssetValue.hashCode);
    _$hash = $jc(_$hash, receivableCashOrDebit.hashCode);
    _$hash = $jc(_$hash, payablesToSuppliers.hashCode);
    _$hash = $jc(_$hash, numberOfInfrastructureInLiability.hashCode);
    _$hash = $jc(_$hash, monthlyExpense.hashCode);
    _$hash = $jc(_$hash, monthlyIncome.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'BusinessDetail')
          ..add('id', id)
          ..add('loanID', loanID)
          ..add('nature', nature)
          ..add('name', name)
          ..add('establishmentYear', establishmentYear)
          ..add('rcNumber', rcNumber)
          ..add('rcDocumentID', rcDocumentID)
          ..add('rcDocumentUrl', rcDocumentUrl)
          ..add('gstNumber', gstNumber)
          ..add('gstDocumentID', gstDocumentID)
          ..add('gstDocumentUrl', gstDocumentUrl)
          ..add('monthlyRevenue', monthlyRevenue)
          ..add('directCost', directCost)
          ..add('indirectCost', indirectCost)
          ..add('numberOfGoodsInAsset', numberOfGoodsInAsset)
          ..add('numberOfInfrastructureInAsset', numberOfInfrastructureInAsset)
          ..add('infrastructureInAssetValue', infrastructureInAssetValue)
          ..add('receivableCashOrDebit', receivableCashOrDebit)
          ..add('payablesToSuppliers', payablesToSuppliers)
          ..add('numberOfInfrastructureInLiability',
              numberOfInfrastructureInLiability)
          ..add('monthlyExpense', monthlyExpense)
          ..add('monthlyIncome', monthlyIncome)
          ..add('address', address))
        .toString();
  }
}

class BusinessDetailBuilder
    implements Builder<BusinessDetail, BusinessDetailBuilder> {
  _$BusinessDetail? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  int? _loanID;
  int? get loanID => _$this._loanID;
  set loanID(int? loanID) => _$this._loanID = loanID;

  String? _nature;
  String? get nature => _$this._nature;
  set nature(String? nature) => _$this._nature = nature;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  int? _establishmentYear;
  int? get establishmentYear => _$this._establishmentYear;
  set establishmentYear(int? establishmentYear) =>
      _$this._establishmentYear = establishmentYear;

  int? _rcNumber;
  int? get rcNumber => _$this._rcNumber;
  set rcNumber(int? rcNumber) => _$this._rcNumber = rcNumber;

  int? _rcDocumentID;
  int? get rcDocumentID => _$this._rcDocumentID;
  set rcDocumentID(int? rcDocumentID) => _$this._rcDocumentID = rcDocumentID;

  String? _rcDocumentUrl;
  String? get rcDocumentUrl => _$this._rcDocumentUrl;
  set rcDocumentUrl(String? rcDocumentUrl) =>
      _$this._rcDocumentUrl = rcDocumentUrl;

  String? _gstNumber;
  String? get gstNumber => _$this._gstNumber;
  set gstNumber(String? gstNumber) => _$this._gstNumber = gstNumber;

  int? _gstDocumentID;
  int? get gstDocumentID => _$this._gstDocumentID;
  set gstDocumentID(int? gstDocumentID) =>
      _$this._gstDocumentID = gstDocumentID;

  String? _gstDocumentUrl;
  String? get gstDocumentUrl => _$this._gstDocumentUrl;
  set gstDocumentUrl(String? gstDocumentUrl) =>
      _$this._gstDocumentUrl = gstDocumentUrl;

  double? _monthlyRevenue;
  double? get monthlyRevenue => _$this._monthlyRevenue;
  set monthlyRevenue(double? monthlyRevenue) =>
      _$this._monthlyRevenue = monthlyRevenue;

  double? _directCost;
  double? get directCost => _$this._directCost;
  set directCost(double? directCost) => _$this._directCost = directCost;

  double? _indirectCost;
  double? get indirectCost => _$this._indirectCost;
  set indirectCost(double? indirectCost) => _$this._indirectCost = indirectCost;

  int? _numberOfGoodsInAsset;
  int? get numberOfGoodsInAsset => _$this._numberOfGoodsInAsset;
  set numberOfGoodsInAsset(int? numberOfGoodsInAsset) =>
      _$this._numberOfGoodsInAsset = numberOfGoodsInAsset;

  int? _numberOfInfrastructureInAsset;
  int? get numberOfInfrastructureInAsset =>
      _$this._numberOfInfrastructureInAsset;
  set numberOfInfrastructureInAsset(int? numberOfInfrastructureInAsset) =>
      _$this._numberOfInfrastructureInAsset = numberOfInfrastructureInAsset;

  double? _infrastructureInAssetValue;
  double? get infrastructureInAssetValue => _$this._infrastructureInAssetValue;
  set infrastructureInAssetValue(double? infrastructureInAssetValue) =>
      _$this._infrastructureInAssetValue = infrastructureInAssetValue;

  double? _receivableCashOrDebit;
  double? get receivableCashOrDebit => _$this._receivableCashOrDebit;
  set receivableCashOrDebit(double? receivableCashOrDebit) =>
      _$this._receivableCashOrDebit = receivableCashOrDebit;

  String? _payablesToSuppliers;
  String? get payablesToSuppliers => _$this._payablesToSuppliers;
  set payablesToSuppliers(String? payablesToSuppliers) =>
      _$this._payablesToSuppliers = payablesToSuppliers;

  int? _numberOfInfrastructureInLiability;
  int? get numberOfInfrastructureInLiability =>
      _$this._numberOfInfrastructureInLiability;
  set numberOfInfrastructureInLiability(
          int? numberOfInfrastructureInLiability) =>
      _$this._numberOfInfrastructureInLiability =
          numberOfInfrastructureInLiability;

  double? _monthlyExpense;
  double? get monthlyExpense => _$this._monthlyExpense;
  set monthlyExpense(double? monthlyExpense) =>
      _$this._monthlyExpense = monthlyExpense;

  double? _monthlyIncome;
  double? get monthlyIncome => _$this._monthlyIncome;
  set monthlyIncome(double? monthlyIncome) =>
      _$this._monthlyIncome = monthlyIncome;

  String? _address;
  String? get address => _$this._address;
  set address(String? address) => _$this._address = address;

  BusinessDetailBuilder();

  BusinessDetailBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _loanID = $v.loanID;
      _nature = $v.nature;
      _name = $v.name;
      _establishmentYear = $v.establishmentYear;
      _rcNumber = $v.rcNumber;
      _rcDocumentID = $v.rcDocumentID;
      _rcDocumentUrl = $v.rcDocumentUrl;
      _gstNumber = $v.gstNumber;
      _gstDocumentID = $v.gstDocumentID;
      _gstDocumentUrl = $v.gstDocumentUrl;
      _monthlyRevenue = $v.monthlyRevenue;
      _directCost = $v.directCost;
      _indirectCost = $v.indirectCost;
      _numberOfGoodsInAsset = $v.numberOfGoodsInAsset;
      _numberOfInfrastructureInAsset = $v.numberOfInfrastructureInAsset;
      _infrastructureInAssetValue = $v.infrastructureInAssetValue;
      _receivableCashOrDebit = $v.receivableCashOrDebit;
      _payablesToSuppliers = $v.payablesToSuppliers;
      _numberOfInfrastructureInLiability = $v.numberOfInfrastructureInLiability;
      _monthlyExpense = $v.monthlyExpense;
      _monthlyIncome = $v.monthlyIncome;
      _address = $v.address;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BusinessDetail other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BusinessDetail;
  }

  @override
  void update(void Function(BusinessDetailBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  BusinessDetail build() => _build();

  _$BusinessDetail _build() {
    final _$result = _$v ??
        new _$BusinessDetail._(
            id: id,
            loanID: loanID,
            nature: nature,
            name: name,
            establishmentYear: establishmentYear,
            rcNumber: rcNumber,
            rcDocumentID: rcDocumentID,
            rcDocumentUrl: rcDocumentUrl,
            gstNumber: gstNumber,
            gstDocumentID: gstDocumentID,
            gstDocumentUrl: gstDocumentUrl,
            monthlyRevenue: monthlyRevenue,
            directCost: directCost,
            indirectCost: indirectCost,
            numberOfGoodsInAsset: numberOfGoodsInAsset,
            numberOfInfrastructureInAsset: numberOfInfrastructureInAsset,
            infrastructureInAssetValue: infrastructureInAssetValue,
            receivableCashOrDebit: receivableCashOrDebit,
            payablesToSuppliers: payablesToSuppliers,
            numberOfInfrastructureInLiability:
                numberOfInfrastructureInLiability,
            monthlyExpense: monthlyExpense,
            monthlyIncome: monthlyIncome,
            address: address);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
