// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_comment.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<LoanComment> _$loanCommentSerializer = new _$LoanCommentSerializer();

class _$LoanCommentSerializer implements StructuredSerializer<LoanComment> {
  @override
  final Iterable<Type> types = const [LoanComment, _$LoanComment];
  @override
  final String wireName = 'LoanComment';

  @override
  Iterable<Object?> serialize(Serializers serializers, LoanComment object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.description;
    if (value != null) {
      result
        ..add('description')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.step;
    if (value != null) {
      result
        ..add('step')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.loanID;
    if (value != null) {
      result
        ..add('loan_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.creatorType;
    if (value != null) {
      result
        ..add('creator_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.creatorID;
    if (value != null) {
      result
        ..add('creator_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.creatorFirstname;
    if (value != null) {
      result
        ..add('creator_firstname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.creatorLastname;
    if (value != null) {
      result
        ..add('creator_lastname')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.cratorProfilePictureUrl;
    if (value != null) {
      result
        ..add('creator_profile_pic_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.createdAt;
    if (value != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.creatorBranch;
    if (value != null) {
      result
        ..add('creator_branch')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.attachment;
    if (value != null) {
      result
        ..add('attachment')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  LoanComment deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LoanCommentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'step':
          result.step = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'loan_id':
          result.loanID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'creator_type':
          result.creatorType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'creator_id':
          result.creatorID = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'creator_firstname':
          result.creatorFirstname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'creator_lastname':
          result.creatorLastname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'creator_profile_pic_url':
          result.cratorProfilePictureUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'creator_branch':
          result.creatorBranch = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'attachment':
          result.attachment = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$LoanComment extends LoanComment {
  @override
  final int? id;
  @override
  final String? description;
  @override
  final String? step;
  @override
  final int? loanID;
  @override
  final String? creatorType;
  @override
  final int? creatorID;
  @override
  final String? creatorFirstname;
  @override
  final String? creatorLastname;
  @override
  final String? cratorProfilePictureUrl;
  @override
  final String? createdAt;
  @override
  final String? creatorBranch;
  @override
  final String? attachment;

  factory _$LoanComment([void Function(LoanCommentBuilder)? updates]) =>
      (new LoanCommentBuilder()..update(updates))._build();

  _$LoanComment._(
      {this.id,
      this.description,
      this.step,
      this.loanID,
      this.creatorType,
      this.creatorID,
      this.creatorFirstname,
      this.creatorLastname,
      this.cratorProfilePictureUrl,
      this.createdAt,
      this.creatorBranch,
      this.attachment})
      : super._();

  @override
  LoanComment rebuild(void Function(LoanCommentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanCommentBuilder toBuilder() => new LoanCommentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoanComment &&
        id == other.id &&
        description == other.description &&
        step == other.step &&
        loanID == other.loanID &&
        creatorType == other.creatorType &&
        creatorID == other.creatorID &&
        creatorFirstname == other.creatorFirstname &&
        creatorLastname == other.creatorLastname &&
        cratorProfilePictureUrl == other.cratorProfilePictureUrl &&
        createdAt == other.createdAt &&
        creatorBranch == other.creatorBranch &&
        attachment == other.attachment;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, description.hashCode);
    _$hash = $jc(_$hash, step.hashCode);
    _$hash = $jc(_$hash, loanID.hashCode);
    _$hash = $jc(_$hash, creatorType.hashCode);
    _$hash = $jc(_$hash, creatorID.hashCode);
    _$hash = $jc(_$hash, creatorFirstname.hashCode);
    _$hash = $jc(_$hash, creatorLastname.hashCode);
    _$hash = $jc(_$hash, cratorProfilePictureUrl.hashCode);
    _$hash = $jc(_$hash, createdAt.hashCode);
    _$hash = $jc(_$hash, creatorBranch.hashCode);
    _$hash = $jc(_$hash, attachment.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanComment')
          ..add('id', id)
          ..add('description', description)
          ..add('step', step)
          ..add('loanID', loanID)
          ..add('creatorType', creatorType)
          ..add('creatorID', creatorID)
          ..add('creatorFirstname', creatorFirstname)
          ..add('creatorLastname', creatorLastname)
          ..add('cratorProfilePictureUrl', cratorProfilePictureUrl)
          ..add('createdAt', createdAt)
          ..add('creatorBranch', creatorBranch)
          ..add('attachment', attachment))
        .toString();
  }
}

class LoanCommentBuilder implements Builder<LoanComment, LoanCommentBuilder> {
  _$LoanComment? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _step;
  String? get step => _$this._step;
  set step(String? step) => _$this._step = step;

  int? _loanID;
  int? get loanID => _$this._loanID;
  set loanID(int? loanID) => _$this._loanID = loanID;

  String? _creatorType;
  String? get creatorType => _$this._creatorType;
  set creatorType(String? creatorType) => _$this._creatorType = creatorType;

  int? _creatorID;
  int? get creatorID => _$this._creatorID;
  set creatorID(int? creatorID) => _$this._creatorID = creatorID;

  String? _creatorFirstname;
  String? get creatorFirstname => _$this._creatorFirstname;
  set creatorFirstname(String? creatorFirstname) =>
      _$this._creatorFirstname = creatorFirstname;

  String? _creatorLastname;
  String? get creatorLastname => _$this._creatorLastname;
  set creatorLastname(String? creatorLastname) =>
      _$this._creatorLastname = creatorLastname;

  String? _cratorProfilePictureUrl;
  String? get cratorProfilePictureUrl => _$this._cratorProfilePictureUrl;
  set cratorProfilePictureUrl(String? cratorProfilePictureUrl) =>
      _$this._cratorProfilePictureUrl = cratorProfilePictureUrl;

  String? _createdAt;
  String? get createdAt => _$this._createdAt;
  set createdAt(String? createdAt) => _$this._createdAt = createdAt;

  String? _creatorBranch;
  String? get creatorBranch => _$this._creatorBranch;
  set creatorBranch(String? creatorBranch) =>
      _$this._creatorBranch = creatorBranch;

  String? _attachment;
  String? get attachment => _$this._attachment;
  set attachment(String? attachment) => _$this._attachment = attachment;

  LoanCommentBuilder();

  LoanCommentBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _description = $v.description;
      _step = $v.step;
      _loanID = $v.loanID;
      _creatorType = $v.creatorType;
      _creatorID = $v.creatorID;
      _creatorFirstname = $v.creatorFirstname;
      _creatorLastname = $v.creatorLastname;
      _cratorProfilePictureUrl = $v.cratorProfilePictureUrl;
      _createdAt = $v.createdAt;
      _creatorBranch = $v.creatorBranch;
      _attachment = $v.attachment;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanComment other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanComment;
  }

  @override
  void update(void Function(LoanCommentBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanComment build() => _build();

  _$LoanComment _build() {
    final _$result = _$v ??
        new _$LoanComment._(
            id: id,
            description: description,
            step: step,
            loanID: loanID,
            creatorType: creatorType,
            creatorID: creatorID,
            creatorFirstname: creatorFirstname,
            creatorLastname: creatorLastname,
            cratorProfilePictureUrl: cratorProfilePictureUrl,
            createdAt: createdAt,
            creatorBranch: creatorBranch,
            attachment: attachment);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
