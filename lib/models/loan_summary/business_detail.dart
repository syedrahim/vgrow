import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'business_detail.g.dart';

abstract class BusinessDetail implements Built<BusinessDetail, BusinessDetailBuilder>{
  BusinessDetail._();
  factory BusinessDetail([BusinessDetailBuilder updates(BusinessDetailBuilder builder)]) = _$BusinessDetail;

  int? get id;

  @BuiltValueField(wireName: 'loan_id')
  int? get loanID;

  String? get nature;

  String? get name;

  @BuiltValueField(wireName: 'establishment_year')
  int? get establishmentYear;

  @BuiltValueField(wireName: 'rc_number')
  int? get rcNumber;

  @BuiltValueField(wireName: 'rc_document_id')
  int? get rcDocumentID;

  @BuiltValueField(wireName: 'rc_document_url')
  String? get rcDocumentUrl;

  @BuiltValueField(wireName: 'gst_number')
  String? get gstNumber;

  @BuiltValueField(wireName: 'gst_document_id')
  int? get gstDocumentID;

  @BuiltValueField(wireName: 'gst_document_url')
  String? get gstDocumentUrl;

  @BuiltValueField(wireName: 'monthly_revenue')
  double? get monthlyRevenue;

  @BuiltValueField(wireName: 'direct_cost')
  double? get directCost;

  @BuiltValueField(wireName: 'indirect_cost')
  double? get indirectCost;

  @BuiltValueField(wireName: 'no_of_goods_in_asset')
  int? get numberOfGoodsInAsset;

  @BuiltValueField(wireName: 'no_of_infrastructure_in_asset')
  int? get numberOfInfrastructureInAsset;

  @BuiltValueField(wireName: 'infrastructure_in_asset_value')
  double? get infrastructureInAssetValue;

  @BuiltValueField(wireName: 'receivable_cash_or_debit')
  double? get receivableCashOrDebit;

  @BuiltValueField(wireName: 'payables_to_suppliers')
  String? get payablesToSuppliers;

  @BuiltValueField(wireName: 'no_of_infrastructure_in_liability')
  int? get numberOfInfrastructureInLiability;

  @BuiltValueField(wireName: 'monthly_expense')
  double? get monthlyExpense;

  @BuiltValueField(wireName: 'monthly_income')
  double? get monthlyIncome;

  String? get address;

  static Serializer<BusinessDetail> get serializer => _$businessDetailSerializer;
}