import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'loan_branch.g.dart';

abstract class LoanBranch implements Built<LoanBranch, LoanBranchBuilder> {
  LoanBranch._();
  factory LoanBranch([updates(LoanBranchBuilder b)]) = _$LoanBranch;

  static Serializer<LoanBranch> get serializer => _$loanBranchSerializer;

  int? get id;
  String? get name;
  String? get code;

  @BuiltValueField(wireName: 'organisation_name')
  String? get organisationName;
}
