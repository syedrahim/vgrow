import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:vgrow/models/models.dart';

import 'loan_summary/additional_details.dart';
import 'loan_summary/bank_detail.dart';
import 'loan_summary/crif_score.dart';
import 'loan_summary/eligibility_detail.dart';
import 'loan_summary/existing_loan.dart';
import 'loan_summary/family_detail.dart';
import 'loan_summary/family_member.dart';
import 'loan_summary/loan_address.dart';
import 'loan_summary/created_by.dart';
import 'loan_summary/loan_address.dart';
import 'loan_summary/loan_center.dart';
import 'loan_summary/loan_customer.dart';
import 'loan_summary/loan_document.dart';
import 'loan_summary/owned_entity.dart';

part 'serializers.g.dart';

@SerializersFor(<Type>[

  Pagination,
  ApiError,
  ApiSuccess,
  AppUser,
  Notifications,
  FileAttachment,
  AccessToken,
  Loans,
  LoanSummary,
  LoanAddress,
  LoanAddressAttachment,
  LoanCustomer,
  EligibilityDetail,
  CreatedBy,
])
final Serializers serializers =
(_$serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();



