import 'package:vgrow/connector/auth_connector.dart';
import 'package:vgrow/views/home/home_page.dart';
import 'package:vgrow/views/loader/app_loader.dart';
import 'package:vgrow/views/auth/login_page.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/views/loan_summary/loan_summary.dart';
import 'package:vgrow/views/submit_loans/LoansPage.dart';

class InitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthConnector(
      builder: (BuildContext c, AuthViewModel model) {
        // if (model.isInitializing) {
        //   return AppLoader();
        // }
        return model.currentUser == null ? LoginPage() : const HomePage();
        // return LoanSummaryPage();
      },
    );
  }
}
