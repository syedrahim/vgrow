import 'package:vgrow/core/theme/app_assets.dart';
import 'package:vgrow/core/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/connector/auth_connector.dart';
import 'package:vgrow/global_widgets/rounded_button.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';

import '../../core/theme/app_styles.dart';
import '../../global_widgets/text_input.dart';

class LoginPage extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final Hero logo = Hero(
    tag: 'hero',
    child: CircleAvatar(
      backgroundColor: Colors.transparent,
      radius: 48.0,
      child: Text('qw12'),
    ),
  );

  String? _emailValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }

  String? _passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return AuthConnector(
      builder: (BuildContext c, AuthViewModel model) {
        return Scaffold(
          backgroundColor: AppColors.darkGreenColor,
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Image.asset(
                  AppAssets.appLogo,
                  width: 100,
                ),
                getSpace(40, 0),
                Form(
                  key: _formKey,
                  child: Container(
                    // height: 700,
                    width: double.maxFinite,
                    decoration: const BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          topLeft: Radius.circular(20)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(30, 32, 30, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Login to VGro',
                            textAlign: TextAlign.left,
                            style: AppStyle.fullBlack32BoldTextstyle,
                          ),
                          getSpace(48, 0),
                          Text(
                            'Employee ID',
                            style: AppStyle.black14SemiboldTextStyle,
                          ),
                          getSpace(12, 0),
                          TextInput(
                            hintText: 'Employee ID',
                            controller: _emailController,
                            validator: _emailValidator,
                          ),
                          getSpace(32, 0),
                          Text(
                            'Password',
                            style: AppStyle.black14SemiboldTextStyle,
                          ),
                          getSpace(12, 0),
                          TextInput(
                            hintText: 'Password',
                            controller: _passwordController,
                            validator: _passwordValidator,
                            filled: true,
                            enableBorder: false,
                            hideText: true,
                          ),
                          getSpace(180, 0),
                          SizedBox(
                            width: double.maxFinite,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                RoundedButton(
                                    buttonText: 'Login',
                                    onPressed: () {
                                      if (_formKey.currentState!.validate()) {
                                        model.loginWithPassword(
                                            _emailController.text,
                                            '',
                                            _passwordController.text);
                                        // ScaffoldMessenger.of(context)
                                        //     .showSnackBar(
                                        //   const SnackBar(
                                        //       content: Text('Processing Data')),
                                        // );
                                      }
                                    }),
                                getSpace(10, 0),
                                RoundedButton(
                                    buttonText: 'Forgot Password ?',
                                    enableBackgroundColor: false,
                                    onPressed: () {}),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
