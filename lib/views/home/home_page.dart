import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/connector/auth_connector.dart';
import 'package:vgrow/connector/loan/loan_connector.dart';
import 'package:vgrow/core/theme/app_assets.dart';
import 'package:vgrow/global_widgets/grid_item.dart';
import 'package:vgrow/global_widgets/rounded_button.dart';
import 'package:vgrow/global_widgets/swipe_button.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';
import 'package:vgrow/views/more_menu/more_menu.dart';
import 'package:vgrow/views/submit_loans/LoansPage.dart';

import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/all_app_bar.dart';
import '../../global_widgets/progress_arc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  static const List _gridItems = [
    ['231','Customer'],
    ['147','All Loans'],
    ['23','Applied Loans'],
    ['34','Submitted Loans'],
    ['132','Resubmit Loans'],
    ['54','Approved Loans'],
  ];

  @override
  Widget build(BuildContext context) {
    return AuthConnector(
      builder: (BuildContext authContext, AuthViewModel authModel) {
        return LoanConnector(
            builder: (BuildContext loanContext, LoanViewModel loanModel) {
          return Scaffold(
            appBar: const AllAppBar(),
            drawer: const Drawer(
              width: double.maxFinite,
              child: MoreMenu(),
            ),
            backgroundColor: AppColors.darkGreenColor,
            body: Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Hello BM',
                        style: AppStyle.white32BoldTextStyle,
                      ),
                      Container(
                        height: 42.0,
                        width: 42.0,
                        decoration: BoxDecoration(
                            color: const Color(0XFF105F49).withAlpha(250),
                            borderRadius: BorderRadius.circular(5)),
                        child: IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.filter_alt_sharp,
                              color: AppColors.white,
                            )),
                      )
                    ],
                  ),
                  SwipeButton(),
                  SizedBox(
                    width: 250,
                    height: 250,
                    child: CustomPaint(
                      size:
                          const Size(250, 250), // no effect while adding child
                      painter: CircularPaint(
                        progressValue: .7, //[0-1]
                      ),
                      child: Center(
                        child: Column(
                          children: [
                            getSpace(30, 0),
                            Image.asset(
                              AppAssets.flag,
                              width: 40,
                            ),
                            getSpace(23, 0),
                            const Text(
                              '₹5,20,000',
                              style: AppStyle.white30BoldTextStyle,
                            ),
                            getSpace(23, 0),
                            Text(
                              '/₹10,00,000',
                              style: AppStyle.osloBlue22MediumTextStyle,
                            ),
                            getSpace(27, 0),
                            const Text(
                              'Target',
                              style: AppStyle.white17RegularTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  getSpace(40, 0),
                  GridView.builder(
                      shrinkWrap: true,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 5,
                              mainAxisSpacing: 5),
                      itemCount: HomePage._gridItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GridItem(title: HomePage._gridItems[index][0], subtitle: HomePage._gridItems[index][1]);
                      }),
                  RoundedButton(
                    buttonText: 'SUBMIT LOANS',
                    onPressed: () {
                      loanModel.getLoanDetails();
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => const LoansPage()));
                    },
                    backgroundColor: AppColors.lightGreen,
                    textStyle: AppStyle.darkGreen15MediumTextStyle,
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }
}
