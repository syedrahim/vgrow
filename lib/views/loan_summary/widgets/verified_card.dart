import 'package:flutter/material.dart';

import '../../../core/theme/app_assets.dart';
import '../../../core/theme/app_colors.dart';
import '../../../core/theme/app_styles.dart';
import '../../../global_widgets/widget_helper.dart';

class VerifiedCard extends StatelessWidget {
  const VerifiedCard({Key? key, required this.title, required this.subtitle}) : super(key: key);

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
      padding: const EdgeInsets.all(20),
      width: double.maxFinite,
      height: 90,
      decoration: BoxDecoration(
          color: AppColors.lightGreen3,
          borderRadius: BorderRadius.circular(12)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(title, style: AppStyle.black15RegularTextStyle,),
              getSpace(10, 0),
              Text(subtitle,style: AppStyle.black15SemiboldTextStyle,),
            ],
          ),
          Image.asset(AppAssets.verified,width: 40,),
        ],
      ),
    );
  }
}
