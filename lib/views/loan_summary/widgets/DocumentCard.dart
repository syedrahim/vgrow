import 'package:flutter/material.dart';

import '../../../core/theme/app_styles.dart';
import '../../../global_widgets/widget_helper.dart';

class DocumentCard extends StatelessWidget {
  const DocumentCard(
      {Key? key,
      required this.documentName,
      required this.documentNumber,
      required this.documentFrontSideUrl,
      this.documentBackSideUrl = ""})
      : super(key: key);

  final String documentName;
  final String documentNumber;
  final String documentFrontSideUrl;
  final String documentBackSideUrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        getSpace(7, 0),
        Text(
          documentName,
          style: AppStyle.black13RegularTextStyle,
        ),
        getSpace(7, 0),
        Text(
          documentNumber,
          style: AppStyle.black16SemiboldTextStyle,
        ),
        getSpace(5, 0),
        const Divider(),
        getSpace(5, 0),
        Text(
          '$documentName - front side',
          style: AppStyle.black16SemiboldTextStyle,
        ),
        getSpace(5, 0),
        if(documentFrontSideUrl != "null")
          Image.network(
            documentFrontSideUrl,
            width: 500,
          ),
        if (documentBackSideUrl != "")
          Text(
            '$documentName - back side',
            style: AppStyle.black16SemiboldTextStyle,
          ),
        if (documentBackSideUrl != "") getSpace(5, 0),
        if (documentBackSideUrl != "")
          Image.network(
            documentBackSideUrl,
            width: 500,
          )
      ],
    );
  }
}
