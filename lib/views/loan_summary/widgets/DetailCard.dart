import 'package:flutter/material.dart';

import '../../../core/theme/app_assets.dart';
import '../../../core/theme/app_colors.dart';
import '../../../core/theme/app_styles.dart';
import '../../../global_widgets/widget_helper.dart';

TableRow renderTableRow(row1, row2) {
  return TableRow(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              row1 ?? '---',
              style: AppStyle.black15SemiboldTextStyle,
            ),
            getSpace(10, 0),
            Text(
              row2 ?? '---',
              style: AppStyle.black15RegularTextStyle,
            ),
          ],
        ),
      ),
    ],
  );
}

TableRow renderTableRow1(column1, column2) {
  return TableRow(
    children: [
      Padding(
        padding: const EdgeInsets.all(10),
        child: Text(
          column1 ?? '---',
          style: AppStyle.fullBlack16SemiboldTextstyle,
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(10),
        child: Text(
          column2 ?? '---',
          style: AppStyle.black14RegularTextStyle,
        ),
      )
    ],
  );
}

class DetailCard extends StatelessWidget {
  const DetailCard(
      {Key? key, this.title = "", this.subtitle = "", required this.data, this.showHeader = false, this.showProfile = true})
      : super(key: key);

  final String title;
  final String subtitle;
  final List data;
  final bool showHeader;
  final bool showProfile;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
      decoration: BoxDecoration(
          color: AppColors.white, borderRadius: BorderRadius.circular(12)),
      child: Column(
        children: [
          if(showHeader)
            Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if(showProfile)
                Image.asset(
                  AppAssets.person,
                  width: 40,
                ),
              if(showProfile)
                getSpace(0, 40),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FittedBox(
                    child: Text(
                      title,
                      style: AppStyle.black16SemiboldTextStyle,
                      maxLines: 2,
                      softWrap: false,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  getSpace(10, 0),
                  if(subtitle!="")
                    Text(
                      subtitle,
                      style: AppStyle.black15SemiboldTextStyle,
                    ),
                ],
              ),
            ],
          ),
          if(showHeader)
            const Divider(
            color: AppColors.darkGray,
           ),
          Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.bottom,
            children: [
              for (int i = 0; i < data.length; i++)
                renderTableRow1(data[i][0], data[i][1]),
            ],
          ),
        ],
      ),
    );
  }
}
