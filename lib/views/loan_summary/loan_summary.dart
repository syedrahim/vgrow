import 'package:flutter/material.dart';
import 'package:vgrow/models/loan_summary/additional_details.dart';
import 'package:vgrow/models/loan_summary/eligibility_detail.dart';
import 'package:vgrow/models/loan_summary/family_member.dart';
import 'package:vgrow/models/loan_summary/loan_customer.dart';
import 'package:vgrow/models/loan_summary/loan_document.dart';
import 'package:vgrow/views/loan_summary/widgets/DetailCard.dart';
import 'package:vgrow/views/loan_summary/widgets/DocumentCard.dart';
import 'package:vgrow/views/loan_summary/widgets/verified_card.dart';

import '../../connector/loan_summary/loan_summary_connector.dart';
import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/rounded_button.dart';
import '../../global_widgets/widget_helper.dart';
import '../../models/loan_summary/bank_detail.dart';

class LoanSummaryPage extends StatelessWidget {
  LoanSummaryPage({Key? key}) : super(key: key);

  final List<String> _loanSummary = [
    ' Status of Customer Housing?',
    ' Type of house?',
    ' Number of earning members in the family?',
    ' Ownership of the business building?',
    ' How old is the business?',
    ' Business model type?',
  ];

  final List<String> _customerDetails = [
    'DOB',
    'Age',
    'Gender',
    'Address',
    'Phone Number',
    'Email',
    'Educational Qualification',
  ];

  TableRow renderTableRow(row1, row2) {
    return TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                row1 ?? '---',
                style: AppStyle.black15SemiboldTextStyle,
              ),
              getSpace(10, 0),
              Text(
                row2 ?? '---',
                style: AppStyle.black15RegularTextStyle,
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return LoanSummaryConnector(
        builder: (BuildContext c, LoanSummaryViewModel model) {
      AdditionalDetailsJson? additionalDetailsJson;
      EligibilityDetail? eligibilityDetail;
      LoanCustomer? customer;
      BankDetail? bankDetail;
      List<FamilyMember> familyMembers = [];
      List<LoanDocument> loanDocuments = [];

      if (model.isLoading == false && model.loanSummary != null) {
        if (model.loanSummary!.customer != null) {
          customer = model.loanSummary!.customer;
          familyMembers = model.loanSummary!.customer!.familyMembers.toList();
          bankDetail = model.loanSummary!.customer!.bankDetail;
          if (model.loanSummary!.customer!.documents != null) {
            loanDocuments = model.loanSummary!.customer!.documents!.toList();
          }
        }
        if (model.loanSummary!.eligibilityDetail != null) {
          eligibilityDetail = model.loanSummary!.eligibilityDetail;
        }
        if (model.loanSummary!.additionalDetailsJson != null) {
          additionalDetailsJson = model.loanSummary!.additionalDetailsJson;
        }

      }

      return Scaffold(
        backgroundColor: AppColors.darkGreenColor,
        body: Padding(
          padding: const EdgeInsets.fromLTRB(0, 60, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '9. Loan Summary',
                      textAlign: TextAlign.left,
                      style: AppStyle.grey13RegularTextStyle,
                    ),
                    getSpace(16, 0),
                    Text(
                      'Loan Summary',
                      // customer!.phoneNumber!.toString(),
                      textAlign: TextAlign.left,
                      style: AppStyle.grey24BoldTextStyle,
                    ),
                    getSpace(30, 0),
                  ],
                ),
              ),
              Container(
                width: double.maxFinite,
                decoration: const BoxDecoration(
                  color: AppColors.backgroundGray,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                ),
                child: model.isLoading
                    ? Center(
                        child: Container(
                            padding: EdgeInsets.all(5),
                            width: 50,
                            height: 50,
                            child: CircularProgressIndicator()),
                      )
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 600,
                              child: ListView(shrinkWrap: true, children: [
                                if (eligibilityDetail != null)
                                  Container(
                                    decoration: BoxDecoration(
                                        color: AppColors.white,
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    child: Padding(
                                      padding: const EdgeInsets.all(12),
                                      child: Table(
                                        border: const TableBorder(
                                            horizontalInside: BorderSide(
                                                width: .2,
                                                color: AppColors.darkGray,
                                                style: BorderStyle.solid)),
                                        defaultVerticalAlignment:
                                            TableCellVerticalAlignment.bottom,
                                        children: [
                                          if (eligibilityDetail!
                                                  .housingOwnershipStatus !=
                                              null)
                                            renderTableRow(
                                                _loanSummary[0],
                                                eligibilityDetail!
                                                    .housingOwnershipStatus),
                                          if (eligibilityDetail!
                                                  .typeOfResidence !=
                                              null)
                                            renderTableRow(
                                                _loanSummary[1],
                                                eligibilityDetail!
                                                    .typeOfResidence),
                                          if (eligibilityDetail!
                                                  .earningMembersCount !=
                                              null)
                                            renderTableRow(_loanSummary[2],
                                                "${eligibilityDetail!.earningMembersCount.toString()} Members"),
                                          if (eligibilityDetail!
                                                  .housingOwnershipStatus !=
                                              null)
                                            renderTableRow(_loanSummary[3],
                                                "Business in a ${eligibilityDetail!.housingOwnershipStatus} building."),
                                          if (eligibilityDetail!
                                                  .yearsOfBusiness !=
                                              null)
                                            renderTableRow(_loanSummary[4],
                                                "${eligibilityDetail!.yearsOfBusiness.toString()} years, ${eligibilityDetail!.monthsOfBusiness.toString()} months"),
                                          if (eligibilityDetail!
                                                  .typeOfBusinessModel !=
                                              null)
                                            renderTableRow(_loanSummary[5],
                                                "${eligibilityDetail!.typeOfBusinessModel} ownership"),
                                        ],
                                      ),
                                    ),
                                  ),
                                if (customer != null)
                                  VerifiedCard(
                                      title: 'Pan card verified',
                                      subtitle: customer!.panNumber.toString()),
                                if (customer != null)
                                  VerifiedCard(
                                      title: 'Aadhaar Details verified',
                                      subtitle: customer!.aadhaarNumber ?? "-"),
                                if (customer != null)
                                  DetailCard(
                                      showHeader: true,
                                      title:
                                          "${customer!.firstname ?? "-"} \n ${customer!.lastname ?? " "}",
                                      subtitle:
                                          customer!.aadhaarNumber.toString(),
                                      data: [
                                        [
                                          _customerDetails[0],
                                          customer!.dob ?? '-'
                                        ],
                                        [
                                          _customerDetails[1],
                                          customer!.age.toString() ?? '-'
                                        ],
                                        [_customerDetails[2], customer!.gender],
                                        if (model.loanSummary!.customer!
                                                .address !=
                                            null) ...[
                                          _customerDetails[3],
                                          model.loanSummary!.customer!.address!
                                              .toString()
                                        ],
                                      ]),
                                if (customer != null)
                                  DetailCard(data: [
                                    [
                                      _customerDetails[4],
                                      customer!.phoneNumber
                                    ],
                                    [_customerDetails[5], customer!.email],
                                    [
                                      _customerDetails[6],
                                      customer!.educationalQualification
                                    ],
                                  ]),
                                if (customer != null)
                                  if (additionalDetailsJson?.address != null)
                                    DetailCard(data: [
                                      [
                                        _customerDetails[3],
                                        additionalDetailsJson!.address
                                            .toString()
                                      ],
                                    ]),
                                if (customer != null && customer!.profilePictureUrl != null)
                                  Image.network(
                                      customer!.profilePictureUrl.toString()),
                                if (additionalDetailsJson != null)
                                  DetailCard(
                                      showHeader: true,
                                      showProfile: false,
                                      title: 'Nature of Business',
                                      data: [
                                        [
                                          'Nature of Business',
                                          additionalDetailsJson!
                                                  .natureOfBusiness ??
                                              '--'
                                        ],
                                        [
                                          'Enterprise Name',
                                          additionalDetailsJson!
                                                  .enterpriseName ??
                                              '--'
                                        ],
                                        [
                                          'Business Address',
                                          additionalDetailsJson!.address
                                              .toString()
                                        ],
                                      ]),
                                if (additionalDetailsJson != null)
                                  DetailCard(
                                      showHeader: true,
                                      showProfile: false,
                                      title: 'Profit and Loss account',
                                      data: [
                                        [
                                          'Monthly revenue',
                                          additionalDetailsJson!.monthlyRevenue
                                              .toString()
                                        ],
                                        [
                                          'Direct cost (Material)',
                                          additionalDetailsJson!.directCost
                                              .toString()
                                        ],
                                        [
                                          'Indirect cost (Operational costs)',
                                          additionalDetailsJson!.indirectCost
                                              .toString()
                                        ],
                                      ]),
                                if (additionalDetailsJson != null)
                                  DetailCard(
                                      showHeader: true,
                                      showProfile: false,
                                      title: 'Present business asset',
                                      data: [
                                        [
                                          'How much Goods?',
                                          additionalDetailsJson!
                                              .noOfGoodsInasset
                                              .toString()
                                        ],
                                        [
                                          'How much infrastructure?',
                                          additionalDetailsJson!
                                              .noOfInfrastructureInasset
                                              .toString()
                                        ],
                                        [
                                          'Infrastructure value?',
                                          additionalDetailsJson!
                                              .infrastructureInAssetValue
                                              .toString()
                                        ],
                                        [
                                          'How much Cash/Debt receivables?',
                                          additionalDetailsJson!
                                              .receivableCashOrDebit
                                              .toString()
                                        ],
                                      ]),
                                if (familyMembers.length != 0)
                                  for (var familyMember in familyMembers)
                                    DetailCard(
                                        showHeader: true,
                                        title: familyMember.name.toString(),
                                        subtitle: familyMember.relationship
                                            .toString(),
                                        data: [
                                          ['Age', familyMember.age.toString()],
                                          [
                                            'Occupation',
                                            familyMember.occupation ?? '--'
                                          ],
                                          [
                                            'Income',
                                            "${familyMember.bussinessIncome ?? "--"} /month"
                                          ],
                                          if (familyMember!.isCoBorrower ==
                                              true) ...[
                                            ['Co-Borrower', 'Yes'],
                                            [
                                              'Nominee',
                                              familyMember!.isNominee == true
                                                  ? 'Yes'
                                                  : 'No'
                                            ],
                                            [
                                              'Location',
                                              familyMember.location.toString()
                                            ],
                                            [
                                              'Mobile',
                                              familyMember.mobileNumber
                                                  .toString()
                                            ],
                                            [
                                              'Aadhaar',
                                              familyMember.aadhaarNumber
                                                  .toString()
                                            ],
                                            [
                                              'PAN',
                                              familyMember.panNumber.toString()
                                            ],
                                          ]
                                        ]),
                                if (customer != null)
                                  DocumentCard(
                                      documentName: 'Aadhar',
                                      documentNumber:
                                          customer!.aadhaarNumber.toString(),
                                      documentFrontSideUrl: customer!
                                          .aadharFrontImageUrl.toString(),
                                      documentBackSideUrl: customer!
                                          .aadharBackImageUrl
                                          .toString()),
                                if (customer != null)
                                  DocumentCard(
                                    documentName: 'PAN',
                                    documentNumber:
                                        customer!.panNumber.toString(),
                                    documentFrontSideUrl:
                                        customer!.panDocumentUrl.toString(),
                                  ),
                                if (loanDocuments.length != 0)
                                  for (int i = 0; i < loanDocuments.length; i++)
                                    DocumentCard(
                                      documentName: loanDocuments[i]
                                          .documentName
                                          .toString(),
                                      documentNumber: loanDocuments[i].id
                                          .toString(),
                                      documentFrontSideUrl: loanDocuments[i]
                                          .frontSideImageUrl
                                          .toString(),
                                      documentBackSideUrl: loanDocuments[i]
                                          .backSideImageUrl
                                          .toString(),
                                    ),
                                if (bankDetail != null)
                                  VerifiedCard(
                                      title: 'Bank Details verified',
                                      subtitle:
                                          bankDetail!.accountNumber.toString()),
                                if (customer != null && bankDetail != null)
                                  DetailCard(data: [
                                    ['Bank', bankDetail!.bankName],
                                    [
                                      'Name',
                                      "${customer!.firstname} ${customer!.lastname}"
                                    ],
                                    ['Account No', bankDetail!.accountNumber],
                                    ['IFSC', bankDetail!.ifscCode],
                                  ]),
                              ]),
                            ),
                            SizedBox(
                              width: double.maxFinite,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  RoundedButton(
                                      buttonText: 'CONFIRM AND SUBMIT LOAN',
                                      onPressed: () {}),
                                  getSpace(10, 0),
                                  RoundedButton(
                                      buttonText: 'REJECT LOAN',
                                      backgroundColor: AppColors.backgroundGray,
                                      textStyle:
                                          AppStyle.darkGreen15MediumTextStyle,
                                      onPressed: () {}),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
