import 'package:flutter/material.dart';
import 'package:vgrow/connector/auth_connector.dart';
import 'package:vgrow/core/theme/app_assets.dart';
import 'package:vgrow/core/theme/app_styles.dart';

import '../../core/theme/app_colors.dart';

class MyProfile extends StatelessWidget {
  MyProfile({Key? key}) : super(key: key);

  final _list = [
    'Full Name',
    'User ID',
    'Branch',
    'Region',
    'Phone',
    'Email',
    'Aadhaar Number',
    'Role',
    'Assigned IA',
  ];

  TableRow renderTableRow(column1, column2) {
    return TableRow(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
          child: Text(
            column1 ?? '---',
            style: AppStyle.darkGrey14RegularTextStyle,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 10),
          child: Text(
            column2 ?? '---',
            style: AppStyle.fullBlack16SemiboldTextstyle,
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AuthConnector(builder: (BuildContext c, AuthViewModel model) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.darkGreenColor,
          elevation: 0.0,
          title: Text(
            'My Profile',
            style: AppStyle.grey24BoldTextStyle,
          ),
          centerTitle: false,
        ),
        backgroundColor: AppColors.darkGreenColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Stack(
                alignment: Alignment.topCenter,
                clipBehavior: Clip.none,
                children: [
                  Container(
                    height: 720,
                    width: double.maxFinite,
                    decoration: const BoxDecoration(
                        color: AppColors.backgroundGray,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20))),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 100, 20, 30),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(31, 42, 31, 0),
                        decoration: BoxDecoration(
                            color: AppColors.white,
                            borderRadius: BorderRadius.circular(12)),
                        child: Table(
                          border: TableBorder(
                              horizontalInside: BorderSide(
                                  width: .2,
                                  color: AppColors.darkGray,
                                  style: BorderStyle.solid)),
                          defaultVerticalAlignment:
                              TableCellVerticalAlignment.bottom,
                          children: [
                            renderTableRow(_list[0], model.currentUser!.firstName),
                            renderTableRow(_list[1], model.currentUser!.userId.toString()),
                            renderTableRow(_list[2], '-'),
                            renderTableRow(_list[3], '-'),
                            renderTableRow(_list[4], model.currentUser!.phone_number),
                            renderTableRow(_list[5], model.currentUser!.email),
                            renderTableRow(_list[6], '-'),
                            renderTableRow(_list[7], model.currentUser!.role_name),
                            renderTableRow(_list[8], '-'),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      top: -70,
                      child: Image.asset(
                        AppAssets.profilePic,
                        width: 150,
                      )),
                  Positioned(
                    top: 20,
                    right: 130,
                    child: CircleAvatar(
                      radius: 25,
                      backgroundColor: AppColors.white,
                      child: Image.asset(
                        AppAssets.camera,
                        width: 20,
                      ),
                    ),
                  )
                ]),
          ],
        ),
      );
    });
  }
}
