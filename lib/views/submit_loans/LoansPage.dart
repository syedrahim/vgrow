import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vgrow/connector/loan/loan_connector.dart';
import 'package:vgrow/connector/loan_summary/loan_summary_connector.dart';
import 'package:vgrow/global_widgets/LoanItem.dart';
import 'package:vgrow/global_widgets/all_app_bar.dart';
import 'package:vgrow/global_widgets/tab_container.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';

import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/icon_container.dart';
import '../../global_widgets/text_input.dart';
import '../loan_summary/loan_summary.dart';
import '../more_menu/more_menu.dart';

class LoansPage extends StatefulWidget {
  const LoansPage({Key? key}) : super(key: key);

  @override
  State<LoansPage> createState() => _LoansPageState();
}

class _LoansPageState extends State<LoansPage> {
  String? _validator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Please enter some text';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return LoanConnector(builder: (BuildContext c, LoanViewModel model) {
      var loanList = model.loanDetails!;
      return LoanSummaryConnector(
          builder: (loanSummaryContext, loanSummaryModel) {
        return Scaffold(
          appBar: const AllAppBar(),
          drawer: const Drawer(
            width: double.maxFinite,
            child: MoreMenu(),
          ),
          backgroundColor: AppColors.darkGreenColor,
          body: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 30),
                  child: Text(
                    'Loans',
                    style: AppStyle.white32BoldTextStyle,
                  ),
                ),
                DefaultTabController(
                    length: 4, // length of tabs
                    initialIndex: 0,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          SizedBox(
                            width: double.maxFinite,
                            child: TabBar(
                              indicatorColor: Colors.transparent,
                              labelStyle: AppStyle.white16BoldTextStyle,
                              unselectedLabelStyle:
                                  AppStyle.white14RegularTextStyle.copyWith(
                                      color: AppColors.white.withOpacity(0.50)),
                              tabs: const [
                                Tab(text: 'All'),
                                Tab(text: 'Applied'),
                                Tab(text: 'Submitted'),
                                Tab(text: 'Resubmit'),
                              ],
                            ),
                          ),
                          SizedBox(
                              height: 670, //height of TabBarView
                              child: TabBarView(children: <Widget>[
                                TabContainer(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 0,
                                      horizontal: 20,
                                    ),
                                    child: Column(
                                      children: [
                                        getSpace(30, 0),
                                        Row(
                                          children: [
                                            SizedBox(
                                              width: 310,
                                              height: 55,
                                              child: TextInput(
                                                hintText: 'Search Loans',
                                                controller:
                                                    TextEditingController(),
                                                validator: _validator,
                                                borderColor:
                                                    AppColors.lightGray,
                                                filled: true,
                                                fillColor: AppColors.white,
                                                borderWidth: 0.5,
                                              ),
                                            ),
                                            getSpace(0, 11),
                                            Container(
                                              height: 52.0,
                                              width: 52.0,
                                              decoration: BoxDecoration(
                                                color: AppColors.white,
                                                borderRadius:
                                                    BorderRadius.circular(12),
                                                border: Border.all(
                                                    width: .5,
                                                    color: AppColors.lightGray),
                                              ),
                                              child: IconButton(
                                                  onPressed: () {},
                                                  icon: const Icon(
                                                    Icons.filter_alt_sharp,
                                                    color: Color(0XFFA3B4B0),
                                                  )),
                                            )
                                          ],
                                        ),
                                        model.isLoading
                                            ? const CircularProgressIndicator()
                                            : Expanded(
                                                child: ListView.builder(
                                                    itemCount: loanList.length,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return LoanItem(
                                                          onTap: () {
                                                            loanSummaryModel
                                                                .getLoanSummaryDetails(
                                                                    loanList[
                                                                            index]
                                                                        .id);
                                                            Navigator.push(
                                                                context,
                                                                MaterialPageRoute(
                                                                    builder:
                                                                        (context) =>
                                                                            LoanSummaryPage()));
                                                          },
                                                          loanItem:
                                                              loanList[index]);
                                                    })),
                                      ],
                                    ),
                                  ),
                                ),
                                TabContainer(child: ListView()),
                                TabContainer(child: ListView()),
                                TabContainer(child: ListView()),
                              ]))
                        ])),
              ],
            ),
          ),
        );
      });
    });
  }
}
