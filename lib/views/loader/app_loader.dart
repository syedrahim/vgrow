import 'package:flutter/material.dart';
import 'package:vgrow/core/theme/app_assets.dart';

import '../../core/theme/app_colors.dart';

class AppLoader extends StatefulWidget {
  @override
  _AppLoaderState createState() => _AppLoaderState();
}

class _AppLoaderState extends State<AppLoader> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.darkGreenColor,
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height/3,
          width: MediaQuery.of(context).size.width/3,
          child: Image.asset(AppAssets.appLogo,
            // width: 150,height: 150,
          ),
        ),
      ),
    );
  }
}
