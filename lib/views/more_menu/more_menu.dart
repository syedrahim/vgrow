import 'package:flutter/material.dart';
import 'package:vgrow/connector/auth_connector.dart';
import 'package:vgrow/core/theme/app_assets.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';
import 'package:vgrow/views/home/home_page.dart';
import 'package:vgrow/views/profile/my_profile.dart';

import '../../core/theme/app_colors.dart';
import '../../core/theme/app_styles.dart';
import '../../global_widgets/menu_item.dart';
import '../loan_summary/loan_summary.dart';

class Item {
  final String title;
  final String subtitle;
  final String iconPath;

  const Item({
    required this.title,
    required this.subtitle,
    required this.iconPath,
  });
}

class MoreMenu extends StatelessWidget {
  const MoreMenu({Key? key}) : super(key: key);

  final List<Item> _list = const [
    Item(
        title: 'Home',
        subtitle: 'Metrics, Start a Loan',
        iconPath: AppAssets.home),
    Item(
        title: 'My Profile',
        subtitle: 'Aadhaar, Contact, Income, Photo Details',
        iconPath: AppAssets.person),
    Item(
        title: 'Customers',
        subtitle: 'Individual & Group customers',
        iconPath: AppAssets.customers),
    Item(
        title: 'Loans',
        subtitle: 'Loans in Draft, Rejected Loans',
        iconPath: AppAssets.loans),
    Item(
        title: 'Set Target',
        subtitle: 'Set target for CRO',
        iconPath: AppAssets.set_target),
    Item(
        title: 'Settings',
        subtitle: 'Change Password',
        iconPath: AppAssets.settings),
    Item(
        title: 'FAQ',
        subtitle: 'All frequently asked questions',
        iconPath: AppAssets.faq),
    Item(title: 'Sign Out', subtitle: '', iconPath: AppAssets.signout),
  ];

  @override
  Widget build(BuildContext context) {
    return AuthConnector(builder: (context, authModel) {
      return Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Container(
              width: double.maxFinite,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    AppAssets.profilePic,
                    width: 82,
                    height: 82,
                  ),
                  getSpace(12, 0),
                  Text(
                    "${authModel!.currentUser!.firstName.toString()}  ${authModel!.currentUser!.lastName.toString()}",
                    style: AppStyle.black24SemiboldTextStyle,
                  ),
                  getSpace(7, 0),
                  Text(
                    authModel!.currentUser!.userId.toString(),
                    style: AppStyle.darkGrey14RegularTextStyle,
                  ),
                  for (int i = 0; i < _list.length; i++)
                    MoreMenuItem(
                      title: _list[i].title,
                      subtitle: _list[i].subtitle,
                      iconPath: _list[i].iconPath,
                      onTap: (i == 0)
                          ? () {
                              Navigator.pop(context);
                            }
                          : (i == 1)
                              ? () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => MyProfile()));
                                }
                              : (i == 7)
                                  ? () {
                                      authModel.logOut();
                                    }
                                  : () {},
                    ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 72,
                      height: 72,
                      decoration: BoxDecoration(
                          color: AppColors.darkGreenColor,
                          borderRadius: BorderRadius.circular(36)),
                      child: const Icon(Icons.close,
                          color: AppColors.white, size: 40),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
