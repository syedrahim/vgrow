// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_summary_connector.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LoanSummaryViewModel extends LoanSummaryViewModel {
  @override
  final GetLoanSummaryDetailsAction getLoanSummaryDetails;
  @override
  final LoanSummary loanSummary;
  @override
  final bool isLoading;

  factory _$LoanSummaryViewModel(
          [void Function(LoanSummaryViewModelBuilder)? updates]) =>
      (new LoanSummaryViewModelBuilder()..update(updates))._build();

  _$LoanSummaryViewModel._(
      {required this.getLoanSummaryDetails,
      required this.loanSummary,
      required this.isLoading})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(getLoanSummaryDetails,
        r'LoanSummaryViewModel', 'getLoanSummaryDetails');
    BuiltValueNullFieldError.checkNotNull(
        loanSummary, r'LoanSummaryViewModel', 'loanSummary');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, r'LoanSummaryViewModel', 'isLoading');
  }

  @override
  LoanSummaryViewModel rebuild(
          void Function(LoanSummaryViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanSummaryViewModelBuilder toBuilder() =>
      new LoanSummaryViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    final dynamic _$dynamicOther = other;
    return other is LoanSummaryViewModel &&
        getLoanSummaryDetails == _$dynamicOther.getLoanSummaryDetails &&
        loanSummary == other.loanSummary &&
        isLoading == other.isLoading;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, getLoanSummaryDetails.hashCode);
    _$hash = $jc(_$hash, loanSummary.hashCode);
    _$hash = $jc(_$hash, isLoading.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanSummaryViewModel')
          ..add('getLoanSummaryDetails', getLoanSummaryDetails)
          ..add('loanSummary', loanSummary)
          ..add('isLoading', isLoading))
        .toString();
  }
}

class LoanSummaryViewModelBuilder
    implements Builder<LoanSummaryViewModel, LoanSummaryViewModelBuilder> {
  _$LoanSummaryViewModel? _$v;

  GetLoanSummaryDetailsAction? _getLoanSummaryDetails;
  GetLoanSummaryDetailsAction? get getLoanSummaryDetails =>
      _$this._getLoanSummaryDetails;
  set getLoanSummaryDetails(
          GetLoanSummaryDetailsAction? getLoanSummaryDetails) =>
      _$this._getLoanSummaryDetails = getLoanSummaryDetails;

  LoanSummaryBuilder? _loanSummary;
  LoanSummaryBuilder get loanSummary =>
      _$this._loanSummary ??= new LoanSummaryBuilder();
  set loanSummary(LoanSummaryBuilder? loanSummary) =>
      _$this._loanSummary = loanSummary;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  LoanSummaryViewModelBuilder();

  LoanSummaryViewModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _getLoanSummaryDetails = $v.getLoanSummaryDetails;
      _loanSummary = $v.loanSummary.toBuilder();
      _isLoading = $v.isLoading;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanSummaryViewModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanSummaryViewModel;
  }

  @override
  void update(void Function(LoanSummaryViewModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanSummaryViewModel build() => _build();

  _$LoanSummaryViewModel _build() {
    _$LoanSummaryViewModel _$result;
    try {
      _$result = _$v ??
          new _$LoanSummaryViewModel._(
              getLoanSummaryDetails: BuiltValueNullFieldError.checkNotNull(
                  getLoanSummaryDetails,
                  r'LoanSummaryViewModel',
                  'getLoanSummaryDetails'),
              loanSummary: loanSummary.build(),
              isLoading: BuiltValueNullFieldError.checkNotNull(
                  isLoading, r'LoanSummaryViewModel', 'isLoading'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'loanSummary';
        loanSummary.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'LoanSummaryViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
