import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/models/models.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart' hide Builder;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

part 'loan_summary_connector.g.dart';

typedef GetLoanSummaryDetailsAction = void Function(int? id);

abstract class LoanSummaryViewModel
    implements Built<LoanSummaryViewModel, LoanSummaryViewModelBuilder> {
  factory LoanSummaryViewModel(
      [LoanSummaryViewModelBuilder Function(LoanSummaryViewModelBuilder builder)
          updates]) = _$LoanSummaryViewModel;

  LoanSummaryViewModel._();

  factory LoanSummaryViewModel.fromStore(Store<AppState> store) {
    return LoanSummaryViewModel((LoanSummaryViewModelBuilder b) {
      return b
        ..isLoading = store.state.isLoading
        ..loanSummary = store.state.loanSummary?.toBuilder()
        ..getLoanSummaryDetails = (id) {
          store.dispatch(GetLoanSummaryDetails(id: id));
        };
    });
  }

  GetLoanSummaryDetailsAction get getLoanSummaryDetails;

  LoanSummary get loanSummary;

  bool get isLoading;
}

class LoanSummaryConnector extends StatelessWidget {
  const LoanSummaryConnector({required this.builder});

  final ViewModelBuilder<LoanSummaryViewModel> builder;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LoanSummaryViewModel>(
      builder: builder,
      converter: (Store<AppState> store) =>
          LoanSummaryViewModel.fromStore(store),
    );
  }
}
