import 'package:built_collection/built_collection.dart';
import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/models/models.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart' hide Builder;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

part 'loan_connector.g.dart';

typedef GetLoanDetailsAction = void Function();

abstract class LoanViewModel
    implements Built<LoanViewModel, LoanViewModelBuilder> {
  factory LoanViewModel(
      [LoanViewModelBuilder Function(LoanViewModelBuilder builder)
          updates]) = _$LoanViewModel;

  LoanViewModel._();

  factory LoanViewModel.fromStore(Store<AppState> store) {
    return LoanViewModel((LoanViewModelBuilder b) {
      return b
        ..isLoading = store.state.isLoading
        ..loanDetails = store.state.loanDetails?.toBuilder()
        ..getLoanDetails = () {
          store.dispatch(GetLoanDetails());
        };
    });
  }

  GetLoanDetailsAction get getLoanDetails;

  BuiltList<Loans> get loanDetails;

  bool get isLoading;
}

class LoanConnector extends StatelessWidget {
  const LoanConnector({required this.builder});

  final ViewModelBuilder<LoanViewModel> builder;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, LoanViewModel>(
      builder: builder,
      converter: (Store<AppState> store) => LoanViewModel.fromStore(store),
    );
  }
}
