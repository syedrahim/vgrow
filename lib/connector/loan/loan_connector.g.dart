// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loan_connector.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LoanViewModel extends LoanViewModel {
  @override
  final GetLoanDetailsAction getLoanDetails;
  @override
  final BuiltList<Loans> loanDetails;
  @override
  final bool isLoading;

  factory _$LoanViewModel([void Function(LoanViewModelBuilder)? updates]) =>
      (new LoanViewModelBuilder()..update(updates))._build();

  _$LoanViewModel._(
      {required this.getLoanDetails,
      required this.loanDetails,
      required this.isLoading})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        getLoanDetails, r'LoanViewModel', 'getLoanDetails');
    BuiltValueNullFieldError.checkNotNull(
        loanDetails, r'LoanViewModel', 'loanDetails');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, r'LoanViewModel', 'isLoading');
  }

  @override
  LoanViewModel rebuild(void Function(LoanViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoanViewModelBuilder toBuilder() => new LoanViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    final dynamic _$dynamicOther = other;
    return other is LoanViewModel &&
        getLoanDetails == _$dynamicOther.getLoanDetails &&
        loanDetails == other.loanDetails &&
        isLoading == other.isLoading;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, getLoanDetails.hashCode);
    _$hash = $jc(_$hash, loanDetails.hashCode);
    _$hash = $jc(_$hash, isLoading.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'LoanViewModel')
          ..add('getLoanDetails', getLoanDetails)
          ..add('loanDetails', loanDetails)
          ..add('isLoading', isLoading))
        .toString();
  }
}

class LoanViewModelBuilder
    implements Builder<LoanViewModel, LoanViewModelBuilder> {
  _$LoanViewModel? _$v;

  GetLoanDetailsAction? _getLoanDetails;
  GetLoanDetailsAction? get getLoanDetails => _$this._getLoanDetails;
  set getLoanDetails(GetLoanDetailsAction? getLoanDetails) =>
      _$this._getLoanDetails = getLoanDetails;

  ListBuilder<Loans>? _loanDetails;
  ListBuilder<Loans> get loanDetails =>
      _$this._loanDetails ??= new ListBuilder<Loans>();
  set loanDetails(ListBuilder<Loans>? loanDetails) =>
      _$this._loanDetails = loanDetails;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  LoanViewModelBuilder();

  LoanViewModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _getLoanDetails = $v.getLoanDetails;
      _loanDetails = $v.loanDetails.toBuilder();
      _isLoading = $v.isLoading;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoanViewModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoanViewModel;
  }

  @override
  void update(void Function(LoanViewModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  LoanViewModel build() => _build();

  _$LoanViewModel _build() {
    _$LoanViewModel _$result;
    try {
      _$result = _$v ??
          new _$LoanViewModel._(
              getLoanDetails: BuiltValueNullFieldError.checkNotNull(
                  getLoanDetails, r'LoanViewModel', 'getLoanDetails'),
              loanDetails: loanDetails.build(),
              isLoading: BuiltValueNullFieldError.checkNotNull(
                  isLoading, r'LoanViewModel', 'isLoading'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'loanDetails';
        loanDetails.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'LoanViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
