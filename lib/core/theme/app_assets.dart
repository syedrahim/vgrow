class AppAssets {
  AppAssets._();

  static const String appLogo = 'assets/applogo.png';
  static const String appLogoColored = 'assets/appLogo_colored.png';
  static const String staticImage = 'assets/static_image.png';
  static const String bajajFinservFD = 'assets/bajaj_finserv.png';
  static const String pdfIcon = 'assets/pdf.png';
  static const String staticImage1 = 'assets/static_image1.png';
  static const String staticImage2 = 'assets/static_image2.png';
  static const String dummyPlaceholder = 'assets/dummy_placeholder.jpg';
  static const String userDp =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRt5-LnCYReecjsd-bcFX1WhPAL_5lR_2BaAQ&usqp=CAU';

  static const String profilePic = 'assets/Rectangle 8162.png';
  static const String home = 'assets/home.png';
  static const String person = 'assets/person.png';
  static const String customers = 'assets/customers.png';
  static const String loans = 'assets/loans.png';
  static const String settings = 'assets/settings.png';
  static const String set_target = 'assets/set_target.png';
  static const String faq = 'assets/faq.png';
  static const String signout = 'assets/sign_out.png';

  static const String camera = 'assets/camera.png';
  static const String flag = 'assets/flag.png';
  static const String verified = 'assets/verified.png';
}
