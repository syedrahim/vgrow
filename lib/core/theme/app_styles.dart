import 'package:vgrow/core/theme/app_colors.dart';
import 'package:flutter/material.dart';

class AppStyle {
  //**************************** white - color ********************************//

  static const TextStyle white14RegularTextStyle =
      TextStyle(fontFamily: 'Regular', fontSize: 14, color: Colors.white);

  static const TextStyle white17RegularTextStyle =
      TextStyle(fontFamily: 'Regular', fontSize: 17, color: Colors.white);

  static TextStyle white14SemiboldTextStyle = const TextStyle(
      fontFamily: 'SemiBold', fontSize: 14, color: Colors.white);

  static const TextStyle white15RegularTextStyle =
      TextStyle(fontFamily: 'Regular', fontSize: 15, color: Colors.white);

  static const TextStyle white15SemiboldTextStyle =
      TextStyle(fontWeight: FontWeight.w600, fontSize: 15, color: Colors.white);

  static const TextStyle white20MediumTextStyle =
      TextStyle(fontFamily: 'Medium', fontSize: 20, color: Colors.white);

  static const TextStyle white16BoldTextStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white);

  static const TextStyle white24BoldTextStyle =
      TextStyle(fontFamily: 'Bold', fontSize: 24, color: Colors.white);

  static const TextStyle white30BoldTextStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 30, color: Colors.white);

  static const TextStyle white32BoldTextStyle =
      TextStyle(fontSize: 32, color: Colors.white, fontWeight: FontWeight.bold);

  //**************************** black - color ********************************//

  static TextStyle black14RegularTextStyle =
      TextStyle(fontSize: 14, color: AppColors.black);

  static TextStyle black13RegularTextStyle =
      TextStyle(fontSize: 14, color: AppColors.black);

  static TextStyle black14SemiboldTextStyle = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w600,
      color: AppColors.black,
      fontFamily: 'Acumin Pro');

  static TextStyle black24SemiboldTextStyle = TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w600,
      color: AppColors.black,
      fontFamily: 'Acumin Pro');

  static TextStyle black15SemiboldTextStyle = const TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w600,
      color: AppColors.black,
      fontFamily: 'Acumin Pro');

  static TextStyle black15RegularTextStyle = const TextStyle(
      fontSize: 15,
      color: AppColors.black,
      fontFamily: 'Acumin Pro');


  static TextStyle black17SemiboldTextStyle = const TextStyle(
      fontSize: 17,
      fontWeight: FontWeight.w600,
      color: AppColors.black,
      fontFamily: 'Acumin Pro');

  static TextStyle black28BoldTextStyle =
      TextStyle(fontFamily: 'Bold', fontSize: 28, color: AppColors.black);

  static TextStyle black16MediumTextStyle =
      TextStyle(fontFamily: 'Medium', fontSize: 16, color: AppColors.black);
static TextStyle black16SemiboldTextStyle =
      TextStyle(fontFamily: 'Medium',fontWeight: FontWeight.w600, fontSize: 16, color: AppColors.black);

  //***************************** grey - color ********************************//

  static TextStyle grey14RegularTextStyle = TextStyle(
      fontFamily: 'Regular',
      fontSize: 14,
      color: AppColors.gray2);

  static TextStyle darkGrey14RegularTextStyle =
      TextStyle(fontFamily: 'Regular', fontSize: 14, color: AppColors.darkGray);

  static TextStyle lightGrey13RegularTextStyle = TextStyle(
      fontFamily: 'Regular', fontSize: 13, color: AppColors.lightGray);

  static TextStyle grey24BoldTextStyle = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 24,
      color: AppColors.gray);

  static TextStyle grey13RegularTextStyle = TextStyle(
      fontFamily: 'Regular',
      fontSize: 13,
      color: AppColors.gray);

  //**************************** purple - color *******************************//

  static TextStyle purple28BoldTextStyle =
      TextStyle(fontFamily: 'Bold', fontSize: 28, color: AppColors.themeColor);

  static TextStyle purple32SemiboldTextStyle = TextStyle(
      fontFamily: 'SemiBold', fontSize: 32, color: AppColors.themeColor);

  static TextStyle purple16SemiBoldTextStyle = TextStyle(
      fontFamily: 'SemiBold', fontSize: 16, color: AppColors.themeColor);

  static TextStyle purple20SemiBoldTextStyle = TextStyle(
      fontFamily: 'SemiBold', fontSize: 20, color: AppColors.themeColor);

  static TextStyle purple15MediumTextStyle = TextStyle(
      fontFamily: 'Medium', fontSize: 15, color: AppColors.themeColor);

  // full black text style
  static TextStyle fullBlack32BoldTextstyle = const TextStyle(
      fontSize: 32,
      fontWeight: FontWeight.bold,
      color: AppColors.fullBlack,
      fontFamily: 'Acumin Pro');
  static TextStyle fullBlack16SemiboldTextstyle = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w600,
      color: AppColors.fullBlack,
      fontFamily: 'Acumin Pro');

  static TextStyle fullBlack14SemiboldTextstyle = const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w600,
      color: AppColors.fullBlack,
      fontFamily: 'Acumin Pro');

  static TextStyle fullBlack16BoldTextstyle = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: AppColors.fullBlack,
      fontFamily: 'Acumin Pro');

  // dark green text style
  static TextStyle darkGreen15MediumTextStyle = const TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w600,
      color: AppColors.darkGreenColor,
      fontFamily: 'Acumin Pro');

  // osloblue text style
  static TextStyle osloBlue14RegularTextStyle = const TextStyle(
      fontSize: 14, color: AppColors.osloBlue, fontFamily: 'Acumin Pro');

  static TextStyle osloBlue22MediumTextStyle = const TextStyle(
      fontSize: 22,
      // fontWeight: FontWeight.,
      color: AppColors.osloBlue,
      fontFamily: 'Acumin Pro');

  // summarskyblue text style
  static TextStyle summarSkyBlue14MediumItalicTextStyle = const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      fontStyle: FontStyle.italic,
      color: AppColors.summarSkyBlue,
      fontFamily: 'Acumin Pro');
}
