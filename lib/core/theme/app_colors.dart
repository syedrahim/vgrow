import 'package:flutter/material.dart';

class AppColors {
  static const Color themeColor = const Color(0xFF363A7C);
  static const Color bgColor = const Color(0XFFF2F2E5);
  static const Color textFieldBorderColor = const Color(0XFFD3D3D3);
  static const Color textFieldFillColor = const Color(0XFFF6F6F6);
  static const Color textFieldTextColor = const Color(0XFF4F4F4F);
  static const Color textFieldTitleColor = const Color(0XFF9E9E9E);
  static const Color checkBoxGreenColor = const Color(0XFF00B53E);
  static const Color yellowColor = const Color(0XFFF3CB5E);
  static const Color greenColor = const Color(0XFF26AD10);
  static const Color invitePendingColor = const Color(0XFFF3A65E);
  static const Color blueColor = const Color(0XFF058AFF);
  static const Color bajajColor = const Color(0XFF0074BF);
  static const Color bajajPaymentModeColor = const Color(0XFFEFF9FF);

  //vgrow colors
  static const Color darkGreenColor = const Color(0XFF024242);
  static const Color lightGreen = const Color(0XFF6CE2AE);
  static const Color lightGreen2 = const Color(0XFF79E5B5);
  static const Color lightGreen3 = const Color(0XFF77E6B5);
  static const Color white = const Color(0XFFFFFFFF);
  static const Color fullBlack = const Color(0XFF000000);
  static const Color black = const Color(0XFF010101);
  static const Color borderGreen = const Color(0XFF046769);
  static const Color gray = const Color(0XFFF3F5F7);
  static const Color backgroundGray = const Color(0XFFF2F8F8);
  static const Color darkGray = const Color(0XFF525252);
  static const Color lightGray = const Color(0XFF6A7286);
  static const Color gray2 = const Color(0XFF7E7E7E);
  static const Color osloBlue = const Color(0XFFA3BABA);
  static const Color summarSkyBlue = const Color(0XFF2276EF);
  static const Color blizzardBlue = const Color(0XFFD1F7F8);


}
