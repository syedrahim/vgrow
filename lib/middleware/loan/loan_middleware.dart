import 'package:built_collection/built_collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/actions/loan/loan_action.dart';
import 'package:vgrow/data/services/loan/loan_service.dart';
import 'package:vgrow/models/models.dart';

import '../../core/utils/utils.dart';
import '../../data/app_repository.dart';
import '../../global_widgets/global_error_alert.dart';

class LoanMiddleware {
  LoanMiddleware({required this.repository})
      : loanService = repository.getService<LoanService>() as LoanService;

  final AppRepository repository;
  final LoanService loanService;

  List<Middleware<AppState>> createLoanMiddleware() {
    return <Middleware<AppState>>[
      TypedMiddleware<AppState, GetLoanDetails>(getLoanDetails),
    ];
  }

  void getLoanDetails(
      Store<AppState> store, GetLoanDetails action, NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final AccessToken? token = await repository.getUserAccessToken();
      final Map<String, String> headersToApi = await Utils.getHeader(token!);
      final loanDetails = await loanService.getLoanDetails(headersToApi);
      store.dispatch(SaveLoanDetails(loanDetails: loanDetails));
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      debugPrint(e.toString());
      GlobalErrorAlert.buildAlert(
          store!.state!.navigator!.currentContext!, e.toString());

      // store.dispatch(new ForceLogOutUser(error: e));
      return;
    } catch (e) {
      store.dispatch(SetLoader(false));
      GlobalErrorAlert.buildAlert(
          store!.state!.navigator!.currentContext!, e.toString());

      // store.dispatch(ForceLogOutUser(error: true));
      debugPrint('loan details error ${e.toString()}');
    }
    next(action);
  }
}
