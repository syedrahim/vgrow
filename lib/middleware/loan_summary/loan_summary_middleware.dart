import 'package:built_collection/built_collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:redux/redux.dart';
import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/models/models.dart';

import '../../core/utils/utils.dart';
import '../../data/app_repository.dart';
import '../../data/services/loan_summary/loan_summary_service.dart';
import '../../global_widgets/global_error_alert.dart';

class LoanSummaryMiddleware {
  LoanSummaryMiddleware({required this.repository})
      : loanSummaryService =
  repository.getService<LoanSummaryService>() as LoanSummaryService;

  final AppRepository repository;
  final LoanSummaryService loanSummaryService;

  List<Middleware<AppState>> createLoanSummaryMiddleware() {
    return <Middleware<AppState>>[
      TypedMiddleware<AppState, GetLoanSummaryDetails>(getLoanSummaryDetails),
    ];
  }

  void getLoanSummaryDetails(
      Store<AppState> store, GetLoanSummaryDetails action, NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final AccessToken? token = await repository.getUserAccessToken();
      final Map<String, String> headersToApi = await Utils.getHeader(token!);
      var loanSummary = await loanSummaryService.getLoanSummaryDetails(headersToApi,action.id);
      // print(loanSummary.toString());
      store.dispatch(SaveLoanSummaryDetails(loanSummary: loanSummary));
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      debugPrint(e.toString());
      GlobalErrorAlert.buildAlert(
          store!.state!.navigator!.currentContext!, e.toString());
      return;
    }
    catch (e) {
      store.dispatch(SetLoader(false));
      GlobalErrorAlert.buildAlert(
          store!.state!.navigator!.currentContext!, e.toString());
      debugPrint('loan summary details error ${e.toString()}');
    }
    next(action);
  }
}
