import 'package:vgrow/data/app_repository.dart';
import 'package:vgrow/middleware/loan/loan_middleware.dart';
import 'package:vgrow/middleware/loan_summary/loan_summary_middleware.dart';
import 'package:vgrow/models/models.dart';
import 'package:vgrow/middleware/auth/auth_middleware.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';

EpicMiddleware<AppState> epicMiddleware(AppRepository repository) =>
    EpicMiddleware<AppState>(
      combineEpics<AppState>(
        <Epic<AppState>>[],
      ),
    );

List<Middleware<AppState>> middleware(AppRepository repository) =>
    <List<Middleware<AppState>>>[
      AuthMiddleware(repository: repository).createAuthMiddleware(),
      LoanMiddleware(repository: repository).createLoanMiddleware(),
      LoanSummaryMiddleware(repository: repository)
          .createLoanSummaryMiddleware(),
    ].expand((List<Middleware<AppState>> list) => list).toList();
