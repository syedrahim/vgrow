import 'package:flutter/material.dart';
import 'package:getwidget/components/toast/gf_toast.dart';
import 'package:vgrow/actions/actions.dart';
import 'package:vgrow/data/app_repository.dart';
import 'package:vgrow/data/services/api_service.dart';
import 'package:vgrow/data/services/auth/auth_service.dart';
import 'package:vgrow/global_widgets/global_error_alert.dart';
import 'package:vgrow/models/models.dart';
import 'package:vgrow/views/home/home_page.dart';
import 'package:redux/redux.dart';

class AuthMiddleware {
  AuthMiddleware({required this.repository})
      : authService = repository.getService<AuthService>() as AuthService;

  final AppRepository repository;
  final AuthService authService;

  List<Middleware<AppState>> createAuthMiddleware() {
    return <Middleware<AppState>>[
      TypedMiddleware<AppState, CheckForUserInPrefs>(checkForUserInPrefs),
      TypedMiddleware<AppState, LoginWithPassword>(loginWithPassword),
      TypedMiddleware<AppState, LogOutUser>(logOutUser)
    ];
  }

  void checkForUserInPrefs(Store<AppState> store, CheckForUserInPrefs action,
      NextDispatcher next) async {
    next(action);
    try {
      final AppUser? user = await repository.getUserFromPrefs();

      if (user != null) {
        store.dispatch(SetInitializer(false));
        store.dispatch(SaveUser(userDetails: user));
      } else {
        store.dispatch(SetInitializer(false));
        store.dispatch(SaveUser(userDetails: null));
      }
    } catch (e) {
      return;
    }
  }

  void loginWithPassword(Store<AppState> store, LoginWithPassword action,
      NextDispatcher next) async {
    try {
      String registrationToken = '';
      store.dispatch(new SetLoader(true));
      final Map<String, dynamic> objToApi = <String, dynamic>{
        'employee': <String, String?>{
          "email": action!.email,
          "password": action!.password,
          "grant_type": "password"
        }
      };
      final Map<String, dynamic> response =
          await authService.loginWithPassword(objToApi: objToApi);

      // debugPrint(response.toString());
      final AppUser user = response['employee'];
      // debugPrint(response['token'].toString());
      // print("==========first==============${user.toString()}");
      // print(
      //     "=========second===================${response['employee'].toString()}");
      if (user != null) {
        repository.setUserAccessToken(accessToken: response['token']);
        repository.setUserPrefs(appUser: user);
        store.dispatch(SaveUser(userDetails: user));
        store.state.navigator.currentState!
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        print(
            "=============in state =========${store.state.currentUser.toString()}");
      }
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      debugPrint('============ login error block ========== ${e.toString()}');
      store.dispatch(SetLoader(false));
      GlobalErrorAlert.buildAlert(
          store!.state!.navigator!.currentContext!, e.toString());

      return;
    } catch (e) {
      store.dispatch(SetLoader(false));
      GlobalErrorAlert.buildAlert(
          store!.state!.navigator!.currentContext!, e.toString());
      debugPrint('============ login catch block ========== ${e.toString()}');
    }
    next(action);
  }

  void logOutUser(
      Store<AppState> store, LogOutUser action, NextDispatcher next) async {
    repository.setUserPrefs(appUser: null);
    store.dispatch(SaveUser(userDetails: null));
    next(action);
  }
}
