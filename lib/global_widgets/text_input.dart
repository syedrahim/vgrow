import 'package:flutter/material.dart';
import '../core/theme/app_colors.dart';

class TextInput extends StatefulWidget {
  TextInput(
      {Key? key,
      required this.hintText,
      required this.controller,
      required this.validator,
      this.filled = false,
      this.fillColor = AppColors.gray,
      this.enableBorder = true,
      this.borderColor = AppColors.darkGreenColor,
      this.borderWidth = 1.5,
      this.hideText = false})
      : super(key: key);

  final String hintText;
  final Color borderColor;
  final double borderWidth;
  final bool enableBorder;
  final bool filled;
  final Color fillColor;
  final bool hideText;
  bool obscureText = true;
  final TextEditingController controller;
  final String? Function(String?) validator;

  @override
  State<TextInput> createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      validator: widget.validator,
      decoration: InputDecoration(
          hintText: widget.hintText,
          focusedBorder: OutlineInputBorder(
            borderSide: widget.enableBorder
                ? BorderSide(
                    width: widget.borderWidth, color: widget.borderColor)
                : BorderSide.none,
            borderRadius: BorderRadius.circular(12),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: widget.enableBorder
                ? BorderSide(
                    width: widget.borderWidth, color: widget.borderColor)
                : BorderSide.none,
            borderRadius: BorderRadius.circular(12),
          ),
          filled: widget.filled,
          fillColor: widget.fillColor,
          suffixIcon: widget.hideText
              ? IconButton(
                  icon: widget.obscureText
                      ? const Icon(
                          Icons.visibility,
                          color: AppColors.darkGray,
                        )
                      : const Icon(
                          Icons.visibility_off,
                          color: AppColors.darkGray,
                        ),
                  onPressed: () {
                    setState(() {
                      widget.obscureText = !widget.obscureText;
                    });
                  },
                )
              : null),
      obscureText: widget.hideText && widget.obscureText,
    );
  }
}
