import 'package:flutter/material.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';

import '../core/theme/app_colors.dart';
import '../core/theme/app_styles.dart';

class MoreMenuItem extends StatelessWidget {
  const MoreMenuItem(
      {Key? key,
      required this.iconPath,
      required this.title,
      required this.subtitle,
      required this.onTap})
      : super(key: key);

  final String iconPath;
  final String title;
  final String subtitle;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(50, 10, 0, 0),
        child: Row(
          children: [
            Container(
                width: 64,
                height: 64,
                decoration: BoxDecoration(
                    color: AppColors.gray,
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Image.asset(
                    iconPath,
                    width: 20,
                    height: 20,
                  ),
                )),
            Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 0, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: AppStyle.black17SemiboldTextStyle,
                  ),
                  getSpace(10, 0),
                  Text(
                    subtitle,
                    style: AppStyle.lightGrey13RegularTextStyle,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
