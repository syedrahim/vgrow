import 'package:flutter/material.dart';
import 'package:vgrow/connector/loan_summary/loan_summary_connector.dart';
import 'package:vgrow/global_widgets/icon_container.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';

import '../core/theme/app_assets.dart';
import '../core/theme/app_colors.dart';
import '../core/theme/app_styles.dart';
import '../models/loan/loans.dart';
import '../views/loan_summary/loan_summary.dart';

class LoanItem extends StatelessWidget {
  const LoanItem({Key? key, required this.loanItem, this.onTap})
      : super(key: key);

  final Loans loanItem;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return LoanSummaryConnector(builder: (context, loanSummaryModel) {
      return InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
          child: Container(
            width: 395,
            height: 220,
            padding: EdgeInsets.fromLTRB(30, 27, 0, 0),
            decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(12)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      loanItem.id.toString(),
                      style: AppStyle.fullBlack16BoldTextstyle,
                    ),
                    getSpace(0, 10),
                    Text(
                      loanItem.loanTypeName.toString(),
                      style: AppStyle.summarSkyBlue14MediumItalicTextStyle,
                    ),
                    Container(
                      height: 30,
                      // width: 56,
                      decoration: BoxDecoration(
                        color: AppColors.blizzardBlue,
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 5, 12, 5),
                        child: Text(
                          loanItem.status.toString(),
                          style: AppStyle.summarSkyBlue14MediumItalicTextStyle,
                        ),
                      ),
                    ),
                  ],
                ),
                getSpace(10, 0),
                Text(
                  'Members:',
                  style: AppStyle.grey14RegularTextStyle,
                ),
                getSpace(8, 0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const CircleAvatar(
                      radius: 18,
                      backgroundImage: AssetImage(AppAssets.profilePic),
                    ),
                    Row(
                      children: [
                        const IconContainer(
                            size: 48,
                            color: AppColors.gray,
                            icon: Icon(
                              Icons.phone_enabled,
                              color: Color(0XFFA3B4B0),
                            )),
                        getSpace(0, 8),
                        const IconContainer(
                            size: 48,
                            color: AppColors.gray,
                            icon: Icon(
                              Icons.message,
                              color: Color(0XFFA3B4B0),
                            )),
                        getSpace(0, 8),
                      ],
                    ),
                  ],
                ),
                getSpace(10, 0),
                Text(
                  'Submitted CRO:',
                  style: AppStyle.grey14RegularTextStyle,
                ),
                getSpace(8, 0),
                Row(
                  children: [
                    CircleAvatar(
                      radius: 18,
                      backgroundImage: AssetImage(AppAssets.profilePic),
                    ),
                    getSpace(0, 11),
                    Column(
                      children: [
                        Text(
                          loanItem!.createdBy?.firstname != null
                              ? loanItem!.createdBy!.firstname! +
                                  ' ' +
                                  loanItem!.createdBy!.lastname!
                              : '---',
                          // loanItem.createdBy!.firstname.toString(),
                          style: AppStyle.fullBlack14SemiboldTextstyle,
                        ),
                        Text(
                          'ID: ' + loanItem!.createdById!.toString(),
                          style: AppStyle.grey14RegularTextStyle,
                        )
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      );
    });
  }
}
