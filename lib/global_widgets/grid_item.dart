import 'package:flutter/material.dart';
import 'package:vgrow/global_widgets/widget_helper.dart';

import '../core/theme/app_colors.dart';
import '../core/theme/app_styles.dart';

class GridItem extends StatelessWidget {
  const GridItem({Key? key, this.title = "", this.subtitle = ""}) : super(key: key);

  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        color: AppColors.lightGreen2.withOpacity(.06),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: AppStyle.white32BoldTextStyle,
            ),
            getSpace(10, 0),
            Text(
              subtitle,
              style: AppStyle.osloBlue14RegularTextStyle,
            )
          ],
        ));
  }
}
