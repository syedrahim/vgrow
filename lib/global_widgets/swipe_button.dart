import 'package:flutter/material.dart';
import 'package:vgrow/global_widgets/rounded_button.dart';

import '../core/theme/app_colors.dart';
import '../core/theme/app_styles.dart';

class SwipeButton extends StatefulWidget {
  SwipeButton({Key? key}) : super(key: key);

  bool isButton1Selected = true;
  bool isButton2Selected = false;

  @override
  State<SwipeButton> createState() => _SwipeButtonState();
}

class _SwipeButtonState extends State<SwipeButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.white, width: 0.5),
          borderRadius: BorderRadius.circular(30)),
      child: Row(
        children: [
          RoundedButton(
            buttonText: 'PIPELINE',
            onPressed: () {
              setState(() {
                widget.isButton1Selected = true;
                widget.isButton2Selected = false;
              });
            },
            width: 180,
            backgroundColor: widget.isButton1Selected
                ? AppColors.gray
                : AppColors.darkGreenColor,
            textStyle: widget.isButton1Selected
                ? AppStyle.darkGreen15MediumTextStyle
                : AppStyle.white15SemiboldTextStyle,
          ),
          RoundedButton(
            buttonText: 'DUE COLLECTION',
            onPressed: () {
              setState(() {
                widget.isButton1Selected = false;
                widget.isButton2Selected = true;
              });
            },
            width: 206,
            backgroundColor: widget.isButton2Selected
                ? AppColors.gray
                : AppColors.darkGreenColor,
            textStyle: widget.isButton2Selected
                ? AppStyle.darkGreen15MediumTextStyle
                : AppStyle.white15SemiboldTextStyle,
          ),
        ],
      ),
    );
  }
}
