import 'package:flutter/material.dart';

class GlobalErrorAlert {
  static buildAlert(context, message) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }
}
