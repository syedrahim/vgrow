import 'package:flutter/material.dart';


class IconContainer extends StatelessWidget {
  const IconContainer({Key? key, required this.size, required this.color, required this.icon}) : super(key: key);

  final double size;
  final Color color;
  final Icon icon;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(12),
      ),
      child: IconButton(
          onPressed: () {},
          icon: icon),
    );
  }
}
