import 'package:flutter/material.dart';

import '../core/theme/app_colors.dart';

class TabContainer extends StatelessWidget {
  const TabContainer({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: AppColors.backgroundGray,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20)),
      ),
      child: Column(
        children: [
          Expanded(child: child),
        ],
      ),
    );
  }
}
