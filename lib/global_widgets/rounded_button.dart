import 'package:flutter/material.dart';

import '../core/theme/app_colors.dart';
import '../core/theme/app_styles.dart';

class RoundedButton extends StatelessWidget {
  const RoundedButton(
      {Key? key,
      required this.buttonText,
      required this.onPressed,
      this.textStyle = AppStyle.white15SemiboldTextStyle,
      this.backgroundColor = AppColors.darkGreenColor,
      this.enableBackgroundColor = true,
      this.width = 350})
      : super(key: key);

  final String buttonText;
  final TextStyle textStyle;
  final Color backgroundColor;
  final bool enableBackgroundColor;
  final void Function() onPressed;
  final double width;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      style: TextButton.styleFrom(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0), side: BorderSide.none),
        fixedSize: Size(width, 64),
        backgroundColor:
            enableBackgroundColor ? backgroundColor : AppColors.white,
      ),
      child: Text(buttonText,
          style: enableBackgroundColor
              ? textStyle
              : AppStyle.darkGreen15MediumTextStyle),
    );
  }
}
